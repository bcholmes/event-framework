//
//  BCHTheme.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-02-15.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <UIColor-HexString/UIColor+HexString.h>

#import <MaterialComponents/MaterialButtons.h>
#import <MaterialComponents/MaterialTextFields.h>
#import <MaterialComponents/MaterialAppBar.h>

#import "BCHTheme.h"

#import "BCHCardView.h"
#import "UIColor+Extensions.h"
#import "BCHGridline.h"
#import "BCHBioTableViewCell.h"
#import "BCHEventCell.h"
#import "BCHEventTableViewCell.h"
#import "BCHDayChooserView.h"
#import "BCHTimeRowHeaderBackground.h"
#import "BCHTimeRowHeader.h"
#import "BCHCurrentTimeIndicator.h"
#import "BCHDivider.h"
#import "BCHOpenItemButton.h"
#import "BCHBubbleLabel.h"

@interface BCHTheme ()

@property (nonatomic, strong) UIColor* titleColor;
@property (nonatomic, strong) UIColor* cellSelectionColor;

@end

@implementation BCHTheme

- (id) initWithBaseColor:(UIColor*) baseColor {
    if (self = [super init]) {
        self.baseColor = baseColor;
        self.menuItemColor = baseColor;
        self.lightColor = [self.baseColor blendWithColor:[UIColor whiteColor] alpha:0.5];
        self.cellSelectionColor = [self.baseColor blendWithColor:[UIColor whiteColor] alpha:0.7];
        self.darkColor = [self.baseColor blendWithColor:[UIColor blackColor] alpha:0.3];
        self.myItemColor = [self.baseColor blendWithColor:[UIColor whiteColor] alpha:0.8];
        self.selectedMenuItemColor = [self.baseColor blendWithColor:[UIColor whiteColor] alpha:0.25];
        self.subListColor = [self.baseColor blendWithColor:[UIColor whiteColor] alpha:0.8];
        self.leaderboardBarColor = self.myItemColor;
        self.titleColor = [UIColor whiteColor];
        self.detailScreenBox = [[UIColor whiteColor] colorWithAlphaComponent:0.85];
        self.greyTextColor = [UIColor colorWithHexString:@"666666"];
        self.buttonColor = self.baseColor;  //[UIColor colorWithHexString:@"45d2ff"];
    }
    return self;
}

-(void) configure {
    [self configureNavigationBar];
    [self configureGridLine];
    [self configureBioTableCell];
    [self configureCardView];
    [self configureEventTableViewCell];
    [self configureGridCell];
    [self configureDayChooserView];
    [self configureTimeRowHeaderBackground];
    [self configureTimeRowHeader];
    [self configureCurrentTimeIndicator];
    [self configureSearchBar];
    [self configureDivider];
    [self configureOpenItemButton];
    [self configureBubbleLabel];
    [self configurePrimaryFlatButton];
    [self configureMaterialNavigationBar];
}

-(void) configureOpenItemButton {
    BCHOpenItemButton* proxy = [BCHOpenItemButton appearance];
    proxy.highlightedColor = self.cellSelectionColor;
    proxy.inkColor = [self.baseColor colorWithAlphaComponent:0.32];
}

-(void) configureDivider {
    BCHDivider* proxy = [BCHDivider appearance];
    proxy.backgroundColor = [UIColor colorWithHexString:@"aaaaaa"];
}

-(void) configureEventTableViewCell {
    BCHEventTableViewCell* proxy = [BCHEventTableViewCell appearance];
    proxy.inkColor = [self.baseColor colorWithAlphaComponent:0.32];
}

-(void) configureCardView {
    BCHCardView* proxy = [BCHCardView appearance];
    proxy.inkColor = [self.baseColor colorWithAlphaComponent:0.32];
}


-(void) configureBubbleLabel {
    BCHBubbleLabel* proxy = [BCHBubbleLabel appearance];
    proxy.bubbleColor = [UIColor colorWithHexString:@"ff0000"];
}

-(void) configureSearchBar {
    UISearchBar* proxy = [UISearchBar appearance];
    proxy.barTintColor = self.baseColor;
    proxy.tintColor = [UIColor whiteColor];
}

-(void) configureCurrentTimeIndicator {
    BCHCurrentTimeIndicator* proxy = [BCHCurrentTimeIndicator appearance];
    proxy.backgroundColor = self.darkColor;
}

-(void) configureTimeRowHeader {
    BCHTimeRowHeader* proxy = [BCHTimeRowHeader appearance];
    proxy.textColor = [UIColor whiteColor];
}

-(void) configureTimeRowHeaderBackground {
    BCHTimeRowHeaderBackground* proxy = [BCHTimeRowHeaderBackground appearance];
    proxy.backgroundColor = self.darkColor;
}

-(void) configureGridCell {
    BCHEventCell* proxy = [BCHEventCell appearance];
    proxy.backgroundColor = [UIColor whiteColor];
    proxy.cellSelectionColor = self.cellSelectionColor;
    proxy.highligherColor = [UIColor yellowColor];
    
}

-(void) configureBioTableCell {
    BCHBioTableViewCell* proxy = [BCHBioTableViewCell appearance];
    proxy.selectedBackgroundColor = self.cellSelectionColor;
    
}

-(void) configureDayChooserView {
    BCHDayChooserView* proxy = [BCHDayChooserView appearance];
    proxy.backgroundColor = self.baseColor;
    
}

-(void) configureGridLine {
    BCHGridline* proxy = [BCHGridline appearance];
    proxy.backgroundColor = [[UIColor colorWithHexString:@"d7d7d7"] colorWithAlphaComponent:0.5];

}

-(void) configureNavigationBar {
    
    UINavigationBar* barProxy = [UINavigationBar appearance];
    [barProxy setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [barProxy setShadowImage:[[UIImage alloc] init]];
    [barProxy setBarTintColor:self.baseColor];
    [barProxy setBarStyle:UIBarStyleBlack];
    [barProxy setTranslucent:NO];
//    [barProxy setOpaque:NO];
    [barProxy setTitleTextAttributes:@{NSForegroundColorAttributeName: self.titleColor }];
    [barProxy setTintColor:self.titleColor];
    
}

-(NSDictionary*) findStyleWithAttributes:(BCHHtmlStyle) htmlStyles size:(CGFloat) fontSize {
    UIFont* font = [UIFont systemFontOfSize:fontSize];
    
    UIFontDescriptorSymbolicTraits traits = 0;
    if (htmlStyles & BCHHtmlBoldStyle) {
        traits |= UIFontDescriptorTraitBold;
    }
    if (htmlStyles & BCHHtmlItalicStyle)
    {
        traits |= UIFontDescriptorTraitItalic;
    }
    
    UIFont* newFont = [UIFont fontWithDescriptor:[[font fontDescriptor] fontDescriptorWithSymbolicTraits:traits] size:font.pointSize];
    
    if (htmlStyles & BCHHtmlStrikethroughStyle) {
        return @{ NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle],
                  NSFontAttributeName : newFont,
                  NSForegroundColorAttributeName : [UIColor colorWithHexString:@"888888"] };
    } else {
        return @{ NSFontAttributeName : newFont,
                  NSForegroundColorAttributeName : [UIColor blackColor]};
    }
    
}

-(void) configurePrimaryFlatButton {
    MDCFlatButton* proxy = [MDCFlatButton appearance];
    proxy.backgroundColor = self.buttonColor;
    proxy.tintColor = [UIColor whiteColor];
    proxy.inkColor = [[UIColor whiteColor] colorWithAlphaComponent:0.32];
    proxy.disabledAlpha = 1.0;
    proxy.uppercaseTitle = YES;
    [proxy setBackgroundColor:[UIColor grayColor] forState:UIControlStateDisabled];
    [proxy setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [proxy setTitleColor:[UIColor grayColor]  forState:UIControlStateDisabled];
}

-(void) applyTheme:(MDCTextInputControllerBase*) textInputController {
    textInputController.errorColor = [UIColor redColor];
    textInputController.activeColor = self.baseColor;
    textInputController.floatingPlaceholderActiveColor = [UIColor grayColor];
}

-(void) configureMaterialNavigationBar {
    MDCNavigationBar* proxy = [MDCNavigationBar appearance];
    proxy.leadingBarItemsTintColor = [UIColor clearColor];
    proxy.trailingBarItemsTintColor = [UIColor clearColor];
}
@end
