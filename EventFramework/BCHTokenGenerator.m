//
//  BCHTokenGenerator.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHTokenGenerator.h"
#import <CommonCrypto//CommonDigest.h>

@implementation BCHTokenGenerator

-(id) initWithClientId:(NSString *)clientId andPersonId:(NSString *)personId {
    if (self = [super init]) {
        self.clientId = clientId;
        self.personId = personId;
    }
    return self;
}

-(NSString*) token {
    NSString* key = [NSString stringWithFormat:@"%@||hereistherootoftherootandthebudofthebudandtheskyoftheskyofatreecalledlife", self.clientId];
    if (self.personId != nil) {
        key = [NSString stringWithFormat:@"%@||%@", key, self.personId];
    }
    
    NSData* dataIn = [key dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData* macOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(dataIn.bytes, (unsigned int) dataIn.length,  macOut.mutableBytes);
    
    NSString* base64String = [macOut base64EncodedStringWithOptions:0];
    
    return base64String;
}

@end
