//
//  BCHTheme.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-02-15.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "NSAttributedString+Html.h"

@class MDCTextInputControllerBase;


@interface BCHTheme : NSObject<BCHStyleProvider>

- (id) initWithBaseColor:(UIColor*) baseColor;

@property (nonatomic, strong) UIColor* baseColor;
@property (nonatomic, strong) UIColor* lightColor;
@property (nonatomic, strong) UIColor* darkColor;
@property (nonatomic, strong) UIColor* myItemColor;
@property (nonatomic, strong) UIColor* bottomBarColor;
@property (nonatomic, strong) UIColor* buttonColor;

@property (nonatomic, strong) UIColor* menuItemColor;
@property (nonatomic, strong) UIColor* selectedMenuItemColor;

@property (nonatomic, strong) UIColor* subListColor;
@property (nonatomic, strong) UIColor* leaderboardBarColor;

@property (nonatomic, strong) UIColor* detailScreenBox;
@property (nonatomic, strong) UIColor* greyTextColor;

-(void) configure;

-(void) applyTheme:(MDCTextInputControllerBase*) textInputController;

@end
