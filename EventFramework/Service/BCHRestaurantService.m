//
//  BCHRestaurantService.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHRestaurantService.h"
#import "BCHHttpHelper.h"
#import "BCHCacheEntry.h"
#import "BCHRestaurant.h"

@interface BCHRestaurantService()

@property (nonatomic, strong) NSString* cachePath;

@end

@implementation BCHRestaurantService

-(instancetype) init {
    if (self = [super init]) {
        self.cachePath = [NSString stringWithFormat:@"%@/Library/Caches/Restaurants", NSHomeDirectory()];
    }
    return self;
}


-(void) fetchAndCacheRestaurantUpdates {
    /*
    BCHHttpHelper* http = [BCHHttpHelper new];
    [http getJsonFromPath:@"/wissched/restaurants.json" callback:^(NSError* error, NSURLResponse* response, NSData* data) {
        
        if (error) {
            NSLog(@"Error fetching restaurant updats");
        } else {
            // write to cache...
            NSLog(@"save restaurants");
            BCHCacheEntry* entry = [[BCHCacheEntry alloc] init];
            entry.data = data;
            [NSKeyedArchiver archiveRootObject:entry toFile:self.cachePath];
        }
    }];
    */
}

- (NSData*) readResourceFile:(NSString*)path {
    NSString* filePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    if (filePath) {
        return [NSData dataWithContentsOfFile:filePath];
    } else {
        return nil;
    }
}


-(BCHCacheEntry*) readCache {
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.cachePath]) {
        return [NSKeyedUnarchiver unarchiveObjectWithFile:self.cachePath];
    } else {
        return nil;
    }
}

- (id) parseJson:(NSData*)json {
    return json != nil ? [NSJSONSerialization JSONObjectWithData:json options:kNilOptions error:nil] : nil;
}

- (id) parseJsonFile:(NSString*)name {
    return [self parseJson:[self readResourceFile:name]];
}
-(NSArray*) restaurants {
    /*
    BCHCacheEntry* entry = [self readCache];
    
    if (entry && entry.data) {
        NSLog(@"get restaurants from cache");
        NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:entry.data options:kNilOptions error:nil];
        return [BCHRestaurant fromJsonCollection:dictionary];
    } else {
     */
        NSLog(@"get restaurants from initial list");
        NSDictionary *dictionary = [self parseJsonFile:@"restaurants.json"];
        return [BCHRestaurant fromJsonCollection:dictionary];
    /* } */
}


@end
