//
//  BCHGridViewService.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-02-27.
//

#import "BCHGridViewService.h"

#import "BCHSimpleScheduledItem.h"
#import "BCHScheduleEntryTable.h"

@interface BCHGridViewService()

@property (nonatomic, nonnull, strong) BCHEventService* eventService;
@property (nonatomic, nonnull, strong) NSArray* simpleItems;

@end

@implementation BCHGridViewService

@synthesize allDays = _allDays;

-(instancetype) initWithEventService:(BCHEventService*) eventService {
    if (self = [super init]) {
        self.eventService = eventService;
    }
    return self;
}

-(NSArray*) simpleItems {
    if (_simpleItems == nil) {
        _simpleItems = [self loadHours];
    }
    return _simpleItems;
}

-(NSData*) contentsOfHoursJsonFile {
    NSData* result = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"hours" ofType:@"json"];
    if (filePath) {
        NSError *error = nil;
        result = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&error];
        if (error) {
            NSLog(@"%@: error=%@", NSStringFromSelector(_cmd), [error localizedDescription]);
            result = nil;
        }
    }
    return result;
}
-(NSArray*) loadHours {
    NSError* error = nil;
    NSData* json = [self contentsOfHoursJsonFile];
    if (json) {
        NSDictionary* data = [NSJSONSerialization JSONObjectWithData:json options:0 error:&error];
        return [BCHSimpleScheduledItem fromJsonArray:[data objectForKey:@"items"]];
    } else {
        return [NSArray new];
    }
}

-(NSArray*) itemsForDay:(NSString*) day {
    NSMutableArray* events = [[self.eventService topLevelEventsForDay:day] mutableCopy];
    for (BCHScheduledItem* item in self.simpleItems) {
        if ([item.time.day isEqualToString:day]) {
            [events addObject:item];
        }
    }
    return [NSArray arrayWithArray:events];
}

-(NSArray*) allDays {
    if (_allDays == nil) {
        NSMutableArray* times = [NSMutableArray new];
        for (BCHScheduledItem* event in [self.eventService allLeafEvents]) {
            if (![times containsObject:event.time]) {
                [times addObject:event.time];
            }
        }
        for (BCHScheduledItem* item in self.simpleItems) {
            if (![times containsObject:item.time]) {
                [times addObject:item.time];
            }
        }

        NSMutableArray* days = [NSMutableArray new];
        for (UnslackedTimeRange* time in [times sortedArrayUsingSelector:@selector(compare:)]) {
            if (![days containsObject:time.day]) {
                [days addObject:time.day];
            }
        }
        
        _allDays = days;
    }
    return _allDays;
}

@end
