//
//  BCHRestaurantService.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHRestaurantService : NSObject

@property (nonatomic, readonly) NSArray* restaurants;

-(void) fetchAndCacheRestaurantUpdates;

@end
