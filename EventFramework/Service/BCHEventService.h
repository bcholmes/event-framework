//
//  BCHEventRatingService.h
//  EventsSchedule
//
//  Created by BC Holmes on 2016-03-03.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "BCHPrimaryEvent.h"

@interface BCHEventService : NSObject

-(instancetype) initWithConventionId:(NSString*) conventionId primaryEvent:(BCHPrimaryEvent*) primaryEvent managedObjectContext:(NSManagedObjectContext*) managedObjectContext;
-(void) fetchRankings:(BCHPrimaryEvent*) primaryEvent;
-(NSArray*) topLevelEventsForDay:(NSString*) day;
-(NSInteger) countOfItemsOnMySchedule;
-(void) changeScheduledStatusOfEvent:(UnslackedEvent*) event added:(BOOL) added;
-(void) changeHighlightStatusOfEvent:(UnslackedEvent*) event status:(BOOL) highlight;
-(void) changeYayStatusOfEvent:(UnslackedEvent*) event status:(BOOL) yay;
-(NSArray*) allLeafEvents;
-(BCHRank*) findRankForEventId:(NSString*) eventId;
-(void) readSelectionsFromDatabase;

@end
