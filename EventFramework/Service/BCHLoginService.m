//
//  BCHLoginService.m
//  Appirater
//
//  Created by BC Holmes on 2017-10-15.
//

#import "BCHLoginService.h"

#import <AFNetworking/AFNetworking.h>

#import "BCHLoginHelper.h"

@interface BCHLoginService()

@property (nonnull, nonatomic, strong) AFHTTPSessionManager* manager;
@property (nonnull, nonatomic, strong) BCHLoginHelper* loginHelper;

@end

@implementation BCHLoginService

-(instancetype) initWithHelper:(BCHLoginHelper*) loginHelper {
    if (self = [super init]) {
        self.loginHelper = loginHelper;
        self.manager = [AFHTTPSessionManager manager];
        self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    return self;
}

-(void) loginWithUserid:(NSString*) userid password:(NSString*) password callback:(void (^)(NSError* error)) callback {
    NSDictionary* credentials = @{ @"userid": userid, @"password": password, @"app-id": @"dccf1a78-3a26-11e6-ac61-9e71128cae77"  };
    
    [self.manager POST:@"https://ayizan.org/api/login" parameters:credentials progress:nil success:^(NSURLSessionTask* task, id responseObject) {
        NSString* jwtToken = [self extractJwtToken:(NSHTTPURLResponse*) task.response];
        NSDictionary* json = (NSDictionary*) responseObject;
        
        [self.loginHelper recordLogin:[json objectForKey:@"personId"] appKey:[json objectForKey:@"api-key"] jwtToken:jwtToken];
        
        if (callback != nil) {
            callback(nil);
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (callback != nil) {
            callback(error);
        }
    }];
}

-(NSString*) extractJwtToken:(NSHTTPURLResponse*) httpResponse {
    NSDictionary* headers = [httpResponse allHeaderFields];
    NSString* authorizationHeader = [headers objectForKey:@"Authorization"];
    if ([authorizationHeader rangeOfString:@"Bearer "].location == 0) {
        return [authorizationHeader substringFromIndex:@"Bearer ".length];
    } else {
        return nil;
    }
}

@end
