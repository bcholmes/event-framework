//
//  BCHCacheService.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHCacheService.h"

@implementation Entry

- (id) initWithCoder:(NSCoder*)decoder {
    if ((self = [super init])) {
        self.data = [decoder decodeObjectForKey:@"data"];
        self.lastModified = [decoder decodeObjectForKey:@"lastModified"];
        self.eTag = [decoder decodeObjectForKey:@"eTag"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.data forKey:@"data"];
    [encoder encodeObject:self.lastModified forKey:@"lastModified"];
    [encoder encodeObject:self.eTag forKey:@"eTag"];
}

- (NSString *) description {
    return [NSString stringWithFormat:@"{ data length=%lu, lastModified=%@, eTag=%@ }",
            (unsigned long)[self.data length], self.lastModified, self.eTag];
}

-(NSDictionary*) createRequestHeaders {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    if (self.lastModified) {
        [result setObject:self.lastModified forKey:@"If-Modified-Since"];
    }
    if (self.eTag) {
        [result setObject:self.eTag forKey:@"If-None-Match"];
    }
    return result;
}

@end

@interface BCHCacheService()
@property (nonatomic, strong) NSString* cachePath;
@property (nonatomic, strong) NSMutableDictionary *cache;

@end


@implementation BCHCacheService
- (id) init {
    if ((self = [super init])) {
        self.cachePath = [NSString stringWithFormat:@"%@/Library/Caches/Updates", NSHomeDirectory()];
        NSLog(@"cache path : %@", self.cachePath);
        [self readCache];
    }
    return self;
}

- (void) writeCache {
    BOOL result = [NSKeyedArchiver archiveRootObject:self.cache toFile:self.cachePath];
    NSLog(@"Flush of cache %s", result ? "succeeded" : "FAILED");
}

- (void) readCache {
    self.cache = [NSKeyedUnarchiver unarchiveObjectWithFile:self.cachePath];
    if (!self.cache) {
        self.cache = [[NSMutableDictionary alloc] init];
    }
}

- (NSString*) textNamed:(NSString*)path {
    NSString *result = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    if (filePath) {
        NSError *error = nil;
        result = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"%@: error=%@", NSStringFromSelector(_cmd), [error localizedDescription]);
        }
    }
    return result;
}

-(Entry*) entryForKey:(NSString*)key {
    Entry* entry = [self.cache objectForKey:key];
    if (!entry) {
        entry = [[Entry alloc] init];
        [self.cache setObject:entry forKey:key];
    }    
    return entry;
}

@end
