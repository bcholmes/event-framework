//
//  BCHGridViewService.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-02-27.
//

#import <Foundation/Foundation.h>

#import "BCHEventService.h"

NS_ASSUME_NONNULL_BEGIN

@interface BCHGridViewService : NSObject

@property (nonatomic, nullable, readonly) NSArray* allDays;

-(instancetype) initWithEventService:(BCHEventService*) eventService;
-(NSArray*) itemsForDay:(NSString*) day;

@end

NS_ASSUME_NONNULL_END
