//
//  BCHLoginService.h
//  Appirater
//
//  Created by BC Holmes on 2017-10-15.
//

#import <Foundation/Foundation.h>

#import "BCHLoginHelper.h"

@interface BCHLoginService : NSObject

-(instancetype _Nonnull) initWithHelper:(BCHLoginHelper* _Nonnull) loginHelper;

-(void) loginWithUserid:(NSString* _Nonnull) userid password:(NSString* _Nonnull) password callback:(void (^ _Nullable)(NSError* _Nullable error)) callback;

@end
