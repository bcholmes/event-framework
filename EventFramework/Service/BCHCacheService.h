//
//  BCHCacheService.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entry : NSObject<NSCoding>
@property (strong) NSData *data;
@property (strong) NSString *lastModified;
@property (strong) NSString *eTag;

-(NSDictionary*) createRequestHeaders;
@end

@interface BCHCacheService : NSObject

-(Entry*) entryForKey:(NSString*) key;
-(void) writeCache;

@end
