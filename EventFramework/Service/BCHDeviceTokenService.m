//
//  BCHDeviceTokenService.m
//  EventsSchedule
//
//  Created by BC Holmes on 2016-03-03.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHDeviceTokenService.h"
#import "BCHHttpHelper.h"

@implementation BCHDeviceTokenService

-(void) sendDeviceTokenToServer:(NSString*) deviceToken clientId:(NSString*) clientId {
    BCHHttpHelper* http = [BCHHttpHelper new];
    
    NSString* appName=[[[NSBundle mainBundle] infoDictionary]  objectForKey:(id)kCFBundleNameKey];
    NSDictionary* json = @{ @"deviceId": deviceToken, @"clientId": clientId, @"os": @"iOS", @"conventionType": appName };
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
    
    [http postJsonData:jsonData withPath:@"/api/devices/" callback:nil];
}

@end
