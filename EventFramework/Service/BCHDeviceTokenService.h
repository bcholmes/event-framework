//
//  BCHDeviceTokenService.h
//  EventsSchedule
//
//  Created by BC Holmes on 2016-03-03.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHDeviceTokenService : NSObject

-(void) sendDeviceTokenToServer:(NSString*) deviceToken clientId:(NSString*) clientId;

@end
