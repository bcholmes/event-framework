//
//  BCHImageService.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BCHParticipant;
@class BCHCacheService;
@class BCHTokenGenerator;
@class BCHPrimaryEvent;

@interface BCHImageService : NSObject

-(id) initWithConventionId:(NSString*) conventionId tokenGenerator:(BCHTokenGenerator*) tokenGenerator andCache:(BCHCacheService*) cacheService primaryEvent:(BCHPrimaryEvent*) event;

-(void) uploadAvatar:(UIImage*) image;
-(void) fetchLatestAvatars;
-(NSString*) getAvatarAsEncodedHtmlImage:(BCHParticipant*) participant;
-(void) writeImageToFileSystem:(UIImage*) image participant:(BCHParticipant*) participant;
-(BOOL) isAvatarAvailable:(BCHParticipant*) participant;
-(void) uploadQueuedAvatar;
-(NSURL*) urlForAvatar:(NSString*) avatarId;

@end
