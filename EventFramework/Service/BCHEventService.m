//
//  BCHEventRatingService.m
//  EventsSchedule
//
//  Created by BC Holmes on 2016-03-03.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHEventService.h"

#import <UserNotifications/UserNotifications.h>

#import "BCHHttpHelper.h"
#import "BCHRank.h"


#define HOUR 60 * 60

@interface BCHEventService()

@property (nonatomic, strong) NSString* conventionId;
@property (nonatomic, strong) NSDate* lastFetchDate;
@property (nonatomic, strong) BCHPrimaryEvent* primaryEvent;
@property (nonnull, nonatomic, strong) NSManagedObjectContext* managedObjectContext;

@end

@implementation BCHEventService

-(instancetype) initWithConventionId:(NSString*) conventionId primaryEvent:(BCHPrimaryEvent*) primaryEvent managedObjectContext:(NSManagedObjectContext*) managedObjectContext {
    if (self = [super init]) {
        self.conventionId = conventionId;
        self.primaryEvent = primaryEvent;
        self.managedObjectContext = managedObjectContext;
    }
    return self;
}

-(void) fetchRankings:(BCHPrimaryEvent*) primaryEvent {
    
    if ([self isLongTimeSinceLastFetch]) {
    
        BCHHttpHelper* http = [BCHHttpHelper new];
        [http getJsonFromPath:[NSString stringWithFormat:@"/api/selections/%@", self.conventionId] callback:^(NSError* error, NSURLResponse* response, NSData* data) {
            
            if (error) {
                NSLog(@"HTTP rankings request returned error: %@", error.localizedDescription);
            } else if (data != nil) {
                self.lastFetchDate = [NSDate new];
                NSArray* result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                for (NSDictionary* entry in result) {
                    id eventId = [entry objectForKey:@"eventId"];
                    NSString* eventIdAsString = [NSString stringWithFormat:@"%@", eventId];
                    UnslackedEvent* event = [primaryEvent findEventForEventId:eventIdAsString];
                    if (event != nil && eventIdAsString != nil) {
                        [self.managedObjectContext performBlock:^{
                            BCHRank* rank = [self findRankForEventId:eventIdAsString];
                            if (rank == nil) {
                                rank = [self createRank:eventIdAsString];
                            }
                            [rank populateFromJson:[entry objectForKey:@"stats"]];
                            [self.managedObjectContext save:nil];
                        }];
                    } else {
                        NSLog(@"Event id %@ was not found", eventId);
                    }
                }
                dispatch_async(dispatch_get_main_queue(),^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:EventUpdates object:nil];
                });
            }
        }];
    } else {
        NSLog(@"It hasn't been that long since the last time we fetched rankings.");
    }
}

-(BCHRank*) createRank:(NSString*) eventId {
    BCHRank* result = [NSEntityDescription insertNewObjectForEntityForName:@"Rank" inManagedObjectContext:self.managedObjectContext];
    result.eventId = eventId;
    result.conventionId = self.conventionId;
    [result.managedObjectContext save:nil];
    return result;
}

-(BCHRank*) findRankForEventId:(NSString*) eventId {
    
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Rank"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(eventId == %@) AND (conventionId == %@)", eventId, self.conventionId];
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error == nil && results.count != 0) {
        NSLog(@"Rank record id=%@ found.", eventId);
        return (BCHRank*) results[0];
    } else {
        return nil;
    }
}

-(BCHSelection*) findOrCreateSelectionForEventId:(NSString*) eventId {
    
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Selection"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(eventId == %@) AND (conventionId == %@)", eventId, self.conventionId];
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error == nil && results.count != 0) {
        NSLog(@"Selection record id=%@ found.", eventId);
        return (BCHSelection*) results[0];
    } else {
        BCHSelection* result = [NSEntityDescription insertNewObjectForEntityForName:@"Selection" inManagedObjectContext:self.managedObjectContext];
        result.eventId = eventId;
        result.conventionId = self.conventionId;
        [result.managedObjectContext save:nil];
        return result;
    }
}


-(NSArray*) findAllSelections {
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Selection"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"conventionId == %@", self.conventionId];
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error == nil && results.count != 0) {
        return results;
    } else {
        return nil;
    }
}

-(void) readSelectionsFromDatabase {
    for (BCHSelection* selection in [self findAllSelections]) {
        // TODO: BCH: I don't think this works with nested events
        UnslackedEvent* event = [self.primaryEvent subEventForEventId:selection.eventId];
        if (event) {
            event.selection = selection;
        }
    }
}

- (NSArray*) topLevelEventsForDay:(NSString*) day {
    NSMutableArray* result = [[NSMutableArray alloc] init];

    NSArray* events = [self.primaryEvent.subEvents copy];
    for (UnslackedEvent* e in events) {
        if ([e.time.day isEqualToString:day]) {
            [result addObject:e];
        }
    }
    return result;
}


-(BOOL) isLongTimeSinceLastFetch {
    if (self.lastFetchDate == nil) {
        return YES;
    } else {
        int seconds = -(int)[self.lastFetchDate timeIntervalSinceNow];
        NSLog(@"Number of seconds %d", seconds);
        return seconds > HOUR;
    }
}

-(NSInteger) countOfItemsOnMySchedule {
    NSInteger result = 0;
    
    for (UnslackedEvent* event in [self.primaryEvent allLeafEvents]) {
        if (event.addedToSchedule) {
            result += 1;
        }
    }
    
    return result;
}

-(void) changeScheduledStatusOfEvent:(UnslackedEvent*) event added:(BOOL) added {
    [self.managedObjectContext performBlock:^{
        if (event != nil && event.selection == nil) {
            event.selection = [self findOrCreateSelectionForEventId:event.eventId];
        }
        event.selection.addedToSchedule = added;
        [[NSNotificationCenter defaultCenter] postNotificationName:MyScheduleChangedNotification object:event];
        
        if (added && [event.time.startTime compare:[NSDate new]] == NSOrderedAscending) {
            [self createNotification:event];
        } else if (!added) {
            [self removeNotification:event];
        }
        [self.managedObjectContext save:nil];
    }];
}

-(void) changeYayStatusOfEvent:(UnslackedEvent*) event status:(BOOL) yay {
    [self.managedObjectContext performBlock:^{
        if (event != nil && event.selection == nil) {
            event.selection = [self findOrCreateSelectionForEventId:event.eventId];
        }
        event.selection.yay = yay;

        BCHRank* rank = [self findRankForEventId:event.eventId];
        if (yay) {
            if (rank == nil) {
                rank = [self createRank:event.eventId];
            }
            rank.yayCount = [NSNumber numberWithInteger:[rank.yayCount integerValue] + 1];
            [rank.managedObjectContext save:nil];
        } else if ([rank.yayCount integerValue] > 0) {
            rank.yayCount = [NSNumber numberWithInteger:[rank.yayCount integerValue] - 1];
            [rank.managedObjectContext save:nil];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:EventChangedNotification object:event];
    }];
}

-(void) changeHighlightStatusOfEvent:(UnslackedEvent*) event status:(BOOL) highlight {
    [self.managedObjectContext performBlock:^{
        if (event != nil && event.selection == nil) {
            event.selection = [self findOrCreateSelectionForEventId:event.eventId];
        }
        event.selection.highlight = highlight;
        [self.managedObjectContext save:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:EventChangedNotification object:event];
    }];
}

-(void) removeNotification:(UnslackedEvent*) event {
    [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[ [self notificationIdForEventReminder:event] ]];
}

- (NSString* _Nonnull) notificationIdForEventReminder:(UnslackedEvent*) event {
    return [NSString stringWithFormat:@"%@-event-reminder-%@", self.conventionId, event.eventId];
}

-(void) createNotification:(UnslackedEvent*) event {
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"Event Reminder" arguments:nil];
    content.body = [NSString localizedUserNotificationStringForKey:event.fullTitleNoHtml arguments:nil];
    content.sound = [UNNotificationSound defaultSound];
    
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:event.time.timeZone];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond|NSCalendarUnitTimeZone fromDate:event.time.startTime];
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[self notificationIdForEventReminder:event]
                                                                          content:content trigger:trigger];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Event reminder notification succeeded");
        } else {
            NSLog(@"Event reminder notification failed: %@", error);
        }
    }];
}

-(NSArray*) allLeafEvents {
    return [self.primaryEvent allLeafEvents];
}

-(void) createEventNotification:(UnslackedEvent*) event {
    NSDate* notificationTime = event.time.startTime;
    notificationTime = [notificationTime dateByAddingTimeInterval:-2 * 60]; // two minutes prior to the event
    
    NSLog(@"NSDate:%@",notificationTime);
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    [calendar setTimeZone:event.time.timeZone];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond|NSCalendarUnitTimeZone fromDate:notificationTime];
    
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    content.title = event.fullTitleNoHtml;
    content.body = event.location.displayName;
    content.sound = [UNNotificationSound defaultSound];
    
    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];
    
    NSString* notificationId = [NSString stringWithFormat:@"%@-event-%@", self.conventionId, event.eventId];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notificationId content:content trigger:trigger];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"Local Notification failed: %@", error);
        }
    }];
}
@end
