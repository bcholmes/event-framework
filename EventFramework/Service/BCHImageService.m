//
//  BCHImageService.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/SDWebImagePrefetcher.h>
#import <Reachability/Reachability.h>
#import <AFNetworking/AFNetworking.h>
#import <UIDevice-Hardware/UIDevice-Hardware.h>

#import "BCHImageService.h"
#import "BCHHttpHelper.h"
#import "BCHParticipant.h"
#import "BCHCacheService.h"
#import "BCHTokenGenerator.h"
#import "BCHPrimaryEvent.h"

@interface BCHImageService ()

@property (nonatomic, strong) NSString* cachePath;
@property (nonatomic, strong) BCHCacheService* cacheService;
@property (nonatomic, strong) BCHTokenGenerator* tokenGenerator;
@property (nonatomic, strong) BCHPrimaryEvent* primaryEvent;

@end

@implementation BCHImageService

-(id) initWithConventionId:(NSString *)conventionId tokenGenerator:(BCHTokenGenerator*)tokenGenerator andCache:(BCHCacheService *)cacheService primaryEvent:(BCHPrimaryEvent*) event {
    if (self = [super init]) {
        self.tokenGenerator = tokenGenerator;
        self.cacheService = cacheService;
        self.cachePath = [NSString stringWithFormat:@"%@/Library/Caches/Images", NSHomeDirectory()];
        self.primaryEvent = event;
        
        BOOL isDir = NO;
        NSError *error;
        
        if (! [[NSFileManager defaultManager] fileExistsAtPath:self.cachePath isDirectory:&isDir] && isDir == NO) {
            [[NSFileManager defaultManager] createDirectoryAtPath:self.cachePath withIntermediateDirectories:YES attributes:nil error:&error];
        } else {
        }
    }
    return self;
}


+(NSString*) mimeTypeForImageData:(NSData*) data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
    case 0xFF:
        return @"image/jpeg";
    case 0x89:
        return @"image/png";
    case 0x47:
        return @"image/gif";
    case 0x49:
    case 0x4D:
        return @"image/tiff";
    default:
        return nil;
    }
}

-(NSString*) appName {
    NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    return [appName lowercaseString];
}
-(void) uploadQueuedAvatar {
    if ([self hasQueuedAvatar]) {
        NSLog(@"Upload new avatar");
        SDImageCache* myCache = [SDImageCache sharedImageCache];
        UIImage* displayImage = [myCache imageFromDiskCacheForKey:@"queuedAvatar"];

        NSData* data = UIImagePNGRepresentation(displayImage);
        NSString* base64Encoded = [data base64EncodedStringWithOptions:0];
        
        NSString* json = [NSString stringWithFormat:@"{ \"personId\": \"%@\", \"photo\": \"%@\", \"clientId\": \"%@\", \"mimeType\": \"%@\", \"convention\": \"%@\" }", self.tokenGenerator.personId, base64Encoded, self.tokenGenerator.clientId, [BCHImageService mimeTypeForImageData:data], [self appName]];

        [BCHHttpHelper sendJson:json toUrl:[NSURL URLWithString:@"https://ayizan.org/api/avatar"] headers:@{ @"Authorization" : [NSString stringWithFormat:@"Basic %@", self.tokenGenerator.token]} onSuccess:^(NSData* data, NSString* contentType) {
            
            NSLog(@"json %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSString* avatarId = [json objectForKey:@"avatarId"];
            NSLog(@"New avatar uploaded. Id = %@", avatarId);
            
            if (avatarId) {
                BCHParticipant* participant = [self.primaryEvent participantWithKey:self.tokenGenerator.personId];
                participant.avatarId = avatarId;
            }
            SDImageCache* myCache = [SDImageCache sharedImageCache];
            [myCache removeImageForKey:@"queuedAvatar"];

            
        } onError:nil];
    }
}


-(BOOL) hasQueuedAvatar {
    SDImageCache* myCache = [SDImageCache sharedImageCache];
    return [myCache diskImageExistsWithKey:@"queuedAvatar"];
}

-(void) uploadAvatar:(UIImage*) image {
    SDImageCache* myCache = [SDImageCache sharedImageCache];
    [myCache storeImage:image forKey:@"queuedAvatar"];

    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    if (reachability.currentReachabilityStatus != NotReachable) {
        [self uploadQueuedAvatar];
    }
}

-(AFHTTPSessionManager*) sessionManager {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:[[UIDevice currentDevice] modelName] forHTTPHeaderField: @"X-UA-Device"];
    return manager;
}

-(void) fetchLatestAvatars {
    
    Entry* entry = [self.cacheService entryForKey:@"avatars"];
    
    NSDictionary* headers = nil;
    if (entry.data) {
        headers = [entry createRequestHeaders];
    }
    
    AFHTTPSessionManager* manager = [self sessionManager];
    [manager GET:[NSString stringWithFormat:@"https://ayizan.org/api/avatar/%@", [self appName]] parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]]) {
            entry.data = [NSJSONSerialization dataWithJSONObject:responseObject options:kNilOptions error:nil];
            entry.lastModified = [headers objectForKey:@"Last-Modified"];
            entry.eTag = [headers objectForKey:@"Etag"];
            [self.cacheService writeCache];
            
            NSDictionary* result = (NSDictionary*) responseObject;
            NSArray* records = [result objectForKey:@"records"];
            NSMutableArray* urls = [NSMutableArray new];
            for (NSDictionary* record in records) {
                
                NSString* avatarId = [record objectForKey:@"avatarId"];
                NSURL* imageUrl = [[BCHHttpHelper new] constructUrl:[NSString stringWithFormat:@"/api/avatarById/%@/%@", [self appName], avatarId]];
                
                [urls addObject:imageUrl];
                
                NSString* key = [record objectForKey:@"id"];
                BCHParticipant* participant = [self.primaryEvent participantWithKey:key];
                if (![participant.avatarId isEqualToString:avatarId]) {
                    NSLog(@"assigning new avatar id to participant %@", participant.key);
                    participant.avatarId = avatarId;
                }
            }
            [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:urls];
        } else {
            NSLog(@"The response to the avatars request is somehow nil");
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"attempt to download avatar info failed: %@", error.localizedDescription);
    }];
}

-(NSURL*) urlForAvatar:(NSString*) avatarId {
    return [[BCHHttpHelper new] constructUrl:[NSString stringWithFormat:@"/api/avatarById/%@/%@", [self appName], avatarId]];
}

-(void) writeImageToFileSystem:(UIImage*) image participant:(BCHParticipant*) participant {
    if (participant) {
        [self writeImageToFileSystem:image key:participant.key];
    }
}

-(void) writeImageToFileSystem:(UIImage*) image key:(NSString*) participantKey {
    SDImageCache* myCache = [SDImageCache sharedImageCache];
    [myCache storeImage:image forKey:@"queuedAvatar"];
}

-(NSString*) pathOfAvatar:(BCHParticipant*) participant {
    return [self.cachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"person%@", participant.key]];
}

-(NSString*) getAvatarAsEncodedHtmlImage:(BCHParticipant*) participant {
    
    if ([participant.key isEqualToString:self.tokenGenerator.personId] && [self hasQueuedAvatar]) {
        SDImageCache* myCache = [SDImageCache sharedImageCache];
        UIImage* displayImage = [myCache imageFromDiskCacheForKey:@"queuedAvatar"];
        
        NSData* data = UIImagePNGRepresentation(displayImage);
        NSString* mimeType = [BCHImageService mimeTypeForImageData:data];
        NSString* base64String = [data base64EncodedStringWithOptions:0];
        
        return [NSString stringWithFormat:@"data:%@;base64,%@", mimeType, base64String];
        
    } else if (participant.avatarId != nil) {
        NSString* url = [[self urlForAvatar:participant.avatarId] absoluteString];
        SDImageCache* myCache = [SDImageCache sharedImageCache];
        UIImage* displayImage = [myCache imageFromDiskCacheForKey:url];
        
        NSData* data = UIImagePNGRepresentation(displayImage);
        NSString* mimeType = [BCHImageService mimeTypeForImageData:data];
        NSString* base64String = [data base64EncodedStringWithOptions:0];
        
        return [NSString stringWithFormat:@"data:%@;base64,%@", mimeType, base64String];

    } else {
        return nil;
    }
}

-(BOOL) isAvatarAvailable:(BCHParticipant *)participant {
    if (participant.isAnonymous) {
        return NO;
    } else if ([participant.key isEqualToString:self.tokenGenerator.personId] && [self hasQueuedAvatar]) {
        return YES;
    } else if (participant.avatarId != nil) {
        return YES;
    } else {
        return NO;
    }
}

@end
