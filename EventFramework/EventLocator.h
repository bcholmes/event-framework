//
//  EventLocator.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#ifndef EventsSchedule_EventLocator_h
#define EventsSchedule_EventLocator_h

@protocol EventLocator

- (UnslackedEvent*)eventById:(NSString*)eventId;

@end


#endif
