//
//  UnslackedApiClient.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BCHApiClient.h"
#import "EventJSONParser.h"
#import "BCHContentCollection.h"
#import "BCHHttpHelper.h"
#import "BCHRank.h"
#import "BCHCacheService.h"

@interface BCHApiClient()

@property (nonatomic, strong) BCHCacheService* cacheService;

@end

@implementation BCHApiClient

- (id) initWithCache:(BCHCacheService*) cacheService {
	if ((self = [super init])) {
        self.cacheService = cacheService;
	}
	return self;
}

- (NSString*) textNamed:(NSString*)path {
    NSString *result = nil;
    NSString *filePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    if (filePath) {
        NSError *error = nil;
        result = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"%@: error=%@", NSStringFromSelector(_cmd), [error localizedDescription]);
        }
    }
    return result;
}

-(BOOL) fetchLatestUpdates:(NSString*) eventId
{
    NSLog(@"Sending HTTP request for %@", eventId);
    NSURLResponse *response;
    
	Entry *entry = [self.cacheService entryForKey:eventId];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://ayizan.org/wissched/%@updates.json", eventId]]
                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                       timeoutInterval:60.0];
    if (entry.lastModified) {
        [request setValue:entry.lastModified forHTTPHeaderField:@"If-Modified-Since"];
    }
    if (entry.eTag) {
        [request setValue:entry.eTag forHTTPHeaderField:@"If-None-Match"];
    }
    NSError* error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    if (data != nil) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"event updates status code: %lu", (unsigned long) httpResponse.statusCode);
        if (httpResponse.statusCode == 200) {
            entry.data = data;
            NSDictionary *headers = [((NSHTTPURLResponse*) response) allHeaderFields];

            entry.lastModified = [headers objectForKey:@"Last-Modified"];
            entry.eTag = [headers objectForKey:@"Etag"];
            [self.cacheService writeCache];
            return YES;
        } else {
            return NO;
        }
    } else {
        NSLog(@"http error : %@", error.localizedDescription);
        return NO;
    }
}

- (id) parseJson:(NSString*)json {
    return json != nil ? [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil] : nil;
}

- (id) parseJsonFile:(NSString*)name {
    return [self parseJson:[self textNamed:name]];
}


-(void) processUpdates:(BCHPrimaryEvent*) event usingLocations:(NSDictionary*) locations{
    Entry* entry = [self.cacheService entryForKey:event.eventId];
    if (entry && entry.data) {
        NSString* json = [[NSString alloc] initWithData:entry.data encoding:NSASCIIStringEncoding];
        NSDictionary *dictionary = [self parseJson:json];
        EventJSONParser* parser = [EventJSONParser new];
        [parser loadJson:dictionary toPrimaryEvent:event usingLocations:locations];
        dispatch_async(dispatch_get_main_queue(),^{
            [[NSNotificationCenter defaultCenter] postNotificationName:EventUpdates object:event];
        });
    }
}

-(BCHContentCollection*) getContent:(NSString*) name {
    if (name != nil && name.length > 0) {
        NSLog(@"parse content json");
        return [BCHContentCollection parseJson:[self parseJsonFile:name]];
    } else {
        return nil;
    }
}

-(NSDictionary*) getLocations {
    NSLog(@"get locations");
    
    NSDictionary *dictionary = [self parseJsonFile:@"locations.json"];
    return [BCHLocation parseLocations:dictionary];
}


-(BCHPrimaryEvent*) getTopLevelEventById:(NSString*) eventId  usingLocations:(NSDictionary*) locations{
    NSLog(@"get events for eventId %@", eventId);
    
    NSDictionary *dictionary = [self parseJsonFile:[NSString stringWithFormat:@"%@.json",eventId]];
    EventJSONParser* parser = [EventJSONParser new];
    BCHPrimaryEvent* result = [parser parseEvent:dictionary usingLocations:locations];
    if (result.eventId == nil) {
        result.eventId = eventId;
    }
    return result;
}

@end
