//
//  EventJSONParser.h
//  unslacked-app
//
//  Created by BC Holmes on 11-10-27.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCHPrimaryEvent.h"

@interface EventJSONParser : NSObject

-(BCHPrimaryEvent*) parseEvent:(NSDictionary*) json usingLocations:(NSDictionary*) locations;
-(void) loadJson:(NSDictionary*) json toPrimaryEvent:(BCHPrimaryEvent*) event usingLocations:(NSDictionary*) locations;

@end
