//
//  UnslackedTimeRange.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-10.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UnslackedTimeRange : NSObject<NSCopying> {
}

@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDate *endTime;
@property (weak, nonatomic, readonly) NSString* day;
@property (readonly) BOOL isMidnightStart;
@property (weak, nonatomic, readonly) NSString* timeAsString;
@property (nonatomic, readonly) NSTimeInterval delta;
@property (nonatomic, strong) NSTimeZone* timeZone;
-(NSString *) removeDuplicates:(NSString *) substring fromString:(NSString *) fromString;
-(NSString *) asString;
-(BOOL) overlapsTimeRange:(UnslackedTimeRange*) timeRange;
-(BOOL) overlapsDate:(NSDate*) date;
-(UnslackedTimeRange*) prefixAfterDifference:(UnslackedTimeRange*) time;
- (NSComparisonResult)compare:(UnslackedTimeRange*) otherObject;

+(NSString*) formatAsDayOfWeek:(NSDate*) date withTimeZone:(NSTimeZone*) timeZone;
+(UnslackedTimeRange*) fromJson:(NSDictionary*) json;
+(NSTimeZone*) conferenceTimeZone;
+(void) setConferenceTimeZone:(NSTimeZone*) conferenceTimeZone;

@end
