
//
//  BCHScheduleEntry.h
//  Pods
//
//  Created by BC Holmes on 2019-05-04.
//

#ifndef BCHScheduleEntry_h
#define BCHScheduleEntry_h

#import <UIKit/UIKit.h>

#import "UnslackedTimeRange.h"

@protocol BCHScheduleEntry <NSObject>

@property (nonatomic, nonnull, readonly) UIImage* icon;
@property (nonatomic, nullable, readonly) NSString* listSubtitle;
@property (nonatomic, nullable, readonly) NSString* fullTitleAsHtml;
@property (nonatomic, nullable, strong) UnslackedTimeRange* time;
@property (nonatomic, readonly) BOOL addedToSchedule;
@property (nonatomic) BOOL highlighted;
@property (readonly) BOOL isMyEvent;
@property (readonly) BOOL visible;

-(BOOL) matchesString:(NSString* _Nonnull) searchString;
-(NSComparisonResult) compare:(NSObject<BCHScheduleEntry>* _Nonnull) otherObject;

@end

#endif /* BCHScheduleEntry_h */
