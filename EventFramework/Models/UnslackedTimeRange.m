//
//  UnslackedTimeRange.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-10.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "UnslackedTimeRange.h"

@interface NSString (my_substr_search)
- (unsigned) countOccurencesOf: (NSString *)subString;
@end


@implementation UnslackedTimeRange

static NSTimeZone* _conferenceTimeZone = nil;

-(id) copyWithZone: (NSZone *) zone
{
    UnslackedTimeRange *copy = [[UnslackedTimeRange alloc] init];
    copy.startTime = self.startTime;
    copy.endTime = self.endTime;
    copy.timeZone = self.timeZone;
    return copy;
}

- (NSUInteger)hash {
    NSUInteger result = 17;
    if (self.startTime != nil) {
        result |= [self.startTime hash];
    }
    if (self.endTime != nil) {
        result |= [self.endTime hash];
    }
    
    return result;
}


- (NSComparisonResult)compare:(UnslackedTimeRange *)otherObject {
    if (self.startTime.timeIntervalSince1970 == otherObject.startTime.timeIntervalSince1970) {
        return [self.endTime compare:otherObject.endTime];
    } else {
        return [self.startTime compare:otherObject.startTime];
    }
}

- (BOOL)isEqual:(id)anObject {
    if (![anObject isKindOfClass:[UnslackedTimeRange class]]) 
    {
        return NO;
    }
    else 
    {
        UnslackedTimeRange *other = (UnslackedTimeRange *)anObject;
        return [self.startTime isEqual:other.startTime] && [self.endTime isEqual:other.endTime];
    }
}

+(NSString*) formatAsDayOfWeek:(NSDate*) date withTimeZone:(NSTimeZone*) timeZone {
    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setTimeZone:timeZone];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dayFormatter setLocale:usLocale];
    [dayFormatter setDateFormat:@"EEEE"];
    NSTimeInterval twelveHoursEarlier = (-12 * 60 * 60);
    return [UnslackedTimeRange isMidnightStart:date timeZone:timeZone] ? [dayFormatter stringFromDate:[date dateByAddingTimeInterval:twelveHoursEarlier]] : [dayFormatter stringFromDate:date];
    
}


-(NSString*) day {
    return [UnslackedTimeRange formatAsDayOfWeek:self.startTime withTimeZone:self.timeZone];
}

-(BOOL) isMidnightStart {
    return [UnslackedTimeRange isMidnightStart:self.startTime timeZone:self.timeZone];
}

+(BOOL) isMidnightStart:(NSDate*) date timeZone:(NSTimeZone*) timeZone {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:timeZone];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:@"h:mmaa"];
    
    return [[dateFormatter stringFromDate:date] isEqualToString:@"12:00AM"];
}

-(NSString *) timeAsString {
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:self.timeZone];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:@"h:mmaa"];
    
    NSMutableString *result = [NSMutableString stringWithString:@""];
    if (self.startTime != nil) {
        [result appendString:[dateFormatter stringFromDate:self.startTime]];
    }
    if (self.endTime != nil) {
        if ([result length] > 0) {
            [result appendString:@"-"];
        }
        [result appendString:[dateFormatter stringFromDate:self.endTime]];
    }
    
    
    NSString* temp = [result stringByReplacingOccurrencesOfString:@"12:00PM" withString:@"Noon"];
    temp = [temp stringByReplacingOccurrencesOfString:@"12:00AM" withString:@"Midnight"];
    temp = [temp stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    return [self removeDuplicates:@"PM" fromString:[self removeDuplicates:@"AM" fromString:temp]];
}
-(NSString *) asString
{
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];

    NSDateFormatter *dayFormatter = [[NSDateFormatter alloc] init];
    [dayFormatter setTimeZone:self.timeZone];
    [dayFormatter setLocale:usLocale];
    [dayFormatter setDateFormat:@"EEEE"];
    
    NSMutableString *result = [NSMutableString stringWithString:@""];
    [result appendString:[dayFormatter stringFromDate:self.startTime]];
    [result appendString:@", "];
    [result appendString:[self timeAsString]];
    return result;
}
-(NSString *) removeDuplicates:(NSString *) substring fromString:(NSString *) fromString
{
    for (int i = [fromString countOccurencesOf:substring] -1; i > 0; i--)
    {
        fromString = [fromString stringByReplacingCharactersInRange:[fromString rangeOfString:substring] withString:@""];
    }
    return fromString;
}

-(BOOL) overlapsDate:(NSDate *)date {
    if ([date compare:self.startTime] >= 0 && [date compare:self.endTime] <= 0) {
        return true;
    } else {
        return false;
    }
}
-(BOOL) overlapsTimeRange:(UnslackedTimeRange*) timeRange {
    if ([timeRange.startTime compare:self.startTime] >= 0 && [timeRange.startTime compare:self.endTime] < 0) {
        return true;
    } else if ([timeRange.endTime compare:self.startTime] > 0 && [timeRange.endTime compare:self.endTime] <= 0) {
        return true;
    } else if ([timeRange.startTime compare:self.startTime] <= 0 && [timeRange.endTime compare:self.endTime] > 0) {
        return true;
    } else {
        return false;
    }
}

-(UnslackedTimeRange*) prefixAfterDifference:(UnslackedTimeRange*) timeRange {
    if ([timeRange.startTime compare:self.startTime] == NSOrderedDescending) {
        UnslackedTimeRange* result = [[UnslackedTimeRange alloc] init];
        result.startTime = self.startTime;
        result.endTime = [self.endTime earlierDate:timeRange.startTime];
        result.timeZone = self.timeZone;
        return result;
    } else {
        return nil;
    }
}

-(NSTimeZone*) timeZone {
    if (_timeZone == nil) {
        _timeZone = [UnslackedTimeRange conferenceTimeZone];
    }
    return _timeZone;
}

-(NSTimeInterval) delta {
    return [self.endTime timeIntervalSinceDate:self.startTime];
}

+(UnslackedTimeRange*) fromJson:(NSDictionary*) json {
    UnslackedTimeRange* result = [[UnslackedTimeRange alloc] init];
    NSString* temp = [json objectForKey:@"startTime"];
    if (temp != nil) {
        result.startTime = [UnslackedTimeRange parseIsoDate:temp];
    }
    temp = [json objectForKey:@"endTime"];
    if (temp != nil) {
        result.endTime = [UnslackedTimeRange parseIsoDate:temp];
    }
    return result.startTime != nil && result.endTime != nil ? result : nil;
}

+(NSDate*) parseIsoDate:(NSString*) date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    dateFormatter.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate* result = [dateFormatter dateFromString:date];
    
    if (result == nil) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mmZ"];
        result = [dateFormatter dateFromString:date];
    }
    if (result == nil) {
        NSLog(@"Date %@ parses to %@", date, result);
    }
    return result;
}

+(NSTimeZone*) conferenceTimeZone {
    return _conferenceTimeZone;
}

+(void) setConferenceTimeZone:(NSTimeZone*) conferenceTimeZone {
    _conferenceTimeZone = conferenceTimeZone;
}

@end

@implementation NSString (my_substring_search)
- (unsigned) countOccurencesOf: (NSString *)subString {
    unsigned count = 0;
    unsigned long myLength = [self length];
    NSRange uncheckedRange = NSMakeRange(0, myLength);
    for(;;) {
        NSRange foundAtRange = [self rangeOfString:subString
                                           options:0
                                             range:uncheckedRange];
        if (foundAtRange.location == NSNotFound) return count;
        unsigned long newLocation = NSMaxRange(foundAtRange);
        uncheckedRange = NSMakeRange(newLocation, myLength-newLocation);
        count++;
    }
}

@end
