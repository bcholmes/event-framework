//
//  UnslackedEvent.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-01.
//  Copyright 2011-2014 Ayizan Studios. All rights reserved.
//

#import <Twitter/TWTweetComposeViewController.h>
#import "UnslackedEvent.h"
#import "BCHParticipation.h"
#import "BaseAppDelegate.h"
#import "NSAttributedString+Html.h"
#import "BCHRank.h" 

@implementation UnslackedEvent

-(id) init
{
    if (self = [super init]) {
        self.subEventMap = [[NSMutableDictionary alloc] init];
        self.participation = [[NSMutableArray alloc] init];
        self.hashTag = @"";
    }
    return self;
}
-(void) addSubEvent:(UnslackedEvent *)event
{
    [self.subEventMap setObject:event forKey:event.eventId];
}

-(BOOL) visible {
    return !self.isPrivate || self.isMyEvent;
}

-(NSString *)fullTitle
{
    if (self.externalId == nil || self.externalId.length == 0) {
        return self.title;
    } else {
        return [[self.externalId stringByAppendingString:@" "] stringByAppendingString:self.title];
    }
}

-(BOOL) isMyEvent {
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (delegate.personId == nil) {
        return NO;
    } else {
        BCHParticipation* participant = [self participationWithKey:delegate.personId];
        return participant != nil;
    }
}

-(NSString *)fullTitleNoHtml
{
    NSMutableString* fullTitle = [self.fullTitle mutableCopy];
    
    NSRange range = [fullTitle rangeOfString:@"<"];
    while (range.location != NSNotFound) {
        NSRange range2 = [fullTitle rangeOfString:@">"];
        
        range.length = range2.location - range.location + 1;
        [fullTitle deleteCharactersInRange:range];
        range = [fullTitle rangeOfString:@"<"];
    }
    return fullTitle;
}
-(NSString *)trackCategory
{
    if (self.track && [self.track length] > 0) {
        return self.track;
    } else if (self.type && [self.type length] > 0 && ![self.type isEqualToString:@"Event"]) {
        return self.type;
    } else {
        return nil;
    }
}

- (NSString*) fullTitleAsHtml {
    NSMutableString* html = [[NSMutableString alloc] init];
    if (self.externalId.length > 0) {
        [html appendString:[NSString stringWithFormat:@"<b>%@</b> ", self.externalId]];
    }
    
    if (self.title.length > 0) {
        [html appendString:self.title];
    }
    if (self.cancelled) {
        [html insertString:@"<s>" atIndex:0];
        [html appendString:@"</s>"];
    }
    return [NSString stringWithString:html];
}

-(NSString*) subTitleAsHtml {
    return self.trackCategory;
}

- (NSArray*)allLeafEvents {
    NSLog(@"finding all leaf nodes of %@", self.title);
    NSMutableArray* result = [[NSMutableArray alloc] init];
    for (UnslackedEvent* e in [NSArray arrayWithArray:[self.subEventMap allValues]]) {
        if ([e.subEventMap count] > 0) {
            [result addObjectsFromArray:[e allLeafEvents]];
        } else {
            [result addObject:e];
        }
    }
    return result;
}
-(UnslackedEvent*) subEventForEventId:(NSString*) eventId {
    return [self.subEventMap objectForKey:eventId];
}
-(UnslackedEvent*) findEventForEventId:(NSString*) eventId {
    UnslackedEvent* result = [self subEventForEventId:eventId];
    if (result == nil) {
        for (UnslackedEvent* e in [self.subEventMap allValues]) {
            result = [e findEventForEventId:eventId];
            if (result != nil) {
                break;
            }
        }
    }
    
    return result;
}
-(NSString*) listSubtitle {
    return self.locationAndTrack;
}
-(NSString *)locationAndTrack {
    if (self.trackCategory && [self.trackCategory length] > 0 && self.location.isRealLocation) {
        return [NSString stringWithFormat:@"%@ | %@", self.location.displayName, self.trackCategory];
    } else if (self.trackCategory && [self.trackCategory length] > 0) {
        return self.trackCategory;
    } else if (self.location.isRealLocation) {
        return self.location.displayName;
    } else {
        return @"";
    }
}
-(NSString *)subTitle
{
    NSMutableString *listText = [NSMutableString stringWithString:(self.location != nil && self.location.isRealLocation ? self.location.displayName : @"")];
    if (self.time != nil) {
        if (listText.length > 0) {
            [listText appendString:@" | "];
        }
        [listText appendString:self.timeAsString];
    }
    return listText;
}

-(NSArray*) subEvents {
    return [self.subEventMap allValues];
}

-(NSString*) timeAsString {
    return [self.time asString];
}

-(NSString*) fullIdentifier {
    return [NSString stringWithFormat:@"Event#%@", self.eventId];
}

-(NSComparisonResult) compare:(NSObject<BCHScheduleEntry>*) otherObject {

    if ([otherObject isKindOfClass:[UnslackedEvent class]]) {
        return [self compareEvent:(UnslackedEvent*) otherObject];
    } else if ([self.time compare:otherObject.time] != NSOrderedSame) {
        return [self.time compare:otherObject.time];
    } else {
        return NSOrderedAscending;
    }
}
-(NSComparisonResult) compareEvent:(UnslackedEvent*)otherObject {
    if ([self.time compare:otherObject.time] != NSOrderedSame) {
        return [self.time compare:otherObject.time];
    } else if ([self.location compare:otherObject.location] != NSOrderedSame) {
        return [self.location compare:otherObject.location];
    } else if (self.externalId != nil && otherObject.externalId != nil) {
        NSNumber* externalId1 = [NSNumber numberWithInteger:[self.externalId integerValue]];
        NSNumber* externalId2 = [NSNumber numberWithInteger:[otherObject.externalId integerValue]];
        return [externalId1 compare:externalId2];
    } else {
        return [self.eventId compare:otherObject.eventId];
    }
}

-(BOOL) matchesStringInParticipantName:(NSString*) searchString {
    BOOL result = NO;
    for (BCHParticipation* participation in self.participation) {
        if ([participation.participant.name rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
            result = YES;
            break;
        }
    }
    return result;
}
-(BOOL) matchesString:(NSString*) searchString {
    if ([self.title rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else if ([self.eventDescription rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else if ([self matchesStringInParticipantName:searchString]) {
        return YES;
    } else {
        return NO;
    }
}

- (BCHParticipation*) participationWithKey:(NSString*) participantId {
    BCHParticipation* result = nil;
    for (BCHParticipation* participation in self.participation) {
        if ([participation.participant.key isEqualToString:participantId]) {
            result = participation;
            break;
        }
    }
    return result;
}

-(UIImage*) icon {
    if ([self.title containsString:@"Bake Sale"]) {
        return [UIImage imageNamed:@"Icon_bakesale"];
    } else if ([self.title containsString:@"Dinner"]) {
        return [UIImage imageNamed:@"Icon_meal"];
    } else if ([self.track isEqualToString:@"Teen Programming"]) {
        return [UIImage imageNamed:@"Icon_teen"];
    } else if ([self.type isEqualToString:@"Workshop"]) {
        return [UIImage imageNamed:@"Icon_workshop"];
    } else if ([self.type isEqualToString:@"Panel"] || [self.type isEqualToString:@"Program"]) {
        return [UIImage imageNamed:@"Icon_panel"];
    } else if ([self.track isEqualToString:@"Teen Programming"]) {
        return [UIImage imageNamed:@"Icon_teen"];
    } else if ([self.type isEqualToString:@"Reading"]) {
        return [UIImage imageNamed:@"Icon_reading"];
    } else if ([self.type isEqualToString:@"Party"]) {
        return [UIImage imageNamed:@"Icon_party"];
    } else if ([self.type isEqualToString:@"Kids"]) {
        return [UIImage imageNamed:@"Icon_event_kids"];
    } else if ([self.type isEqualToString:@"Academic"]) {
        return [UIImage imageNamed:@"Icon_academic"];
    } else if ([self.type isEqualToString:@"Meal"]) {
        return [UIImage imageNamed:@"Icon_meal"];
    } else if ([self.type isEqualToString:@"Gaming"]) {
        return [UIImage imageNamed:@"Icon_gaming"];
    } else {
        return [UIImage imageNamed:@"Icon_special"];
    }
}

-(BOOL) highlighted {
    return self.selection.highlight;
}

-(BOOL) addedToSchedule {
    return self.selection.addedToSchedule;
}

@end

