//
//  UnslackedEventTable.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCHScheduleEntryTable.h"
#import "UnslackedEvent.h"

@implementation BCHScheduleEntryTable

-(id) init {
    if (self = [super init]) {
        self.dictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(BOOL) isEmpty {
    return self.dictionary.count == 0;
}

-(void) addEvent:(NSObject<BCHScheduleEntry>*) event {
    if (event != nil && event.time != nil && event.visible) {
        if ([self.dictionary objectForKey:event.time] == nil) {
            NSMutableArray *value = [[NSMutableArray alloc] initWithObjects:event, nil];
            [self.dictionary setObject:value forKey:event.time];
        } else {
            NSMutableArray *value = [self.dictionary objectForKey:event.time];
            [value addObject:event];
        }
    }
}
-(NSArray*) sectionKeys {
    NSArray* keys = [self.dictionary allKeys];
    return [keys sortedArrayUsingSelector:@selector(compare:)];
}

-(NSDate*) createPseudoDate:(NSDate*) baseDate withDate:(NSDate*) dayOfMonthPart andTimeZone:(NSTimeZone*) tz {
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc ] init];
    [dateFormatter setTimeZone:tz];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString* date = [dateFormatter stringFromDate:dayOfMonthPart];
    
    NSDateFormatter* timeFormatter = [[NSDateFormatter alloc ] init];
    [timeFormatter setTimeZone:tz];
    [timeFormatter setLocale:usLocale];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    
    NSString* time = [timeFormatter stringFromDate:baseDate];
    
    NSDateFormatter* dateTimeFormatter = [[NSDateFormatter alloc ] init];
    [dateTimeFormatter setTimeZone:tz];
    [dateTimeFormatter setLocale:usLocale];
    [dateTimeFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", date, time]);
    
    return [dateTimeFormatter dateFromString:[NSString stringWithFormat:@"%@ %@", date, time]];
}

-(UnslackedTimeRange*) closestTimeRangeToNow:(NSTimeZone*) tz {
    NSArray* keys = [self sectionKeys];
    NSDate* now = [NSDate new];
    NSString* today = [UnslackedTimeRange formatAsDayOfWeek:now withTimeZone:tz];
    UnslackedTimeRange* result = nil;
    
    NSDate* pseudoNow = nil;
    
    for (UnslackedTimeRange* key in keys) {
        if ([today isEqualToString:key.day]) {
            if (pseudoNow == nil) {
                pseudoNow = [self createPseudoDate:now withDate:key.startTime andTimeZone:tz];
            }
            
            if (result == nil) {
                result = key;
            }
            
            if ([pseudoNow compare:key.startTime] ==  NSOrderedAscending) {
                if (![result overlapsDate:pseudoNow]) {
                    result = key;
                }
                break;
            } else if ([key overlapsDate:pseudoNow] && ![result overlapsDate:pseudoNow]) {
                result = key;
            }
        }
    }
    
    return result;
}


-(NSArray*) allDays {
    NSArray* keys = [self sectionKeys];
    NSMutableArray* result = [[NSMutableArray alloc] init];
    
    for (UnslackedTimeRange* key in keys) {
        NSString* day = key.day;
        if (![result containsObject:day]) {
            [result addObject:day];
        }
    }
    
    return result;
}

-(NSArray*) sectionForIndex:(NSInteger)sectionNumber {
    NSArray* keys = [self sectionKeys];
    return [self.dictionary objectForKey:[keys objectAtIndex:sectionNumber]];
}

-(NSArray*) sectionForIndex:(NSInteger) sectionNumber accessType:(EventTableAccessType) accessType {
    NSArray* keys = [self sectionKeys];
    NSArray* events = [self.dictionary objectForKey:[keys objectAtIndex:sectionNumber]];
    if (accessType == EventTableAccessTypeAll) {
        return events;
    } else {
        NSMutableArray* result = [[NSMutableArray alloc] init];
        for (NSObject<BCHScheduleEntry>* event in events) {
            if (event.addedToSchedule) {
                [result addObject:event];
            }
        }
        return result;
    }
}

-(NSObject<BCHScheduleEntry>*) entryAtIndexPath:(NSIndexPath *) indexPath accessType:(EventTableAccessType) accessType{
    NSArray* section = [self sectionForIndex:indexPath.section accessType:accessType];
    return [section count] <= indexPath.row ? nil : [section objectAtIndex:indexPath.row];
}

-(NSInteger) sectionNumberForRange:(UnslackedTimeRange*) timeRange {
    NSArray* keys = [self sectionKeys];
    NSInteger sectionNumber = [keys indexOfObject:timeRange];
    return sectionNumber;
}

-(NSInteger) sectionNumberFor:(NSObject<BCHScheduleEntry>*) event {
    NSArray* keys = [self sectionKeys];
    NSInteger sectionNumber = [keys indexOfObject:event.time];
    return sectionNumber;
}

-(NSInteger) sectionCount {
    return self.dictionary.count;
}

-(NSIndexPath*) indexPathFor:(NSObject<BCHScheduleEntry>*) event accessType:(EventTableAccessType) accessType {
    NSArray* keys = [self sectionKeys];
    NSInteger sectionNumber = [keys indexOfObject:event.time];
    if (sectionNumber == NSNotFound) {
        return nil;
    } else {
        NSInteger row = [[self sectionForIndex:sectionNumber accessType:accessType] indexOfObject:event];
        return row == NSNotFound ? nil : [NSIndexPath indexPathForRow:row inSection:sectionNumber];
    }
}

-(BCHScheduleEntryTable*) eventsContainingSearchString:(NSString*) searchString {
    
    BCHScheduleEntryTable* result = [[BCHScheduleEntryTable alloc] init];
    for (id key in self.dictionary) {
        NSArray* events = [self.dictionary objectForKey:key];
        for (NSObject<BCHScheduleEntry>* event in events) {
            if ([event matchesString:searchString]) {
                [result addEvent:event];
            }
        }
    }

    return result;
}

-(BCHScheduleEntryTable*) filteredByEventType:(NSString *)eventType {
    BCHScheduleEntryTable* eventTable = [[BCHScheduleEntryTable alloc] init];
    
    for (UnslackedTimeRange* time in self.sectionKeys) {
        NSArray* temp = [self.dictionary objectForKey:time];
        for (NSObject<BCHScheduleEntry>* event in temp) {
            if ([event isKindOfClass:[UnslackedEvent class]] && [((UnslackedEvent*) event).type isEqualToString:eventType]) {
                [eventTable addEvent:event];
            }
        }
    }

    return eventTable;
}

+(BCHScheduleEntryTable*) tableFromEvents:(NSArray*) events {
    BCHScheduleEntryTable* eventTable = [[BCHScheduleEntryTable alloc] init];
    NSArray* temp = [events sortedArrayUsingSelector:@selector(compare:)];
    for (NSObject<BCHScheduleEntry>* event in temp) {
        [eventTable addEvent:event];
    }
    return eventTable;
}


@end
