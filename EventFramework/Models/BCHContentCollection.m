//
//  UnslackedContentCollection.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-09.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "BCHContentCollection.h"
#import "BCHEventFilter.h"
#import "BCHSimpleList.h"
#import "SubMenu.h"

@interface BCHContentCollection()

@property (nonatomic, strong) NSArray* items;

@end

@implementation BCHContentCollection

-(id) init {
    if (self = [super init]) {
        self.dictionary = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void) addContent:(BCHPage*) page forKey:(NSString*) key {
    [self.dictionary setObject:page forKey:key];
}
-(BCHPage*) contentPageForKey:(NSString*) key {
     return [self.dictionary objectForKey:key];
}
-(BCHPage*) contentPageForLink:(NSString*) link {
    BCHPage* result = nil;
    for (Renderable* item in self.dictionary.allValues) {
        if ([item isKindOfClass:[BCHPage class]]) {
            BCHPage* page = (BCHPage*) item;
            if ([page.content isEqualToString:link] || [[page.content stringByDeletingPathExtension] isEqualToString:link]) {
                result = page;
                break;
            }
        }
    }
    return result;
}

-(id<ContentItem>) contentForKey:(NSString*) key {
    id<ContentItem> result = nil;
    for (id<ContentItem> item in self.items) {
        if ([item.key isEqualToString:key]) {
            result = item;
            break;
        }
    }
    return result;
}

+(NSArray*) processContentFrom:(NSArray*) pages intoContentCollection:(BCHContentCollection*) collection {
    NSMutableArray* array = [[NSMutableArray alloc] init];
    for (NSDictionary* entry in pages) {
        NSString* key = [entry objectForKey:@"resource"];
        if (key != nil) {
            BCHPage* page = [[BCHPage alloc] initWithFileName:key andTitle:[entry objectForKey:@"title"]];
            if ([entry objectForKey:@"banner"] != nil) {
                page.banner = [entry objectForKey:@"banner"];
            }
            [collection addContent:page forKey:key];
            [array addObject:page];
        } else if ([entry objectForKey:@"content"] != nil) {
            NSArray* subPages = [entry objectForKey:@"content"];
            SubMenu* subMenu = [[SubMenu alloc] init];
            subMenu.title = [entry objectForKey:@"title"];
            subMenu.items = [self processContentFrom:subPages intoContentCollection:collection];
            [array addObject:subMenu];
        } else if ([entry objectForKey:@"filter"] != nil) {
            BCHEventFilter* filter = [[BCHEventFilter alloc] init];
            filter.title = [entry objectForKey:@"title"];
            NSDictionary* temp = [entry objectForKey:@"filter"];
            filter.eventType = [temp objectForKey:@"eventType"];
            [array addObject:filter];
        } else if ([entry objectForKey:@"list"] != nil) {
            BCHSimpleList* list = [[BCHSimpleList alloc] initWithFileName:[entry objectForKey:@"list"] andTitle:[entry objectForKey:@"title"]];
            [array addObject:list];
        }
    }
    collection.items = [NSArray arrayWithArray:array];
    return array;
}

+(BCHContentCollection*) parseJson:(NSDictionary*) json {
    BCHContentCollection* result = [[BCHContentCollection alloc] init];
    NSArray* pages = [json objectForKey:@"content"];
    result.topLevel = [self processContentFrom:pages intoContentCollection:result];
    return result;
}

@end
