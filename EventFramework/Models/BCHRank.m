//
//  BCHRank.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-08.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHRank.h"

@implementation BCHRank

@dynamic eventId, conventionId, yayCount, score;

-(void) populateFromJson:(NSDictionary *)json {
    
    NSString* temp = [json objectForKey:@"yay"];
    if (temp != nil) {
        self.yayCount = [NSNumber numberWithInt:[temp intValue]];
    }
    temp = [json objectForKey:@"rank"];
    if (temp != nil) {
        self.score = [NSNumber numberWithInt:[temp intValue]];
    }
}

@end
