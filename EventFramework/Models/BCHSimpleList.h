//
//  BCHSimpleList.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import <Foundation/Foundation.h>
#import "ContentItem.h"

@interface BCHSimpleListItem : NSObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* subTitle;
@property (nonatomic, strong) NSString* href;

@end

@interface BCHSimpleList : NSObject<ContentItem>

@property (nonatomic, strong) NSString* contentFileName;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, readonly) NSArray* items;

-(id) initWithFileName:(NSString *) name andTitle:(NSString*) title;

@end
