//
//  Location.h
//  unslacked-app
//
//  Created by BC Holmes on 2012-10-20.
//
//

#import <Foundation/Foundation.h>

@interface BCHLocation : NSObject

@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* displayName;
@property (nonatomic, readonly) BOOL isGroup;
@property (nonatomic, readonly) NSInteger intKey;
@property (nonatomic, strong) NSArray* compositeLocations;
@property (weak, nonatomic, readonly) NSArray* allLocations;
@property (nonatomic, strong) NSString* thumbnail;
@property (nonatomic, strong) NSString* resourceKey;
@property (nonatomic, readonly) BOOL isRealLocation;
@property (nonatomic, readonly) BCHLocation* firstLocation;

-(id) initWithName:(NSString*) name andKey:(NSString*) key;
-(id) initWithName:(NSString*) name andLocations:(NSArray*) locations;
-(NSComparisonResult) compare:(BCHLocation*) otherObject;

+(NSDictionary*) parseLocations:(NSDictionary*) json;
+(BCHLocation*) locationByKey:(NSString*) key;
+(NSArray<NSString*>*) locationNames;

@end
