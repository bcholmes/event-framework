//
//  UnslackedEventTable.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-31.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCHScheduleEntry.h"

typedef enum {
    EventTableAccessTypeAll,
    EventTableAccessTypeSelected
} EventTableAccessType;

@interface BCHScheduleEntryTable : NSObject {
}

@property (nonatomic, strong) NSMutableDictionary *dictionary;
@property (readonly) NSInteger sectionCount;
@property (nonatomic, readonly) NSArray* allDays;
@property (nonatomic, readonly) BOOL isEmpty;

-(void) addEvent:(NSObject<BCHScheduleEntry>*) event;
-(NSArray*) sectionKeys;
-(NSInteger) sectionNumberFor:(NSObject<BCHScheduleEntry>*) event;
-(NSInteger) sectionNumberForRange:(UnslackedTimeRange*) timeRange;
-(NSArray*) sectionForIndex:(NSInteger) sectionNumber accessType:(EventTableAccessType) accessType;
-(NSObject<BCHScheduleEntry>*) entryAtIndexPath:(NSIndexPath *) indexPath accessType:(EventTableAccessType) accessType;
-(NSIndexPath*) indexPathFor:(NSObject<BCHScheduleEntry>*) event accessType:(EventTableAccessType) accessType;
-(BCHScheduleEntryTable*) eventsContainingSearchString:(NSString*) searchString;
-(UnslackedTimeRange*) closestTimeRangeToNow:(NSTimeZone*) timeZone;
-(BCHScheduleEntryTable*) filteredByEventType:(NSString*) eventType;

+(BCHScheduleEntryTable*) tableFromEvents:(NSArray*) events;
@end
