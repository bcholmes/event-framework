//
//  ContentItem.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-14.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ContentItem

@required

@property (nonatomic, strong) NSString* title;
@property (nonatomic, readonly) BOOL isMenu;
@property (nonatomic, readonly) NSString* key;

@end
