//
//  Location.m
//  unslacked-app
//
//  Created by BC Holmes on 2012-10-20.
//
//

#import "BCHLocation.h"

@implementation BCHLocation

static NSDictionary* _allLocations = nil;

+(NSArray<NSString*>*) locationNames {
    NSMutableArray* result = [NSMutableArray new];
    for (BCHLocation* location in _allLocations.allValues) {
        [result addObject:location.displayName];
    }
    return [result sortedArrayUsingSelector:@selector(compare:)];
}

-(id) initWithName:(NSString*) name andKey:(NSString*) key {
    if (self = [super init]) {
        self.name = name;
        self.displayName = name;
        self.key = key;
    }
    return self;
}

-(id) initWithName:(NSString*) name andLocations:(NSArray*) locations {
    if (self = [super init]) {
        self.name = name;
        self.displayName = name;
        self.compositeLocations = locations;
    }
    return self;
}

-(NSInteger) intKey {
    return self.key == nil ? 0 : [self.key intValue];
}

- (NSComparisonResult)compare:(BCHLocation *)otherObject {
    if (self.isGroup && otherObject.isGroup) {
        return [self.name compare:otherObject.name];
    } else if (self.isGroup) {
        return NSOrderedDescending;
    } else if (otherObject.isGroup) {
        return NSOrderedAscending;
    } else {
        NSInteger left = self.intKey;
        NSInteger right = otherObject.intKey;
        if (left == right) {
            return NSOrderedSame;
        } else if (left < right) {
            return NSOrderedAscending;
        } else {
            return NSOrderedDescending;
        }
    }
}

-(BOOL) isRealLocation {
    return self.displayName != nil && self.name != nil && ![self.name isEqualToString:@"*"];
}
-(BOOL) isGroup {
    return self.compositeLocations != nil && self.compositeLocations.count > 0;
}

-(NSArray*) allLocations {
    if (self.isGroup) {
        return self.compositeLocations;
    } else {
        return [NSArray arrayWithObject:self];
    }
}


-(BCHLocation*) firstLocation {
    if (self.isGroup) {
        return self.compositeLocations[0];
    } else {
        return self;
    }
}

+(NSDictionary*) addLocations:(NSArray*) locations toDictionary:(NSMutableDictionary*) dictionary {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    for (NSDictionary* entry in locations) {
        BCHLocation* location = [[BCHLocation alloc] initWithName:[entry objectForKey:@"name"] andKey:[entry objectForKey:@"id"]];
        if ([entry objectForKey:@"displayName"] != nil) {
            location.displayName = [entry objectForKey:@"displayName"];
        }
        if ([entry objectForKey:@"thumbnail"] != nil) {
            location.thumbnail = [entry objectForKey:@"thumbnail"];
        }
        if ([entry objectForKey:@"resource"] != nil) {
            location.resourceKey = [entry objectForKey:@"resource"];
        }
        [dictionary setObject:location forKey:location.name];
        [result setObject:location forKey:location.key];
    }
    return result;
}

+(void) addGroups:(NSArray*) locations toDictionary:(NSMutableDictionary*) dictionary usingLocations:(NSDictionary*) basicLocations {
    for (NSDictionary* entry in locations) {
        NSMutableArray* subLocations = [[NSMutableArray alloc] init];
        NSArray* keys = [entry objectForKey:@"ids"];
        for (NSString* key in keys) {
            [subLocations addObject:[basicLocations objectForKey:key]];
        }
        
        BCHLocation* location = [[BCHLocation alloc] initWithName:[entry objectForKey:@"name"] andLocations:subLocations];
        [dictionary setObject:location forKey:location.name];
        if ([entry objectForKey:@"thumbnail"] != nil) {
            location.thumbnail = [entry objectForKey:@"thumbnail"];
        }
        if ([entry objectForKey:@"resource"] != nil) {
            location.resourceKey = [entry objectForKey:@"resource"];
        }
    }
}

+(BCHLocation*) locationByKey:(NSString*) key {
    return [_allLocations objectForKey:key];
}
+(NSDictionary*) allLocations {
    return _allLocations;
}

+(NSDictionary*) parseLocations:(NSDictionary*) json {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    NSDictionary* basicLocations = [BCHLocation addLocations:[json objectForKey:@"locations"] toDictionary:result];
    [BCHLocation addGroups:[json objectForKey:@"groups"] toDictionary:result usingLocations:basicLocations];
    
    _allLocations = [NSDictionary dictionaryWithDictionary:result];
    
    return result;
}
@end
