//
//  Renderable.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-16.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BCHImageProvider

@optional

-(BOOL) isImageAvailable;
-(BOOL) isAddImageAllowed;
-(NSString*) imageSrc;

@end

@interface Renderable : NSObject

@property (weak, readonly) NSString *fullTitle;

- (NSString*) renderAsHtml:(id<BCHImageProvider>) imageProvider;

@end
