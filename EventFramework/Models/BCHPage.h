//
//  UnslackedPage.h
//  unslacked-app
//
//  Created by BC Holmes on 11-09-03.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentItem.h"
#import "Renderable.h"

@interface BCHPage : Renderable<ContentItem> {
}

@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* compareString;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* banner;

-(id) initWithFileName:(NSString *) name andPrefix:(NSString*) prefix;
-(id) initWithFileName:(NSString *) name andTitle:(NSString*) description;

@end
