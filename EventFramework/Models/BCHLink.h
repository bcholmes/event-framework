//
//  BCHLink.h
//  AFNetworking
//
//  Created by BC Holmes on 2018-03-21.
//

#import <Foundation/Foundation.h>

@interface BCHLink : NSObject

@property (nonatomic, nonnull, strong) NSString* name;
@property (nonatomic, nonnull, strong) NSString* type;
@property (nonatomic, nonnull, strong) NSString* href;
@property (nonatomic, nonnull, readonly) UIImage* icon;

+(BCHLink*) fromJson:(NSDictionary*) json;

@end
