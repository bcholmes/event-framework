//
//  UnslackedParticipation.h
//  unslacked-app
//
//  Created by BC Holmes on 11-09-07.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCHParticipant.h"

@interface BCHParticipation : NSObject {
}

@property (nonatomic, strong) BCHParticipant * participant;
@property (nonatomic, strong) NSString * type;
@property (nonatomic) BOOL cancelled;
@property (nonatomic) BOOL hidden;
@property (nonatomic, readonly) BOOL isModerator;
@end
