//
//  UnslackedParticipant.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BCHParticipant.h"
#import "UnslackedEvent.h"
#import "BCHLink.h"

@implementation BCHParticipant

-(id) init {
    if ((self = [super init])) {
        self.events = [[NSMutableArray alloc] init];
    }
    return self;
}

-(NSString*) fullNameAsHtml {
    if (self.firstName == nil) {
        return [NSString stringWithFormat:@"<b>%@</b>", self.name];
    } else {
        NSRange range = [self.name rangeOfString:self.lastName options:NSBackwardsSearch];
        
        if (range.location != NSNotFound) {
            return [self.name stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"<b>%@</b>", self.lastName]];
        } else {
            return [NSString stringWithFormat:@"<b>%@</b>", self.name];
        }
    }
}

-(UIImage*) defaultAvatar {
    if (self.isAnonymous) {
        return [UIImage imageNamed:@"Icon_user_anonymous"];
    } else {
        return [UIImage imageNamed:@"Icon_user"];
    }
}

-(NSString*) sortKey
{
    if (self.sortName.length > 0) {
        return [[self.sortName substringToIndex:1] uppercaseString];
    } else {
        return @" ";
    }
}
-(NSString *)fullTitle {
    return self.name;
}

-(NSString*) avatarId {
    if (self.isAnonymous) {
        return nil;
    } else {
        return _avatarId;
    }
}

-(NSString*) sortName {
    if (_sortName != nil && _sortName.length > 0 && [[NSCharacterSet letterCharacterSet] characterIsMember: [_sortName characterAtIndex:0]]) {
        return _sortName;
    } else if (_sortName != nil && _sortName.length > 0 && [_sortName characterAtIndex:0] == '@') {
        return [_sortName substringFromIndex:1];
    } else {
        return self.name;
    }
}

-(NSString*) name {
    if ([[_name lowercaseString] isEqualToString:@"anonymous"]) {
        return @"Anonymous";
    } else {
        return _name;
    }
}

-(BOOL) hasAvatar {
    return !self.isAnonymous && self.avatarId != nil;
}

-(NSString*) lastName {
    NSString* firstName = self.firstName;
    
    if (firstName == nil) {
        return self.name;
    } else {
        NSString* result = [self.name substringFromIndex:firstName.length];
        return [result stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
}

-(NSString*) firstName {
    NSArray* parts = [self.sortName componentsSeparatedByString:@" "];
    if (parts.count > 1) {
        NSString* lastNameFirstPart = parts[0];
        if ([[lastNameFirstPart substringFromIndex:lastNameFirstPart.length-1] isEqualToString:@","]) {
            lastNameFirstPart = [lastNameFirstPart substringToIndex:lastNameFirstPart.length-1];
        }
        NSRange range = [[self.name uppercaseString] rangeOfString:lastNameFirstPart];
        if ([[lastNameFirstPart substringToIndex:1] isEqualToString:@"("]) {
            return nil;
        } else if (range.location != NSNotFound && range.location > 0) {
            return [[self.name substringToIndex:range.location] stringByTrimmingCharactersInSet:
                    [NSCharacterSet whitespaceCharacterSet]];
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

-(void) addEvent:(UnslackedEvent*) newEvent {
    BOOL ok = YES;
    for (UnslackedEvent* event in self.events) {
        if ([event.eventId isEqualToString:newEvent.eventId]) {
            ok = NO;
            break;
        }
    }
    if (ok) {
        [self.events addObject:newEvent];
    }
}

// BCH: intention: "private" entries have no public events.
-(BOOL) isPrivate {
    BOOL result = YES;
    for (UnslackedEvent* event in self.events) {
        if (!event.isPrivate) {
            BCHParticipation* participation = [event participationWithKey:self.key];
            if (!participation.hidden) {
                result = NO;
                break;
            }
        }
    }
    return result;
}

-(BOOL) visible {
    return !self.isPrivate;
}

-(BOOL) isAnonymous {
    return [[self.name lowercaseString] isEqualToString:@"anonymous"];
}

-(NSComparisonResult) compareParticipant:(BCHParticipant*) participant{
    return [self.sortName compare:participant.sortName];
}

-(void) applyJson:(NSDictionary*) json {
    NSString* temp = [json objectForKey:@"bio"];
    if (temp != nil) {
        self.bio = temp;
    }
    
    temp = [json objectForKey:@"cancelled"];
    if (temp != nil) {
        self.cancelled = [temp boolValue];
    }
    
    temp = [json objectForKey:@"pronouns"];
    if (temp != nil) {
        self.pronouns = temp;
    }
    
    NSDictionary* names = [json objectForKey:@"name"];
    if (names != nil) {
        temp = [names objectForKey:@"display"];
        if (temp != nil) {
            self.name = temp;
        }
        temp = [names objectForKey:@"sort"];
        if (temp != nil) {
            self.sortName = temp;
        }
    }
    
    NSObject* extras = [json objectForKey:@"links"];
    if ([extras isKindOfClass:[NSDictionary class]]) {
        NSDictionary* links = (NSDictionary*) extras;
        NSMutableDictionary* tempDictionary = [[NSMutableDictionary alloc] init];
        if (links != nil) {
            for (NSString* key in [links allKeys]) {
                [tempDictionary setValue:[links objectForKey:key] forKey:key];
            }
        }
        self.oldLinks = tempDictionary;
    } else if ([extras isKindOfClass:[NSArray class]]) {
        NSArray* links = (NSArray*) extras;
        NSMutableArray* temp = [NSMutableArray new];
        for (NSDictionary* linkJson in links) {
            BCHLink* link = [BCHLink fromJson:linkJson];
            [temp addObject:link];
        }
        self.links = [NSArray arrayWithArray:temp];
    }
}

@end
