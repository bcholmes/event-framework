//
//  UnslackedParticipant.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Renderable.h"

@class UnslackedEvent;

@interface BCHParticipant : NSObject {
}
@property (nonatomic, strong) NSString* sortName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* bio;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSArray* links;
@property (nonatomic, strong) NSDictionary* oldLinks;
@property (nonatomic, strong) NSMutableArray* events;
@property (nonatomic) BOOL cancelled;
@property (nonatomic, strong) NSString* avatarId;
@property (nonatomic, strong) NSString* pronouns;

@property (nonatomic,readonly) BOOL visible;
@property (nonatomic,readonly) BOOL isAnonymous;
@property (nonatomic, readonly) NSString* sortKey;
@property (nonatomic, readonly) NSString* firstName;
@property (nonatomic, readonly) NSString* lastName;
@property (nonatomic, readonly) BOOL hasAvatar;
@property (nonatomic, readonly) UIImage* defaultAvatar;
@property (nonatomic, readonly) NSString* fullNameAsHtml;

-(void) applyJson:(NSDictionary*) json;
-(NSComparisonResult) compareParticipant:(BCHParticipant*) participant;
-(void) addEvent:(UnslackedEvent*) event;

@end
