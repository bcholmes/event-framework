//
//  UnslackedPage.m
//  unslacked-app
//
//  Created by BC Holmes on 11-09-03.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import "BCHPage.h"

@implementation BCHPage

- (NSComparisonResult)compare:(BCHPage *)otherObject {
    return [self.compareString compare:otherObject.compareString];
}
-(id) initWithFileName:(NSString *) name andTitle:(NSString*) description {
    if (self = [super init]) {
        self.content = name;
        self.title = description;
        self.compareString = self.title;
    }
    return self;
}

-(id) initWithFileName:(NSString *) name andPrefix:(NSString*) prefix {
    if (self = [super init]) {
        self.content = name;
        self.title = [[name stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
        if ([self.title hasPrefix:prefix]) {
            self.title = [self.title substringFromIndex:[prefix length] +1];
        }
        self.compareString = self.title;
        if ([self.title hasPrefix:@"-"]) {
            self.title = [self.title substringFromIndex:1];
        }
    }
    return self;
}

- (NSString*) fullTitle {
    return self.title;
}

- (NSString*) renderAsHtml:(id<BCHImageProvider>) imageProvider {
    return self.content;
}

-(BOOL) isMenu {
    return NO;
}

-(NSString*) key {
    return [self.content stringByDeletingPathExtension];
}
@end
