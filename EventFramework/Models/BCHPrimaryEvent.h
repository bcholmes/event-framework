//
//  UnslackedPrimaryEvent.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnslackedEvent.h"
#import "BCHParticipant.h"

@interface BCHPrimaryEvent : UnslackedEvent {
}

@property (weak, nonatomic, readonly) NSArray *participants;
-(void) addParticipant:(BCHParticipant *) participant;
-(BCHParticipant*) participantWithKey:(NSString *) key;
@end
