//
//  BCHParticipation.m
//  unslacked-app
//
//  Created by BC Holmes on 11-09-07.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import "BCHParticipation.h"

@implementation BCHParticipation

-(BOOL) isModerator {
    return [@"moderator" isEqualToString:self.type.lowercaseString];
}

@end
