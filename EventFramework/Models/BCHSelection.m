//
//  BCHSelection.m
//  EventsSchedule
//
//  Created by BC Holmes on 2019-05-04.
//  Copyright (c) 2019 Ayizan Studios. All rights reserved.
//

#import "BCHSelection.h"

@interface BCHSelection()

@property (nonatomic, strong) NSNumber* yayFlag;
@property (nonatomic, strong) NSNumber* highlightFlag;
@property (nonatomic, strong) NSNumber* scheduleFlag;

@end

@implementation BCHSelection

@dynamic eventId, conventionId, yayFlag, highlightFlag, scheduleFlag;

-(BOOL) highlight {
    return [self.highlightFlag boolValue];
}

-(void) setHighlight:(BOOL)highlight {
    self.highlightFlag = [NSNumber numberWithBool:highlight];
}

-(BOOL) yay {
    return [self.yayFlag boolValue];
}

-(void) setYay:(BOOL) yay {
    self.yayFlag = [NSNumber numberWithBool:yay];
}

-(BOOL) addedToSchedule {
    return [self.scheduleFlag boolValue];
}

-(void) setAddedToSchedule:(BOOL)addedToSchedule {
    self.scheduleFlag = [NSNumber numberWithBool:addedToSchedule];
}

@end
