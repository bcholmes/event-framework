//
//  SubMenu.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-14.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "SubMenu.h"

@implementation SubMenu

-(BOOL) isMenu {
    return YES;
}

-(NSString*) key {
    return self.title;
}

@end
