//
//  BCHSimpleScheduledItem.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-02-27.
//

#import "BCHSimpleScheduledItem.h"

@implementation BCHSimpleScheduledItem

-(NSString*) fullTitleAsHtml {
    return self.itemDescription;
}

-(NSString*) fullIdentifier {
    return [NSString stringWithFormat:@"Simple#%@", self.itemIdentifier];
}

+(BCHSimpleScheduledItem*) fromJson:(NSDictionary*) json {
    BCHSimpleScheduledItem* result = [BCHSimpleScheduledItem new];
    result.href = [json objectForKey:@"href"];
    result.time = [UnslackedTimeRange fromJson:json];
    result.itemDescription = [json objectForKey:@"description"];
    result.location = [BCHLocation locationByKey:[json objectForKey:@"location"]];
    result.subTitleLine = [json objectForKey:@"subtitle"];
    
    return result;
}

-(NSString*) subTitle {
    return self.subTitleLine;
}

-(NSString*) subTitleAsHtml {
    return self.subTitleLine;
}

+(NSArray*) fromJsonArray:(NSArray*) json {
    NSMutableArray* result = [NSMutableArray new];
    for (NSDictionary* record in json) {
        [result addObject:[self fromJson:record]];
    }
    return result;
}
@end
