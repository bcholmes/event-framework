//
//  BCHSelection.h
//  EventsSchedule
//
//  Created by BC Holmes on 2019-05-04.
//  Copyright (c) 2019 Ayizan Studios. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCHSelection : NSManagedObject

@property (nonatomic, strong) NSString* eventId;
@property (nonatomic, strong) NSString* conventionId;

@property (nonatomic, assign) BOOL highlight;
@property (nonatomic, assign) BOOL addedToSchedule;
@property (nonatomic, assign) BOOL yay;

@end

NS_ASSUME_NONNULL_END
