//
//  BCHScheduledItem.m
//  Event Framework
//
//  Created by BC Holmes on 2018-06-03.
//

#import "BCHScheduledItem.h"

#import "NSAttributedString+Html.h"


@implementation BCHScheduledItem

- (NSAttributedString*) attributedTitle:(CGFloat) fontSize {
    NSMutableString* html = [[NSMutableString alloc] init];
    if (self.fullTitleAsHtml.length > 0) {
        [html appendString:self.fullTitleAsHtml];
    }
    return [NSAttributedString fromHtml:html withFontSize:fontSize];
}

-(NSComparisonResult) compareItem:(BCHScheduledItem*)otherObject {
    if ([self.time compare:otherObject.time] != NSOrderedSame) {
        return [self.time compare:otherObject.time];
    } else if ([self.location compare:otherObject.location] != NSOrderedSame) {
        return [self.location compare:otherObject.location];
    } else {
        return [self.fullIdentifier compare:otherObject.fullIdentifier];
    }
}

-(BOOL) highlighted {
    return NO;
}
@end
