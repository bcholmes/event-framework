//
//  BCHLink.m
//  AFNetworking
//
//  Created by BC Holmes on 2018-03-21.
//

#import "BCHLink.h"

@implementation BCHLink

+(BCHLink*) fromJson:(NSDictionary*) json {
    BCHLink* result = [BCHLink new];
    result.name = [json objectForKey:@"name"];
    result.type = [json objectForKey:@"type"];
    result.href = [json objectForKey:@"href"];
    return result;
}

-(UIImage*) icon {
    if ([@"TWITTER" isEqualToString:self.type]) {
        return [UIImage imageNamed:@"Icon_twitter_user"];
    } else if ([@"DREAMWIDTH" isEqualToString:self.type]) {
        return [UIImage imageNamed:@"Icon_dreamwidth"];
    } else if ([@"BOOK" isEqualToString:self.type]) {
        return [UIImage imageNamed:@"Icon_book"];
    } else {
        return [UIImage imageNamed:@"Icon_link"];
    }
}

@end
