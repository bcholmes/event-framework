//
//  BCHReminder.h
//  EventFramework
//
//  Created by BC Holmes on 2019-04-13.
//

#import <CoreData/CoreData.h>

#import "BCHScheduleEntry.h"

#define BCHReminderChangedNotification @"BCHReminderChangedNotification"

NS_ASSUME_NONNULL_BEGIN

@interface BCHReminder : NSManagedObject<BCHScheduleEntry>

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* reminderId;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSDate* startTime;
@property (nonatomic, strong) NSDate* endTime;

@end

NS_ASSUME_NONNULL_END
