//
//  BCHScheduledItem.h
//  Event Framework
//
//  Created by BC Holmes on 2018-06-03.
//

#import <Foundation/Foundation.h>

#import "BCHLocation.h"
#import "UnslackedTimeRange.h"

@interface BCHScheduledItem : NSObject

@property (nonatomic, readonly) NSString* fullIdentifier;
@property (nonatomic, readonly) NSString* fullTitleAsHtml;
@property (nonatomic, readonly) NSString* subTitleAsHtml;
@property (nonatomic, strong) UnslackedTimeRange *time;
@property (nonatomic, readonly) NSString *subTitle;
@property (nonatomic, strong) BCHLocation* location;
@property (nonatomic, readonly) BOOL highlighted;

-(NSAttributedString*) attributedTitle:(CGFloat) fontSize;
-(NSComparisonResult) compareItem:(BCHScheduledItem*) otherObject;
@end
