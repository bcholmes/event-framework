//
//  BCHEventFilter.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-03.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentItem.h"

@interface BCHEventFilter : NSObject<ContentItem>

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* eventType;
@property (nonatomic, readonly) BOOL isMenu;

@end
