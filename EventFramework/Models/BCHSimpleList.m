//
//  BCHSimpleList.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import "BCHSimpleList.h"

@implementation BCHSimpleListItem

@end

@implementation BCHSimpleList

-(id) initWithFileName:(NSString *) name andTitle:(NSString*) description {
    if (self = [super init]) {
        self.contentFileName = name;
        self.title = description;
    }
    return self;
}

-(BOOL) isMenu {
    return NO;
}

-(NSString*) key {
    return [self.contentFileName stringByDeletingPathExtension];
}

-(NSArray*) items {
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:[self.contentFileName stringByDeletingPathExtension] ofType:[self.contentFileName pathExtension]];
    NSData *data = [NSData dataWithContentsOfFile:jsonPath];
    NSError *error = nil;
    NSArray* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSMutableArray* result = [NSMutableArray new];
    for (NSDictionary* record in json) {
        BCHSimpleListItem* item = [BCHSimpleListItem new];
        item.title = [record valueForKey:@"title"];
        item.subTitle = [record valueForKey:@"subTitle"];
        item.href = [record valueForKey:@"href"];
        [result addObject:item];
    }
    return [NSArray arrayWithArray:result];
}

@end
