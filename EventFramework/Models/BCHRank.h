//
//  BCHRank.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-08.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface BCHRank : NSManagedObject

@property (nonatomic, strong) NSString* eventId;
@property (nonatomic, strong) NSString* conventionId;
@property (nonatomic, strong) NSNumber* yayCount;
@property (nonatomic, strong) NSNumber* score;

-(void) populateFromJson:(NSDictionary*) json;
@end
