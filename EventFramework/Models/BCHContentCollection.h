//
//  UnslackedContentCollection.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-09.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCHPage.h"

@interface BCHContentCollection : NSObject

@property (nonatomic, strong) NSMutableDictionary* dictionary;
@property (nonatomic, strong) NSArray* topLevel;

-(void) addContent:(BCHPage*) page forKey:(NSString*) key;
-(BCHPage*) contentPageForKey:(NSString*) key;
-(BCHPage*) contentPageForLink:(NSString*) link;
-(id<ContentItem>) contentForKey:(NSString*) key;

+(BCHContentCollection*) parseJson:(NSDictionary*) json;
@end
