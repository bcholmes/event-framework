//
//  BCHReminder.m
//  EventFramework
//
//  Created by BC Holmes on 2019-04-13.
//

#import "BCHReminder.h"

#import "UnslackedEvent.h"

@implementation BCHReminder

@dynamic title, location, reminderId, startTime, endTime;

-(UIImage*) icon {
    return [UIImage imageNamed:@"Icon_reminder"];
}
-(NSString*) listSubtitle {
    return self.location;
}
-(NSString*) fullTitleAsHtml {
    return self.title;
}

-(BOOL) addedToSchedule {
    return YES;
}

-(BOOL) isMyEvent {
    return NO;
}

-(BOOL) visible {
    return YES;
}

-(BOOL) highlighted {
    return NO;
}

-(void) setHighlighted:(BOOL)highlighted {
}

-(UnslackedTimeRange*) time {
    UnslackedTimeRange* range = [UnslackedTimeRange new];
    range.startTime = self.startTime;
    range.endTime = self.endTime;
    return range;
}

-(void) setTime:(UnslackedTimeRange*) time {
    self.startTime = time.startTime;
    self.endTime = time.endTime;
}

-(BOOL) matchesString:(NSString* _Nonnull) searchString {
    return [self.title rangeOfString:searchString].location != NSNotFound;
}

-(NSComparisonResult) compare:(NSObject<BCHScheduleEntry>* _Nonnull) otherObject {
    if ([otherObject isKindOfClass:[UnslackedEvent class]]) {
        return NSOrderedDescending;
    } else if ([self.time compare:otherObject.time] != NSOrderedSame) {
        return [self.time compare:otherObject.time];
    } else {
        return [self.fullTitleAsHtml compare:otherObject.fullTitleAsHtml];
    }

}

@end
