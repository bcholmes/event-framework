//
//  BCHRestaurantList.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHRestaurantList : NSObject

@property (weak, readonly) NSArray* keys;
@property (weak, readonly) NSArray* allEntries;
@property (nonatomic,strong) NSString* type;

-(NSArray*) entriesForKey:(NSString*) key;
+(BCHRestaurantList*) nameBasedList:(NSArray*) restaurants;
+(BCHRestaurantList*) cuisineBasedList:(NSArray*) restaurants;
+(BCHRestaurantList*) createList:(NSArray*) restaurants ofType:(NSString*) type;
@end
