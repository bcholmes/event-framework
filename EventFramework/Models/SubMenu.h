//
//  SubMenu.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-14.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentItem.h"

@interface SubMenu : NSObject<ContentItem>

@property (nonatomic, strong) NSArray* items;
@property (nonatomic, strong) NSString* title;

@end
