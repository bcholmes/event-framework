//
//  BCHSimpleScheduledItem.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-02-27.
//

#import "BCHScheduledItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface BCHSimpleScheduledItem : BCHScheduledItem

@property (nonatomic, nullable, strong) NSString* itemIdentifier;
@property (nonatomic, nullable, strong) NSString* itemDescription;
@property (nonatomic, nullable, strong) NSString* href;
@property (nonatomic, nullable, strong) NSString* subTitleLine;

+(NSArray*) fromJsonArray:(NSArray*) json;

@end

NS_ASSUME_NONNULL_END
