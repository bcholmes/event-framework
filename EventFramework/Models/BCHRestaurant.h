//
//  BCHRestaurant.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-16.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Renderable.h"

#define BCHRestaurantListUpdated @"BCHRestaurantListUpdated"

typedef enum {
    BCHAvailabilityYes,
    BCHAvailabilityNo,
    BCHAvailabilityUnknown
} BCHAvailability;

@interface BCHRestaurant : NSObject

@property (nonatomic, strong) CLLocation* location;
@property (nonatomic, strong) NSString* key;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* hours;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* streetAddress;
@property (nonatomic, strong) NSString* phoneNumber;
@property (nonatomic, strong) NSString* cuisine;
@property (nonatomic, strong) NSString* notes;
@property (nonatomic, strong) NSString* memorialDayStatus;
@property (nonatomic, strong) NSString* veggie;
@property (nonatomic, strong) NSString* alcohol;
@property (nonatomic, strong) NSString* takeout;
@property (nonatomic, strong) NSString* allergen;
@property (nonatomic, strong) NSString* snapNotes;

@property (nonatomic) BCHAvailability reservationRecommended;
@property (nonatomic) BCHAvailability delivery;
@property (nonatomic) BCHAvailability wifi;
@property (nonatomic) NSUInteger priceRange;
@property (weak, nonatomic, readonly) NSString* annotations;
@property (weak, nonatomic, readonly) NSString* subTitle;
@property (weak, nonatomic, readonly) NSString* capitalizedCuisine;
@property (weak, nonatomic, readonly) NSString* priceRangeAsString;
@property (nonatomic, readonly) NSString* fullyQualifiedUrl;
@property (nonatomic, readonly) NSString* fullyQualifiedPhoneNumber;

-(BOOL) matchesString:(NSString*) searchString;

+(BCHRestaurant*) fromJsonObject:(NSDictionary*) json;
+(NSArray*) fromJsonCollection:(NSDictionary*) json;

@end
