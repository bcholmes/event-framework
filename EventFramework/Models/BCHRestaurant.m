//
//  Restaurant.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-16.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "BCHRestaurant.h"
#import "UIDevice+Utils.h"

@implementation BCHRestaurant

+(BCHRestaurant*) fromJsonObject:(NSDictionary*) json {
    BCHRestaurant* restaurant = [[BCHRestaurant alloc] init];
    restaurant.key = [json objectForKey:@"id"];
    restaurant.name = [json objectForKey:@"name"];
    restaurant.cuisine = [json objectForKey:@"cuisine"];
    restaurant.phoneNumber = [json objectForKey:@"phoneNumber"];
    restaurant.streetAddress = [json objectForKey:@"streetAddress"];
    restaurant.hours = [json objectForKey:@"hours"];
    restaurant.url = [json objectForKey:@"url"];
    restaurant.memorialDayStatus = [json objectForKey:@"memorialDayStatus"];
    restaurant.veggie = [json objectForKey:@"veggie"];
    restaurant.allergen = [json objectForKey:@"allergen"];
    restaurant.alcohol = [json objectForKey:@"alcohol"];
    restaurant.takeout = [json objectForKey:@"takeout"];
    NSObject* wifi = [json objectForKey:@"wifi"];
    if (wifi == nil || wifi == [NSNull null]) {
        restaurant.wifi = BCHAvailabilityUnknown;
    } else {
        restaurant.wifi = [[json objectForKey:@"wifi"] boolValue] ? BCHAvailabilityYes : BCHAvailabilityNo;
    }
    NSObject* reservationRecommended = [json objectForKey:@"reservationRecommended"];
    if (reservationRecommended == nil || reservationRecommended == [NSNull null]) {
        restaurant.reservationRecommended = BCHAvailabilityUnknown;
    } else {
        restaurant.reservationRecommended = [[json objectForKey:@"reservationRecommended"] boolValue] ? BCHAvailabilityYes : BCHAvailabilityNo;
    }
    NSObject* delivery = [json objectForKey:@"delivery"];
    if (delivery == nil || delivery == [NSNull null]) {
        restaurant.delivery = BCHAvailabilityUnknown;
    } else {
        restaurant.delivery = [[json objectForKey:@"delivery"] boolValue] ? BCHAvailabilityYes : BCHAvailabilityNo;
    }
    restaurant.priceRange = [[json objectForKey:@"priceRange"] intValue];
    
    NSDictionary* coordinates = [json objectForKey:@"coordinates"];

    if (coordinates != nil) {
        float latitude = [[coordinates objectForKey:@"latitude"] floatValue];
        float longitude = [[coordinates objectForKey:@"longitude"] floatValue];
        restaurant.location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    }
    return restaurant;
}

+(NSArray*) fromJsonCollection:(NSDictionary*) json {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    NSArray* array = [json objectForKey:@"restaurants"];
    
    for (NSDictionary* entry in array) {
        BCHRestaurant* restaurant = [BCHRestaurant fromJsonObject:entry];
        if (restaurant.streetAddress.length > 0) {
            [result addObject:restaurant];
        }
    }
    
    return result;
}

-(NSString*) priceRangeAsString {
    if (self.priceRange == 4) {
        return @"N/A";
    } else {
        NSMutableString *result = [NSMutableString string];
        for (NSUInteger i = 0; i < self.priceRange; i++) {
            [result appendString:@"$"];
        }
        return result;
    }
}

-(NSString *)fullTitle {
    return self.name;
}
-(NSString *) capitalizedCuisine {
    NSString* firstLetter = [[self.cuisine substringToIndex:1] uppercaseString];
    return [NSString stringWithFormat:@"%@%@", firstLetter, [self.cuisine substringFromIndex:1]];
}
-(NSString *)subTitle {
    NSMutableString *result = [NSMutableString stringWithString:self.cuisine];
    if (self.priceRange > 0) {
        [result appendString:@" \u2022 "];
        for (NSUInteger i = 0; i < self.priceRange; i++) {
            [result appendString:@"$"];
        }
    }
    if (self.streetAddress.length > 0) {
        [result appendString:@" \u2022 "];
        [result appendString:self.streetAddress];
    }
    return result;
}

-(NSString*) annotations {
    NSMutableString* string = [[NSMutableString alloc] init];
    
    if ([[self.takeout uppercaseString] isEqualToString:@"YES"] || [[self.takeout uppercaseString] isEqualToString:@"Y"]) {
        [string appendString:@"Take-out"];
    }
    if (self.delivery) {
        if (string.length > 0) {
            [string appendString:@", "];
        }
        [string appendString:@"Delivery"];
    }
    if (self.veggie.length > 0 && ![[self.veggie uppercaseString] isEqualToString:@"NO"] && ![[self.veggie uppercaseString] isEqualToString:@"N"]) {
        if (string.length > 0) {
            [string appendString:@", "];
        }
        [string appendFormat:@"Veggie (%@)", self.veggie];
    }
    if (self.alcohol.length > 0  && ![[self.alcohol uppercaseString] isEqualToString:@"NO"] && ![[self.alcohol uppercaseString] isEqualToString:@"N"]) {
        if (string.length > 0) {
            [string appendString:@", "];
        }
        [string appendString:self.alcohol];
    }
    if (self.wifi) {
        if (string.length > 0) {
            [string appendString:@", "];
        }
        [string appendString:@"Wifi"];
    }
    if (self.allergen.length > 0) {
        if (string.length > 0) {
            [string appendString:@", "];
        }
        [string appendString:self.allergen];
    }
    
    return string;
}


-(BOOL) matchesString:(NSString*) searchString {
    if (self.name != nil && [self.name rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else if (self.cuisine != nil && [self.cuisine rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else if (self.notes != nil && [self.notes rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else if (self.annotations != nil && [self.annotations rangeOfString:searchString options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch].location != NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}

-(NSString*) fullyQualifiedUrl {
    if ([self.url rangeOfString:@"http"].location == 0) {
        return self.url;
    } else {
        return [NSString stringWithFormat:@"http://%@", self.url];
    }
}

-(NSString*) fullyQualifiedPhoneNumber {
    if (self.phoneNumber.length == 0 || self.phoneNumber.length >= 10) {
        return self.phoneNumber;
    } else {
        return [NSString stringWithFormat:@"608-%@", self.phoneNumber];
    }
}

@end
