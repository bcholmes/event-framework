//
//  BCHRestaurantList.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "BCHRestaurantList.h"
#import "BCHRestaurant.h"

@interface BCHRestaurantList()

@property (nonatomic,strong) NSDictionary* entries;

@end

@implementation BCHRestaurantList

+(BCHRestaurantList*) createList:(NSArray*) restaurants ofType:(NSString *)type {
    if ([type isEqualToString:@"NAME"]) {
        return [BCHRestaurantList nameBasedList:restaurants];
    } else {
        return [BCHRestaurantList cuisineBasedList:restaurants];
    }
}

+(BCHRestaurantList*) nameBasedList:(NSArray*) restaurants {
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];

    for (BCHRestaurant* restaurant in restaurants) {
        NSString* key = [[restaurant.name substringToIndex:1] uppercaseString];
        if ([@"ABCDEFGHIJKLMNOPQRSTUVWXYZ" rangeOfString:key].location == NSNotFound) {
            key = @"#";
        }
        if ([dictionary objectForKey:key] == nil) {
            [dictionary setObject:[[NSMutableArray alloc] init] forKey:key];
        }
        NSMutableArray* array = [dictionary objectForKey:key];
        [array addObject:restaurant];
    }
    
    BCHRestaurantList* result = [[BCHRestaurantList alloc] init];
    result.entries = dictionary;
    result.type = @"NAME";
    return result;
}

+(BCHRestaurantList*) cuisineBasedList:(NSArray*) restaurants {
    NSMutableDictionary* dictionary = [[NSMutableDictionary alloc] init];
    
    for (BCHRestaurant* restaurant in restaurants) {
        NSString* key = restaurant.capitalizedCuisine;
        if ([dictionary objectForKey:key] == nil) {
            [dictionary setObject:[[NSMutableArray alloc] init] forKey:key];
        }
        NSMutableArray* array = [dictionary objectForKey:key];
        [array addObject:restaurant];
    }
    
    BCHRestaurantList* result = [[BCHRestaurantList alloc] init];
    result.entries = dictionary;
    result.type = @"CUISINE";
    return result;
}



-(NSArray*) keys {
    NSArray* result = [NSArray arrayWithArray:[self.entries allKeys]];
    return [result sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2];
    }];
}

-(NSArray*) allEntries {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    for (NSString* key in self.entries.keyEnumerator) {
        for (BCHRestaurant* restaurant in [self.entries objectForKey:key]) {
            [result addObject:restaurant];
        }
    }
    return result;
}


-(NSArray*) entriesForKey:(NSString*) key {
    return [self.entries objectForKey:key];
}

@end
