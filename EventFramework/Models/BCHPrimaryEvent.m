//
//  UnslackedPrimaryEvent.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-08.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BCHPrimaryEvent.h"

@interface BCHPrimaryEvent()

@property (nonatomic, strong) NSMutableDictionary *participantMap;

@end

@implementation BCHPrimaryEvent

-(id) init
{
    if (self =[super init]) {
        self.participantMap = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void) dealloc
{
    for (UnslackedEvent* e in self.subEvents) {
        [e.participation removeAllObjects];
    }
}

-(void) addParticipant:(BCHParticipant *)participant
{
    [self.participantMap setObject:participant forKey:participant.key];
}

-(NSArray*) participants {
    return [self.participantMap allValues];
}

-(BCHParticipant*) participantWithKey:(NSString *) key {
    return [self.participantMap objectForKey:key];
}
@end
