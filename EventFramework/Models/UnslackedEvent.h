//
//  UnslackedEvent.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-01.
//  Copyright 2011-2014 Ayizan Studios. All rights reserved.
//

#import "UnslackedTimeRange.h"

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>
#import <UIKit/UIKit.h>

#import "BCHLocation.h"
#import "BCHParticipation.h"
#import "BCHScheduleEntry.h"
#import "BCHScheduledItem.h"
#import "BCHSelection.h"

#define EventChangedNotification @"EventChangedNotification"
#define MyScheduleChangedNotification @"MyScheduleChangedNotification"
#define EventUpdates @"EventUpdates"

@class BCHRank;

@interface UnslackedEvent : BCHScheduledItem<BCHScheduleEntry>

@property (nonatomic, strong) NSString *externalId;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *hashTag;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString* eventDescription;
@property (weak, nonatomic, readonly) NSArray *subEvents;
@property (nonatomic, strong) NSMutableDictionary *subEventMap;
@property (nonatomic, strong) NSMutableArray *participation;
@property (nonatomic, strong) NSString* track;
@property (nonatomic) NSInteger sortKey;
@property (readonly) BOOL isMyEvent;
@property (nonatomic) BOOL touched;
@property (weak, readonly) NSString *fullTitle;
@property (weak, readonly) NSString *fullTitleNoHtml;
@property (weak, readonly) NSString *trackCategory;
@property (weak, readonly) NSString *timeAsString;
@property (weak, readonly) NSString *locationAndTrack;
@property (weak, readonly) UIImage* icon;
@property (nonatomic, strong) NSDate* lastModifiedDate;
@property (nonatomic, strong) NSString* notes;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic, readonly) BOOL visible;
@property (nonatomic, strong) NSArray* links;
@property (nonatomic) BOOL cancelled;
@property (nonatomic, strong) BCHSelection* selection;

-(void) addSubEvent:(UnslackedEvent*) event;
-(UnslackedEvent*) subEventForEventId:(NSString*) eventId;
-(UnslackedEvent*) findEventForEventId:(NSString*) eventId;
-(BOOL) matchesString:(NSString*) searchString;
- (NSArray*)allLeafEvents;
- (BCHParticipation*) participationWithKey:(NSString*) participantId;

- (NSComparisonResult)compareEvent:(UnslackedEvent*)otherObject;
@end
