//
//  BaseAppDelegate.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-01-19.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "BaseAppDelegate.h"

#import <AFNetworking/AFNetworking.h>
#import <UserNotifications/UserNotifications.h>

#import "BCHAnnouncement.h"
#import "BCHApiClient.h"
#import "Reachability.h"
#import "BCHRestaurant.h"
#import "BCHHttpHelper.h"
#import "UIDevice+Utils.h"
#import "BCHLoginHelper.h"
#import "RootViewController.h"
#import "BCHMenuViewController.h"
#import "SWRevealViewController.h"
#import "BCHContentCollection.h"
#import "BCHImageService.h"
#import "BCHCacheService.h"
#import "BCHTokenGenerator.h"
#import "NSData+Conversion.h"
#import "BCHDeviceTokenService.h"
#import "BCHEventService.h"
#import "BCHRestaurantService.h"
#import "BCHDateFormattingHelper.h"

@interface BaseAppDelegate()<SWRevealViewControllerDelegate,UNUserNotificationCenterDelegate>

@property (nonatomic, strong) BCHLoginHelper* loginHelper;
@property (nonatomic, strong) BCHCacheService* cacheService;
@property (nonatomic, strong) BCHDeviceTokenService* deviceTokenService;

@property (nonatomic, strong) NSManagedObjectModel* managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator* persistentStoreCoordinator;

@end

@implementation BaseAppDelegate

# define STATUS_STYLE UIStatusBarStyleLightContent

-(void)eventDidChange:(NSNotification*) notification {
    UnslackedEvent* event = notification.object;
    event.touched = YES;
    if (event.selection) {
        [event.selection.managedObjectContext save:nil];
    }
    [self markForUpload];
}

-(void) setTimeZone:(NSTimeZone*) timeZone {
    _timeZone = timeZone;
    [UnslackedTimeRange setConferenceTimeZone:timeZone];
    [BCHAnnouncement setConferenceTimeZone:timeZone];
}

-(BOOL) isLoggedIn {
    return self.loginHelper.isLoggedIn;
}

-(NSString*) personId {
    return self.loginHelper.personId;
}

- (BOOL) isAlreadyPromptedForLogin {
    if (self.isLoggedIn) {
        return YES;
    } else {
        return self.loginHelper.isAlreadyPrompted;
    }
}

- (void) markAsPromptedForLogin {
    [self.loginHelper markAsPrompted];
}

- (void) application:(UIApplication*) application handleEventsForBackgroundURLSession:(NSString*) identifier completionHandler:(void (^)(void)) completionHandler {
    NSLog(@"handleEventsForBackgroundURLSession");
    if ([identifier isEqualToString:ANNOUNCEMENTS_TASK]) {
        self.announcementCompletionHandler = completionHandler;
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [[UIApplication sharedApplication] setStatusBarStyle:STATUS_STYLE];

    [self.theme configure];
    
    self.loginHelper = [[BCHLoginHelper alloc] init];
    self.loginService = [[BCHLoginService alloc] initWithHelper:self.loginHelper];

    self.cacheService = [[BCHCacheService alloc] init];
    self.restaurantService = [[BCHRestaurantService alloc] init];
    [BCHDateFormattingHelper instance].timeZone = self.timeZone;
    self.apiClient = [[BCHApiClient alloc] initWithCache:(BCHCacheService*) self.cacheService];
    self.deviceTokenService = [BCHDeviceTokenService new];
    
    [self loadEventsList];
    self.eventService = [[BCHEventService alloc] initWithConventionId:self.currentCon primaryEvent:self.event managedObjectContext:self.managedObjectContext];
    
    if (self.supportsAnnouncements) {
        self.announcementService = [[BCHAnnouncementService alloc] initWithUrl:self.blogUrl managedObjectContext:self.managedObjectContext];
        [self.announcementService scheduleBackgroundDownload:nil];
    }
    
    [self.eventService readSelectionsFromDatabase];
    
    UIWindow *window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window = window;
    
    RootViewController* rootViewController = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BCHMenuViewController* menuViewController = [storyboard instantiateViewControllerWithIdentifier:@"menu"];
    
    UINavigationController* rootNavigationController = [[UINavigationController alloc] initWithRootViewController:rootViewController];
    
    SWRevealViewController* revealController = [[SWRevealViewController alloc]
                                                    initWithRearViewController:menuViewController frontViewController:rootNavigationController];
    revealController.delegate = self;
    
    self.viewController = revealController;
    
    [self.window setRootViewController:self.viewController];
    [self.window makeKeyAndVisible];
    
    
    // Override point for customization after application launch.
    // Add the navigation controller's view to the window and display.
    /*
    if ([UIDevice isIPad]) {
        UINavigationController* navigationController = [self.tabBarController.viewControllers objectAtIndex:1];
        navigationController.viewControllers = @[ [[BCHGridViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil ] ];
    }
    self.window.rootViewController = self.tabBarController;
    [self.window makeKeyAndVisible];
    */
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:EventChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:MyScheduleChangedNotification object:nil];
    
    [UIApplication.sharedApplication setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    return YES;
}

- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    [self.announcementService scheduleBackgroundDownload:completionHandler];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken %@", [deviceToken hexadecimalString]);
    
    [self.deviceTokenService sendDeviceTokenToServer:[deviceToken hexadecimalString] clientId:self.clientId];
}

-(void) registerForNotifications {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (!error) {
            center.delegate = self;
            NSLog(@"request succeeded!");
            /*
            dispatch_async(dispatch_get_main_queue(), ^{
                [self addWelcomeNotifications];
            });
            */
        } else {
            NSLog(@"No user notifications for you!");
        }
    }];
}

-(BOOL) isWelcomeMessageAlreadyHandled {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    return [userDefaults boolForKey:[NSString stringWithFormat:@"%@-welcome-notification", self.currentCon]];
}

-(void) addWelcomeNotifications {
    if (![self isWelcomeMessageAlreadyHandled] && self.conventionLocation != nil) {
        NSArray* events = [self.eventService allLeafEvents];
        events = [events sortedArrayUsingSelector:@selector(compareEvent:)];
        
        NSDate* startTime = ((UnslackedEvent*)[events firstObject]).time.startTime;
//        NSDate* endTime = ((UnslackedEvent*)[events lastObject]).time.endTime;

        if ([[NSDate new] compare:startTime] == NSOrderedDescending) {
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

            [calendar setTimeZone:[NSTimeZone localTimeZone]];

            UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
            content.title = [NSString localizedUserNotificationStringForKey:@"Welcome!" arguments:nil];
            content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"You've arrived at %@", self.currentConName != nil ? self.currentConName : self.currentCon] arguments:nil];
            content.sound = [UNNotificationSound defaultSound];

            CLLocationCoordinate2D hotel = CLLocationCoordinate2DMake(self.conventionLocation.coordinate.latitude, self.conventionLocation.coordinate.longitude);
            CLCircularRegion* region = [[CLCircularRegion alloc] initWithCenter:hotel radius:100.0 identifier:@"Convention Hotel"];
            region.notifyOnEntry = YES;
            region.notifyOnExit = NO;

            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:[NSString stringWithFormat:@"%@-welcome-msg", self.currentCon] content:content trigger:[UNLocationNotificationTrigger triggerWithRegion:region repeats:NO]];

            UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
            [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                if (!error) {
                    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults setBool:YES forKey:[NSString stringWithFormat:@"%@-welcome-notification", self.currentCon]];
                    [userDefaults synchronize];

                    NSLog(@"Welcome Notification registered");
                } else {
                    NSLog(@"Welcome Notification failed");
                }
            }];
        }
    }
}

- (void)application:(UIApplication*) app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if ([error.domain isEqualToString:NSCocoaErrorDomain] && error.code == 3010) {
        NSLog(@"%@", error.localizedDescription);
    } else {
        NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

-(void)loadEventsList {
    NSLog(@"Loading events...");
    
    self.locations = [self.apiClient getLocations];
    self.event = [self.apiClient getTopLevelEventById:self.currentCon usingLocations:self.locations];
    NSLog(@"Events loaded");
}

- (NSArray*) contentTopLevel {
    return self.contentCollection.topLevel;
}

- (BCHPage*) contentByKey:(NSString*) key {
    return [self.contentCollection contentPageForKey:key];
}

-(BCHContentCollection*) contentCollection {
    if (_contentCollection == nil && self.contentJsonName != nil) {
        _contentCollection = [self.apiClient getContent:self.contentJsonName];
    }
    return _contentCollection;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (BOOL)isInternetConnected {
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    return reachability.currentReachabilityStatus != NotReachable;
}

/*
 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
 */
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"applicationDidBecomeActive");
    
    [self registerForNotifications];
    
    self.shouldScrollToCurrentTimePeriod = YES;
    [self.apiClient processUpdates:self.event usingLocations:self.locations];
    if ([self isInternetConnected]) {
        if (self.isLoggedIn) {
            [self fetchAssignments];
        }
        [self performSelectorInBackground:@selector(dataSynch) withObject:nil];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
}


-(void)dataSynch {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"needsUpload"]) {
        NSString* json = [self createSelectionsJson];
        NSLog(@"Uploading data to server: %@", json);
        BCHTokenGenerator* generator = [[BCHTokenGenerator alloc] initWithClientId:self.clientId andPersonId:self.personId];
        [BCHHttpHelper sendJson:json toUrl:[NSURL URLWithString:@"https://ayizan.org/api/selections"] headers:@{ @"Authorization" : [NSString stringWithFormat:@"Basic %@", generator.token]} onSuccess:^(NSData* data, NSString* string) {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"needsUpload"];
        } onError:nil];
    }
    if ([self.apiClient fetchLatestUpdates:self.currentCon]) {
         [self.apiClient processUpdates:self.event usingLocations:self.locations];
    };
    if (self.supportsRestaurants) {
        [self.restaurantService fetchAndCacheRestaurantUpdates];
    };
    
    [self.eventService fetchRankings:self.event];
    [self.imageService uploadQueuedAvatar];
    [self.imageService fetchLatestAvatars];
}

-(NSString*) clientId {
    NSString* clientId = [[NSUserDefaults standardUserDefaults] stringForKey:@"clientId"];
    if (clientId == nil) {
        clientId = [BaseAppDelegate generateGuid];
        [[NSUserDefaults standardUserDefaults] setObject:clientId forKey:@"clientId"];
    }
    return clientId;
}

-(NSString*) createSelectionsJson {
    NSMutableString* json = [[NSMutableString alloc] init];
    
    NSArray* events = [self.event.subEvents copy];
    for (UnslackedEvent* e in events) {
        if (e.selection) {
            if ([json length] > 0) {
                [json appendString:@","];
            }
            
            [json appendFormat:@"{ \"eventId\": %@, \"scheduled\": %@, \"highlighted\": %@, \"yay\": %@, \"omg\": %@ }", e.eventId, e.selection.addedToSchedule ? @"true" : @"false", e.selection.highlight ? @"true" : @"false", e.selection.yay ? @"true" : @"false", @"false"];
        }
    }
    NSString* personId = self.personId == nil ? @"" : [NSString stringWithFormat:@"\"personId\": %@, ", self.personId];
    return [NSString stringWithFormat:@"{ \"clientId\": \"%@\", \"convention\": \"%@\", %@ \"selections\": [ %@ ] }", self.clientId, self.currentCon, personId, json];
}

-(void)markForUpload {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"needsUpload"];
}

- (UnslackedEvent*)eventById:(NSString*)eventId {
    return [self.event findEventForEventId:eventId];
}
- (NSArray*)allLeafEvents {
    return [self.event allLeafEvents];
}

+ (NSString *) generateGuid {
    return [[NSUUID UUID] UUIDString];
}

- (NSArray*) topLevelEventsForDay:(NSString*) day {
    return [self.eventService topLevelEventsForDay:day];
}

-(void) fetchAssignments {

    AFHTTPSessionManager* sessionManager = [[AFHTTPSessionManager alloc] init];
    sessionManager.completionQueue = dispatch_queue_create("org.ayizan.wissched.queue", NULL);
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [sessionManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", self.loginHelper.jwtToken] forHTTPHeaderField:@"Authorization"];

    [sessionManager POST:@"https://ayizan.org/api/assignments" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {

        NSLog(@"assignments fetched...");
        NSDictionary* json = (NSDictionary*) responseObject;
        NSArray* assignments = [json objectForKey:@"assignments"];
        
        for (NSString* eventId in assignments) {
            UnslackedEvent* event = [self eventById:eventId];
            if (event != nil && !event.addedToSchedule) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.eventService changeScheduledStatusOfEvent:event added:YES];
                });
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSUInteger statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
        NSLog(@"Get assignments failure: %@", error);
        if (error != nil && statusCode == 401) {
            [self.loginHelper clearLogin];
        }
    }];
}

-(NSArray*) menuItems {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    [result addObject:@"Program"];
    [result addObject:@"Grid"];
    [result addObject:@"My Schedule"];
    [result addObject:@"Bios"];
    if (self.supportsConInfo) {
        [result addObject:@"Con Info"];        
    }
    if (self.supportsAnnouncements) {
        [result addObject:@"Announcements"];
    }
    if (self.supportsRestaurants) {
        [result addObject:@"Restaurants"];
    }
    [result addObject:@"Leaderboard"];
    if (self.supportsLogin && !self.loginHelper.isLoggedIn) {
        [result addObject:@"Login"];
    }
    return result;
}

- (NSURL*) urlForAvatarId:(NSString*) avatarId {
    return [self.imageService urlForAvatar:avatarId];
}

+(BaseAppDelegate*) instance {
    return (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
}


-(BCHImageService*) imageService {
    return [[BCHImageService alloc] initWithConventionId:self.currentCon tokenGenerator:[[BCHTokenGenerator alloc] initWithClientId:self.clientId andPersonId:self.personId] andCache:self.cacheService primaryEvent:self.event];
}

- (NSManagedObjectModel*) managedObjectModel {
    
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}


- (NSManagedObjectContext*) managedObjectContext {
    
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _managedObjectContext.persistentStoreCoordinator = coordinator;
    }
    return _managedObjectContext;
}

/**
 Returns the URL to the application's documents directory.
 */
- (NSURL*) applicationDocumentsDirectory {
    return [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
}

- (NSPersistentStoreCoordinator*) persistentStoreCoordinator {
    
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    } else {
        NSString *documentsStorePath = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"EventFramework.sqlite"];
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        // Add the default store to our coordinator.
        NSError *error;
        NSURL *defaultStoreURL = [NSURL fileURLWithPath:documentsStorePath];
        NSDictionary* options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES};
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                       configuration:nil
                                                                 URL:defaultStoreURL
                                                             options:options
                                                               error:&error]) {
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
        
        return _persistentStoreCoordinator;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) showAnnouncementsPage {
    SWRevealViewController* controller = (SWRevealViewController*) self.viewController;
    [((BCHMenuViewController*) controller.rearViewController) showAnnouncementsPage];
}

-(void) processNotification:(NSString*) notificationIdentifier {
    if ([ANNOUNCEMENT_NOTIFICATION_ID isEqualToString:notificationIdentifier]) {
        [self showAnnouncementsPage];
    }
}

#pragma mark UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter*) center didReceiveNotificationResponse:(UNNotificationResponse*) response withCompletionHandler:(void (^)(void)) completionHandler {
    
    [self processNotification:response.notification.request.identifier];
    
    if (completionHandler != nil) {
        completionHandler();
    }
}

- (void) userNotificationCenter:(UNUserNotificationCenter*) center willPresentNotification:(UNNotification*) notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options)) completionHandler {
    if (completionHandler) {
        completionHandler(UNNotificationPresentationOptionAlert);
    }
}

@end
