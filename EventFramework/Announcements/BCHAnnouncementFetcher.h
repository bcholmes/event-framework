//
//  BCHAnnouncementFetcher.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-04-07.
//

#import <UIKit/UIKit.h>

#import <AFNetworking/AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCHAnnouncementFetcher : AFHTTPSessionManager

+ (instancetype)sharedManager;

@property (nonatomic, nullable, copy) void (^savedCompletionHandler)(void);

@end

NS_ASSUME_NONNULL_END
