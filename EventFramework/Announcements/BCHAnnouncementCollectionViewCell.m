//
//  BCHAnnouncementCollectionViewCell.m
//  EventFramework
//
//  Created by BC Holmes on 2019-04-08.
//

#import "BCHAnnouncementCollectionViewCell.h"

#import <MaterialComponents/MaterialInk.h>

@interface BCHAnnouncementCollectionViewCell()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint* widthContraint;

@end

@implementation BCHAnnouncementCollectionViewCell

-(void) setHighlighted:(BOOL)highlighted {
    self.cardView.highlighted = highlighted;
}

-(void) setWidth:(CGFloat) width {
    self.widthContraint.constant = width;
}

-(CGFloat) width {
    return self.widthContraint.constant;
}

@end
