//
//  BCHAnnouncement.m
//  EventFramework
//
//  Created by BC Holmes on 2019-04-07.
//

#import "BCHAnnouncement.h"

#import "NSAttributedString+Html.h"
#import "NSString+HTML.h"
#import "UIDevice+Utils.h"

@implementation BCHAnnouncement

static NSTimeZone* _conferenceTimeZone = nil;

@dynamic announcementId, title, postedByUser, date, content;

-(NSAttributedString*) subTitle {
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d yyyy 'at' h:mm a"];
    [dateFormatter setTimeZone:[BCHAnnouncement conferenceTimeZone]];
    
    dateFormatter.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    return [NSAttributedString fromHtml:[NSString stringWithFormat:@"Posted by <b>%@</b>, %@", self.postedByUser, [dateFormatter stringFromDate:self.date]] withFontSize:font.pointSize];
}

-(NSAttributedString*) attributedTitle {
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleTitle3];
    return [NSAttributedString fromHtml:self.title withFontSize:font.pointSize];
}

-(NSString*) contentNoHtml {
    return [self.content stringByRemovingHtmlMarkup];
}

- (NSString*) renderAsHtml {
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    NSMutableString *result = [NSMutableString stringWithFormat:@"<html><head><title>%@</title><link rel=\"stylesheet\" type=\"text/css\" href=\"./list.css\" />", self.title];

    [result appendString:@"<meta name=\"format-detection\" content=\"telephone=no\">"];
    [result appendString:@"<meta name=\"viewport\" content=\"initial-scale=1.0\" />"];
    [result appendFormat:@"<style>body { font-size: %ldpx }</style>", (NSUInteger) font.pointSize];
    [result appendString:@"</head><body><div class=\"content\"><div class=\"main\">"];
    [result appendFormat:@"<p><b>%@</b></p>", self.title];
    
    [result appendString:self.content];
    
    [result appendString:@"</div></body></html>"];
    
    return result;
}

+(NSDate*) parseDate:(NSDictionary*) json {
    NSString* date = [json objectForKey:@"date"];
    if (date != nil) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        dateFormatter.timeZone = [BCHAnnouncement conferenceTimeZone];
        dateFormatter.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        return [dateFormatter dateFromString:date];
    } else {
        return nil;
    }
}

-(void) updateFromJson:(NSDictionary*) json {
    self.title = [json objectForKey:@"title"];
    self.content = [json objectForKey:@"content"];
    self.date = [BCHAnnouncement parseDate:json];
    
    NSDictionary* authorBlock = [json objectForKey:@"author"];
    self.postedByUser = [authorBlock objectForKey:@"name"];
}

+(NSTimeZone*) conferenceTimeZone {
    return _conferenceTimeZone;
}

+(void) setConferenceTimeZone:(NSTimeZone*) conferenceTimeZone {
    _conferenceTimeZone = conferenceTimeZone;
}

@end
