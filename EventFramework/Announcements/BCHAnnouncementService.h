//
//  BCHAnnouncementService.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-14.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <Foundation/Foundation.h>

#define ANNOUNCEMENTS_TASK @"announcements-task"
#define ANNOUNCEMENT_NOTIFICATION_ID @"new-announcements"

typedef void (^handler)(UIBackgroundFetchResult result);

@interface BCHAnnouncementService : NSObject

-(instancetype) initWithUrl:(NSString*) url managedObjectContext:(NSManagedObjectContext*) managedObjectContext;
-(void) scheduleBackgroundDownload:(handler) completionHandler;

@end
