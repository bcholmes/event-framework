//
//  BCHAnnouncementCollectionViewCell.h
//  EventFramework
//
//  Created by BC Holmes on 2019-04-08.
//

#import <UIKit/UIKit.h>

#import "BCHCardView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BCHAnnouncementCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel* titleLabel;
@property (nonatomic, weak) IBOutlet UILabel* subTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel* previewLabel;
@property (nonatomic, weak) IBOutlet BCHCardView* cardView;

@property (nonatomic, assign) CGFloat width;

@end

NS_ASSUME_NONNULL_END
