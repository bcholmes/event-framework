//
//  BCHAnnouncement.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-04-07.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCHAnnouncement : NSManagedObject

@property (nonatomic, strong) NSString* announcementId;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSString* content;
@property (nonatomic, strong) NSString* postedByUser;

@property (nonatomic, readonly) NSAttributedString* subTitle;
@property (nonatomic, readonly) NSAttributedString* attributedTitle;
@property (nonatomic, readonly) NSString* contentNoHtml;

-(void) updateFromJson:(NSDictionary*) json;

+(void) setConferenceTimeZone:(NSTimeZone*) conferenceTimeZone;
+(NSTimeZone*) conferenceTimeZone;
+(NSDate*) parseDate:(NSDictionary*) json;
-(NSString*) renderAsHtml;

@end

NS_ASSUME_NONNULL_END
