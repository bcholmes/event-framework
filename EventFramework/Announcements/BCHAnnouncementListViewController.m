//
//  BCHAnnouncementListViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-03-15.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHAnnouncementListViewController.h"

#import <MaterialComponents/MaterialAppBar.h>

#import "UIViewController+Style.h"
#import "BaseAppDelegate.h"
#import "BCHAnnouncement.h"
#import "BCHAnnouncementCollectionViewCell.h"
#import "BCHTheme.h"
#import "BCHSimpleRenderableDetailsViewController.h"

static NSString *const kReusableIdentifierItem = @"announcementCell";

@interface BCHAnnouncementListViewController ()<NSFetchedResultsControllerDelegate, MDCFlexibleHeaderViewLayoutDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) MDCAppBar* appBar;
@property (nonatomic, strong) UIImageView* headerImage;
@property (nonatomic, assign) CGFloat maxHeaderHeight;

@end

@implementation BCHAnnouncementListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMenuButton];
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;

    [self configureAppBar];

    UICollectionViewFlowLayout* layout = (UICollectionViewFlowLayout*) self.collectionView.collectionViewLayout;
    CGFloat width = self.collectionView.bounds.size.width;
    layout.estimatedItemSize = CGSizeMake(width, 130);

    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    [self queryData:delegate.managedObjectContext];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

-(void) configureAppBar {
    self.appBar = [MDCAppBar new];
    
    self.appBar.navigationBar.backgroundColor = [UIColor clearColor];
    self.appBar.headerViewController.layoutDelegate = self;
    [self.appBar.navigationBar setTitle:nil];
    
    [self addChildViewController:self.appBar.headerViewController];
    self.headerImage = [UIImageView new];
    self.headerImage.image = [UIImage imageNamed:@"announcements_header"];
    self.headerImage.contentMode = UIViewContentModeScaleAspectFill;
    self.headerImage.clipsToBounds = YES;
    self.headerImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    CGFloat imageAspectRatio = 72.0f/120.0f;
    CGFloat screenWidth = self.view.bounds.size.width;
    NSLog(@"screen width %f", screenWidth);
    self.appBar.headerViewController.headerView.minimumHeight = 44 + [UIApplication sharedApplication].statusBarFrame.size.height;
    self.appBar.headerViewController.headerView.maximumHeight = self.maxHeaderHeight = MIN(240, screenWidth * imageAspectRatio);
    self.headerImage.frame = self.appBar.headerViewController.headerView.bounds;
    
    [self.appBar.headerViewController.headerView insertSubview:self.headerImage atIndex:0];
    
    self.appBar.headerViewController.headerView.trackingScrollView = self.collectionView;
    
    [self.appBar addSubviewsToParent];
}

- (void) queryData:(NSManagedObjectContext*) managedObjectContext {
    
    if (self.fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Announcement" inManagedObjectContext:managedObjectContext];
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        fetchRequest.sortDescriptors = @[ sortDescriptor1 ];
        
        NSFetchedResultsController* aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        aFetchedResultsController.delegate = self;
        self.fetchedResultsController = aFetchedResultsController;
        
        NSError* error;
        if (![self.fetchedResultsController performFetch:&error]) {
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        }
    }
}

#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView*) collectionView numberOfItemsInSection:(NSInteger) section {
    return self.fetchedResultsController.fetchedObjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BCHAnnouncementCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:kReusableIdentifierItem forIndexPath:indexPath];

    BCHAnnouncement* page = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.titleLabel.attributedText = page.attributedTitle;
    cell.subTitleLabel.attributedText = page.subTitle;
    cell.previewLabel.text = page.contentNoHtml;
    CGFloat width = self.collectionView.bounds.size.width;
    cell.width = width - 32;
    
    return cell;
}

- (void) collectionView:(UICollectionView*) collectionView didSelectItemAtIndexPath:(NSIndexPath*) indexPath {
    BCHAnnouncement* page = self.fetchedResultsController.fetchedObjects[indexPath.row];
    BCHSimpleRenderableDetailsViewController* controller = [[BCHSimpleRenderableDetailsViewController alloc] initWithNibName:@"BCHSimpleRenderableDetailsView" bundle:nil];
    controller.renderable = page;
    UIViewController* navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
//    navigationController
    [self.splitViewController showDetailViewController:navigationController sender:self];
}

-(void) scrollViewDidScroll:(UIScrollView*) scrollView {
    [self.appBar.headerViewController.headerView trackingScrollViewDidScroll];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView*) scrollView {
    [self.appBar.headerViewController.headerView trackingScrollViewDidEndDecelerating];
}

-(void) scrollViewDidEndDragging:(UIScrollView*) scrollView willDecelerate:(BOOL) decelerate {
    [self.appBar.headerViewController.headerView trackingScrollViewDidEndDraggingWillDecelerate:decelerate];
}

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self.appBar.headerViewController.headerView trackingScrollViewWillEndDraggingWithVelocity:velocity targetContentOffset:targetContentOffset];
}
     

#pragma mark UICollectionViewDelegate

/*
- (CGSize)collectionView:(UICollectionView*) collectionView layout:(UICollectionViewLayout*) collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath*) indexPath {
    CGFloat width = self.collectionView.bounds.size.width;
    return CGSizeMake(width, 130);
}
*/
#pragma mark NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.collectionView reloadData];
}

#pragma mark MDCFlexibleHeaderViewLayoutDelegate

- (void)flexibleHeaderViewController:(nonnull MDCFlexibleHeaderViewController *)flexibleHeaderViewController flexibleHeaderViewFrameDidChange:(nonnull MDCFlexibleHeaderView *)flexibleHeaderView {

}

@end
