//
//  BCHAnnouncementService.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-14.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHAnnouncementService.h"

#import <UserNotifications/UserNotifications.h>

#import "BaseAppDelegate.h"
#import "BCHAnnouncement.h"


@interface BCHAnnouncementService()<NSURLSessionDownloadDelegate>

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) handler completionHandler;
@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, strong) NSURLSession* session;

@end


@implementation BCHAnnouncementService

-(instancetype) initWithUrl:(NSString*) url managedObjectContext:(NSManagedObjectContext*) managedObjectContext {
    if (self = [super init]) {
        self.url = url;
        self.managedObjectContext = managedObjectContext;
    }
    return self;
}

-(NSURLSession*) session {
    if (_session == nil) {
        NSURLSessionConfiguration* configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:ANNOUNCEMENTS_TASK];
        configuration.discretionary = YES;
        configuration.sessionSendsLaunchEvents = YES;
        _session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    }
    return _session;
}

-(void) scheduleBackgroundDownload:(handler) completionHandler {
    NSLog(@"scheduling background download of announcements");
    self.completionHandler = completionHandler;
    NSURLSessionDownloadTask* task = [self.session downloadTaskWithURL:[NSURL URLWithString:self.url]];
    task.earliestBeginDate = [[NSDate new] dateByAddingTimeInterval:2];
    task.countOfBytesClientExpectsToSend = 200;
    task.countOfBytesClientExpectsToReceive = 12 * 2048;
    [task resume];
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask*) downloadTask didFinishDownloadingToURL:(NSURL*) location {
    NSLog(@"announcements downloaded to %@", location);
    NSData* data = [NSData dataWithContentsOfURL:location];
    [self performSelectorInBackground:@selector(processData:) withObject:data];
}

-(void) createNotification:(NSUInteger) newAnnouncements {
    NSString* applicationName = [[[NSBundle mainBundle] infoDictionary] objectForKey:(id)kCFBundleNameKey];
    UNMutableNotificationContent* content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"New Announcements!" arguments:nil];
    if (newAnnouncements == 1) {
        content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"There's a new %@ announcement.", applicationName] arguments:nil];
    } else {
        content.body = [NSString localizedUserNotificationStringForKey:[NSString stringWithFormat:@"There are %lu new %@ announcements.", newAnnouncements, applicationName] arguments:nil];
    }
    content.sound = [UNNotificationSound defaultSound];
    
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:ANNOUNCEMENT_NOTIFICATION_ID
                                                                          content:content trigger:nil];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Announcements Notification succeeded");
        } else {
            NSLog(@"Announcements Notification failed: %@", error);
        }
    }];
}

-(void) processData:(NSData*) data {
    if (data) {
        [self.managedObjectContext performBlock:^{
            NSUInteger newAnnouncements = 0;
            NSArray* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            NSDate* oldest = [NSDate new];
            for (NSDictionary* record in json) {
                NSDate* date = [BCHAnnouncement parseDate:record];
                if ([date compare:oldest] == NSOrderedAscending) {
                    oldest = date;
                }
            }
            NSMutableDictionary* dictionary = [self mostRecentAnnouncements:oldest];
            NSLog(@"number of existing announcements %lu", dictionary.count);
            for (NSDictionary* record in json) {
                NSString* announcementId = [[record objectForKey:@"ID"] stringValue];
                BCHAnnouncement* announcement = [dictionary objectForKey:announcementId];
                if (announcement == nil) {
                    announcement = [self findAnnouncementById:announcementId];
                }
                if (announcement == nil) {
                    NSLog(@"Announcement record id=%@ not found. Creating a new announcement.", announcementId);
                    announcement = [NSEntityDescription insertNewObjectForEntityForName:@"Announcement" inManagedObjectContext:self.managedObjectContext];
                    announcement.announcementId = announcementId;
                    newAnnouncements++;
                }
                [dictionary removeObjectForKey:announcementId];
                [announcement updateFromJson:record];
            }
            NSLog(@"number of announcements left over after update %lu. We assume these were deleted.", dictionary.count);
            for (BCHAnnouncement* announcement in [dictionary allValues]) {
                [self.managedObjectContext deleteObject:announcement];
            }
            
            [self.managedObjectContext save:nil];
            
            if (newAnnouncements > 0) {
                [self createNotification:newAnnouncements];
                if (self.completionHandler) {
                    self.completionHandler(UIBackgroundFetchResultNewData);
                    self.completionHandler = nil;
                }
            }
            if (self.completionHandler) {
                self.completionHandler(UIBackgroundFetchResultNoData);
                self.completionHandler = nil;
            }
        }];
    }
}

-(NSMutableDictionary*) mostRecentAnnouncements:(NSDate*) oldestDate {
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Announcement"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(date >= %@)", oldestDate];
    NSError* error = nil;
    NSArray* announcements = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error == nil) {
        NSMutableDictionary* dictionary = [NSMutableDictionary new];
        for (BCHAnnouncement* announcement in announcements) {
            [dictionary setObject:announcement forKey:announcement.announcementId];
        }
        return dictionary;
    } else {
        return nil;
    }
}

-(BCHAnnouncement*) findAnnouncementById:(NSString*) announcementId {
    NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Announcement"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"announcementId == %@", announcementId];
    NSError* error = nil;
    NSArray* results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error == nil && results.count != 0) {
        NSLog(@"Announcement record id=%@ found.", announcementId);
        return (BCHAnnouncement*) results[0];
    } else {
        return nil;
    }
}

- (void) URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession*) session {
    NSLog(@"background thing called");
    dispatch_async(dispatch_get_main_queue(), ^{
        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        if (delegate.announcementCompletionHandler) {
            delegate.announcementCompletionHandler();
        }
    });
}
@end
