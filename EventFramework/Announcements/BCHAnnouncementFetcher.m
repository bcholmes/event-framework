//
//  BCHAnnouncementFetcher.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-04-07.
//

#import "BCHAnnouncementFetcher.h"

static NSString * const kBackgroundSessionIdentifier = @"com.ayizan.backgroundsession";

@implementation BCHAnnouncementFetcher

+ (instancetype)sharedManager {
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[BCHAnnouncementFetcher alloc] init];
    });
    return sharedMyManager;
}

- (instancetype)init {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:kBackgroundSessionIdentifier];
    self = [super initWithSessionConfiguration:configuration];
    if (self) {

    }
    return self;
}


@end
