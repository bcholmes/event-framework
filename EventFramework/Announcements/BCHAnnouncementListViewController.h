//
//  BCHAnnouncementListViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-03-15.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MaterialComponents/MaterialCollections.h>
#import <MaterialComponents/MaterialFlexibleHeader.h>

@interface BCHAnnouncementListViewController : UICollectionViewController

@end
