//
//  EventJSONParser.m
//  unslacked-app
//
//  Created by BC Holmes on 11-10-27.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "EventJSONParser.h"
#import "BCHParticipation.h"
#import "BCHLink.h"

@implementation EventJSONParser

-(void) addParticipants:(NSArray*) participants toPrimaryEvent:(BCHPrimaryEvent*) primaryEvent {
    for (NSDictionary* o in participants) {
        NSString* key = [o objectForKey:@"id"];
        BCHParticipant* participant = [primaryEvent participantWithKey:key];
        BOOL added = NO;
        if (participant == nil) {
            participant = [[BCHParticipant alloc] init];
            participant.key = key;
            added = YES;
        }
        [participant applyJson:o];
        if (participant.name != nil && participant.name.length > 0 && added) {
            [primaryEvent addParticipant:participant];
        }
    }
}

-(void) addEvents:(NSArray*) events toParent:(UnslackedEvent*) parentEvent usingLocations:(NSDictionary*) locations andPrimaryEvent:(BCHPrimaryEvent*) primaryEvent{
    
    NSDate* currentDate = [NSDate date];
    for (NSDictionary* entry in events) {
        NSString* eventId = [entry objectForKey:@"id"];
        if (eventId == nil) {
            eventId = [entry objectForKey:@"databaseId"];
        }
        UnslackedEvent* event = [primaryEvent subEventForEventId:eventId];
        if (event == nil) {
            event = [[UnslackedEvent alloc] init];
            event.eventId = eventId;
            [parentEvent addSubEvent:event];
        }
        NSString* temp = [entry objectForKey:@"externalId"];
        if (temp != nil) {
            event.externalId = temp;
        }
        temp = [entry objectForKey:@"location"];
        if (temp != nil) {
            event.location = [locations objectForKey:temp];
            if (event.location == nil) {
                NSLog(@"Unable to resolve location %@", temp);
                event.location = [[BCHLocation alloc] initWithName:temp andKey:temp];
            }
        }
        UnslackedTimeRange* time = [UnslackedTimeRange fromJson:entry];
        if (time != nil) {
            event.time = time;
        }
        temp = [entry objectForKey:@"title"];
        if (temp != nil) {
            event.title = temp;
        }
        temp = [entry objectForKey:@"track"];
        if (temp != nil) {
            event.track = temp;
        }
        temp = [entry objectForKey:@"type"];
        if (temp != nil) {
            event.type = temp;
        }
        temp = [entry objectForKey:@"hashTag"];
        if (temp != nil) {
            event.hashTag = temp;
        }
        temp = [entry objectForKey:@"description"];
        if (temp != nil) {
            event.eventDescription = temp;
        }
        temp = [entry objectForKey:@"sortKey"];
        if (temp != nil) {
            event.sortKey = [temp intValue];
        }
        if (event.sortKey == 0 && event.externalId != nil) {
            event.sortKey = [event.externalId intValue] * 10;
        }
        event.lastModifiedDate = currentDate;
        temp = [entry objectForKey:@"cancelled"];
        if (temp != nil) {
            event.cancelled = [temp boolValue];
        }
        temp = [entry objectForKey:@"private"];
        if (temp != nil) {
            event.isPrivate = [temp boolValue];
        }
        NSArray* links = [entry objectForKey:@"links"];
        NSMutableArray* tempLinks = [NSMutableArray new];
        for (NSDictionary* linkJson in links) {
            BCHLink* link = [BCHLink fromJson:linkJson];
            [tempLinks addObject:link];
        }
        event.links = [NSArray arrayWithArray:tempLinks];

        
        NSArray* participants = [entry objectForKey:@"participants"];
        
        for (NSDictionary* p in participants) {
            NSString* participantId = [p objectForKey:@"id"];
            BOOL add = NO;
            BCHParticipant* participant = [primaryEvent participantWithKey:participantId];
            if (participant != nil) {
                BCHParticipation* participation = [event participationWithKey:participantId];
                if (participation == nil) {
                    participation = [[BCHParticipation alloc] init];
                    add = YES;
                    participation.participant = participant;
                }
            
                participation.type = [p objectForKey:@"type"];
                NSString* cancelled = [p objectForKey:@"cancelled"];
                if (cancelled != nil) {
                    participation.cancelled = [cancelled boolValue];
                }
                NSString* hidden = [p objectForKey:@"hidden"];
                if (hidden != nil) {
                    participation.hidden = [hidden boolValue];
                }
                if (participation.participant != nil) {
                    [participant addEvent:event];
                    if (add) {
                        [event.participation addObject:participation];
                    }
                }
            }
        }

        if ([entry objectForKey:@"events"] != nil) {
            NSArray* array = [entry objectForKey:@"events"];
            [self addEvents:array toParent:event usingLocations:locations andPrimaryEvent:primaryEvent];
        }
    }
}

-(BCHPrimaryEvent*) parseEvent:(NSDictionary*) json usingLocations:(NSDictionary*) locations {
    
    BCHPrimaryEvent* event = [[BCHPrimaryEvent alloc] init];
    [self loadJson:json toPrimaryEvent:event usingLocations:locations];
    return event;
}
-(void) loadJson:(NSDictionary*) json toPrimaryEvent:(BCHPrimaryEvent*) event usingLocations:(NSDictionary*) locations {
    NSDictionary* outer = [json objectForKey:@"event"];
    [self addParticipants:[outer objectForKey:@"participants"] toPrimaryEvent:event];
    [self addEvents:[outer objectForKey:@"events"] toParent:event usingLocations:locations andPrimaryEvent:event];
}

@end
