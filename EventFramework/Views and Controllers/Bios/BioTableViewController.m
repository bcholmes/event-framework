//
//  BioTableViewController.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-09.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>

#import "BioTableViewController.h"
#import "UIViewController+Style.h"
#import "BCHTheme.h"
#import "BCHParticipant.h"
#import "NSAttributedString+Html.h"
#import "BCHBioDetailsViewController.h"

@implementation BioTableViewController
@synthesize participants=_participants;

-(void) reloadAllBios:(NSNotification*) notification {
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.participants = [[NSMutableDictionary alloc] init];
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;

    NSArray *participants = [delegate.event.participants sortedArrayUsingSelector:@selector(compareParticipant:)];
    
    for (BCHParticipant *p in participants) {
        if (p.visible || [p.key isEqualToString:delegate.personId]) {
            NSMutableArray *array = [self.participants objectForKey:p.sortKey];
            if (array == nil)
            {
                array = [[NSMutableArray alloc] init];
                [self.participants setObject:array forKey:p.sortKey];
            }
            [array addObject:p];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadAllBios:nil];

    [self styleNavigationBar:@"Bios"];
    [self initializeMenuButton];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadAllBios:)
     name:EventUpdates
     object:nil];}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.participants.count;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *keys = [self sectionIndexTitlesForTableView:tableView];
    
    NSString *key = [keys objectAtIndex:section];
    NSArray* values = [self.participants objectForKey:key];
    return values.count;
}

- (NSArray *)titleList {
    return [[self.participants allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [self titleList];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [[self titleList] objectAtIndex:section];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"bioCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BCHParticipant* participant = [self participantAtIndexPath:indexPath];
    if (participant.avatarId != nil) {
        NSLog(@"request avatar %@", [delegate urlForAvatarId:participant.avatarId].absoluteString);
        [cell.imageView setImageWithURL:[delegate urlForAvatarId:participant.avatarId] placeholderImage:participant.defaultAvatar];
    } else {
        cell.imageView.image = participant.defaultAvatar;
    }

    BCHTheme* theme = delegate.theme;

    NSString* html = participant.fullNameAsHtml;
    if (participant.cancelled) {
        html = [NSString stringWithFormat:@"<s>%@</s>", html];
    }
    [cell.textLabel setAttributedText:[NSAttributedString fromHtml:html fontSize:16.0 styleProvider:theme]];
    
    return cell;
}

- (BCHParticipant *)participantAtIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger row = [indexPath row];
	NSUInteger section = [indexPath section];
    NSArray * keys = [self titleList];
    
    NSArray* values = [self.participants objectForKey:[keys objectAtIndex:section]];
    return [values objectAtIndex:row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BCHBioDetailsViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"bioDetails"];
    
    controller.participant = [self participantAtIndexPath:indexPath];
    UIViewController* navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.splitViewController showDetailViewController:navigationController sender:self];
}

@end
