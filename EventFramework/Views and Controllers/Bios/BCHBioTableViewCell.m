//
//  BCHBioTableViewCell.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "BCHBioTableViewCell.h"

@interface BCHBioTableViewCell()

@property (nonatomic, weak) IBOutlet UIImageView* avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel* nameLabel;

@end

@implementation BCHBioTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    UIView * selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:self.selectedBackgroundColor];
    [self setSelectedBackgroundView:selectedBackgroundView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.avatarImageView.layer.cornerRadius = 25;
    self.avatarImageView.layer.masksToBounds = YES;
}

-(UIImageView*) imageView {
    return self.avatarImageView;
}

-(UILabel*) textLabel {
    return self.nameLabel;
}

@end
