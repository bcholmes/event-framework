//
//  BioTableViewController.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-09.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BCHParticipant.h"
#import "BaseAppDelegate.h"


@interface BioTableViewController : UITableViewController {
}

@property (nonatomic, strong) NSMutableDictionary *participants;
- (BCHParticipant *)participantAtIndexPath:(NSIndexPath *)indexPath;
@end
