//
//  BCHBioDetailsViewController.h
//  WisSched
//
//  Created by BC Holmes on 2016-08-21.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCHParticipant.h"

@interface BCHBioDetailsViewController : UIViewController

@property (nonatomic, strong) BCHParticipant* participant;

@end
