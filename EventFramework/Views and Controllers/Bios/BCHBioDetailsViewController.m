//
//  BCHBioDetailsViewController.m
//  WisSched
//
//  Created by BC Holmes on 2016-08-21.
//  Copyright Â© 2016 Ayizan Studios. All rights reserved.
//

#import "BCHBioDetailsViewController.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AFNetworking/UIKit+AFNetworking.h>

#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <BlocksKit/UIControl+BlocksKit.h>

#import "BaseAppDelegate.h"
#import "BCHDivider.h"
#import "BCHEventDetailViewController.h"
#import "BCHImageService.h"
#import "BCHLink.h"
#import "BCHOpenItemButton.h"
#import "NSAttributedString+Html.h"
#import "UnslackedEvent.h"

#define MARGIN 10
#define PADDING 10


@interface BCHBioDetailsViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView* scrollView;
@property (nonatomic, weak) IBOutlet UIView* contentView;

@property (nonatomic, strong) UIImageView* avatar;

@property (nonatomic, strong) UIView* bioContentView;
@property (nonatomic, strong) UIView* linkListView;
@property (nonatomic, strong) UIView* eventListView;
@property (nonatomic, readonly) BOOL isAddImageAllowed;

@end

@implementation BCHBioDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    BCHTheme* theme = delegate.theme;
    
    self.bioContentView = [self createMainContentArea:theme];
    self.linkListView = [self createLinkListContentArea:theme];
    self.eventListView = [self createEventListContentArea:theme];
    
    if (self.eventListView == nil) {
        [self.contentView makeConstraints:^(MASConstraintMaker* make) {
            make.bottom.equalTo(self.bioContentView.bottom).with.offset(MARGIN);
        }];
        
    } else {
        [self.contentView makeConstraints:^(MASConstraintMaker* make) {
            make.bottom.equalTo(self.eventListView.bottom).with.offset(MARGIN);
        }];
    }
    
}

-(BOOL) isAddImageAllowed {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    return !self.participant.isAnonymous && [self.participant.key isEqualToString:delegate.personId];
}

-(UIView*) createMainContentArea:(BCHTheme*) theme {
    UIView* mainContent = [UIView new];
    mainContent.backgroundColor = theme.detailScreenBox;
    [self.contentView addSubview:mainContent];
    
    [self createHeading:mainContent];
    [self createBioBlurb:mainContent];
    [self createVisibilityNote:mainContent];
    [self createNonAttendanceNote:mainContent];
    
    UIView* lastSubView = [mainContent.subviews lastObject];
    
    [mainContent makeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo(self.contentView.left).with.offset(MARGIN);
        make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
        make.top.equalTo(self.contentView.top).with.offset(MARGIN);
        
        make.bottom.equalTo(lastSubView.bottom).with.offset(MARGIN);
    }];
    return mainContent;
}

-(UIView*) createEventListContentArea:(BCHTheme*) theme {
    
    if (self.participant.events.count > 0) {
        UIView* topContent = [self.contentView.subviews lastObject];
        UIView* mainContent = [UIView new];
        mainContent.backgroundColor = theme.detailScreenBox;
        [self.contentView addSubview:mainContent];
        
        for (UnslackedEvent* event in self.participant.events) {
            [self createEventButton:event parent:mainContent];
        }
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.contentView.left).with.offset(MARGIN);
            make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
            make.top.equalTo(topContent.bottom).with.offset(MARGIN);
            
            make.bottom.equalTo(lastSubView.bottom);
        }];
        return mainContent;
    } else {
        return nil;
    }
}

-(UIView*) createLinkListContentArea:(BCHTheme*) theme {
    
    if (self.participant.links.count > 0) {
        UIView* topContent = [self.contentView.subviews lastObject];
        UIView* mainContent = [UIView new];
        mainContent.backgroundColor = theme.detailScreenBox;
        [self.contentView addSubview:mainContent];
        
        for (BCHLink* link in self.participant.links) {
            [self createLinkButton:link parent:mainContent];
        }
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.contentView.left).with.offset(MARGIN);
            make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
            make.top.equalTo(topContent.bottom).with.offset(MARGIN);
            
            make.bottom.equalTo(lastSubView.bottom);
        }];
        return mainContent;
    } else {
        return nil;
    }
}

-(void) createEventButton:(UnslackedEvent*) event parent:(UIView*) mainContent {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    BCHTheme* theme = delegate.theme;
    
    BCHOpenItemButton* button = [BCHOpenItemButton new];
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    BCHParticipation* participation = [event participationWithKey:self.participant.key];
    if (self.participant.cancelled || participation.cancelled || event.cancelled) {
        button.textLabel.attributedText = [NSAttributedString fromHtml:[NSString stringWithFormat:@"<s>%@</s>", event.title] fontSize:font.pointSize styleProvider:theme];
    } else {
        button.textLabel.attributedText = [NSAttributedString fromHtml:event.title withFontSize:font.pointSize];
    }
    button.imageView.image = event.icon;
    
    [button bk_addEventHandler:^(id sender) {
        BCHEventDetailViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        controller.event = event;
        [self.navigationController pushViewController:controller animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    if (mainContent.subviews.count == 0) {
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(mainContent.top);
        }];
    } else {
        UIView* lastSubView = [mainContent.subviews lastObject];
        BCHDivider* divider = [BCHDivider new];
        [mainContent addSubview:divider];
        [divider makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom);
            make.height.equalTo(@1);
        }];
        
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(divider.bottom);
        }];
    }
}

-(void) createLinkButton:(BCHLink*) link parent:(UIView*) mainContent {
//    BaseAppDelegate* delegate = [BaseAppDelegate instance];
//    BCHTheme* theme = delegate.theme;
    
    BCHOpenItemButton* button = [BCHOpenItemButton new];
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    button.textLabel.attributedText = [NSAttributedString fromHtml:link.name withFontSize:font.pointSize];
    button.imageView.image = link.icon;
    
    [button bk_addEventHandler:^(id sender) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link.href] options:@{} completionHandler:nil];
    } forControlEvents:UIControlEventTouchUpInside];

    
    if (mainContent.subviews.count == 0) {
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(mainContent.top);
        }];
    } else {
        UIView* lastSubView = [mainContent.subviews lastObject];
        BCHDivider* divider = [BCHDivider new];
        [mainContent addSubview:divider];
        [divider makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom);
            make.height.equalTo(@1);
        }];
        
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(divider.bottom);
        }];
    }
}

-(void) createHeading:(UIView*) mainContent {
    UIView* view = [UIView new];
    self.avatar = [UIImageView new];
    self.avatar.layer.cornerRadius = 25;
    self.avatar.layer.masksToBounds = YES;
    
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (self.participant.avatarId != nil) {
        [self.avatar setImageWithURL:[delegate urlForAvatarId:self.participant.avatarId] placeholderImage:self.participant.defaultAvatar];
    } else {
        self.avatar.image = self.participant.defaultAvatar;
    }
    [view addSubview:self.avatar];
    if (self.isAddImageAllowed) {
        [self.avatar  makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(view.top);
            make.left.equalTo(view.left);
            make.height.equalTo(@50);
            make.width.equalTo(@50);
        }];
        
        UIButton* changeButton = [UIButton new];
        changeButton.titleLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2];
        [changeButton setTitleColor:delegate.theme.baseColor forState:UIControlStateNormal];
        [changeButton addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
        [changeButton setTitle:@"Change" forState:UIControlStateNormal];
        
        [view addSubview:changeButton];
        [changeButton  makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(self.avatar.bottom);
            make.bottom.equalTo(view.bottom);
            make.centerX.equalTo(self.avatar.centerX);
        }];
    } else {
        [self.avatar makeConstraints:^(MASConstraintMaker* make) {
            make.top.equalTo(view.top);
            make.bottom.equalTo(view.bottom);
            make.left.equalTo(view.left);
            make.height.equalTo(@50);
            make.width.equalTo(@50);
        }];
    }
    
    UILabel* label = [UILabel new];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    label.attributedText = [NSAttributedString fromHtml:[NSString stringWithFormat:@"<b>%@</b>", self.participant.name] withFontSize:label.font.pointSize];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    
    UIStackView* stackView = nil;
    if (self.participant.pronouns != nil) {

        UILabel* pronounLabel = [UILabel new];
        pronounLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        pronounLabel.text = self.participant.pronouns;
        pronounLabel.numberOfLines = 0;
        pronounLabel.lineBreakMode = NSLineBreakByWordWrapping;
        stackView = [[UIStackView alloc] initWithArrangedSubviews:@[ label, pronounLabel ]];
    } else {
        stackView = [[UIStackView alloc] initWithArrangedSubviews:@[ label ]];
    }
    
    stackView.axis = UILayoutConstraintAxisVertical;
    [view addSubview:stackView];
    
    [stackView makeConstraints:^(MASConstraintMaker* make) {
        make.centerY.equalTo(self.avatar.centerY);
        make.left.equalTo(self.avatar.right).with.offset(PADDING);
        make.right.equalTo(view.right);
    }];

    [mainContent addSubview:view];
    [view makeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo(mainContent.left).with.offset(PADDING);
        make.right.equalTo(mainContent.right).with.offset(-PADDING);
        make.top.equalTo(mainContent.top).with.offset(PADDING);
    }];
}


-(void) changeAvatar {
    NSLog(@"change avatar");
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeImage, nil];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

-(void) createBioBlurb:(UIView*) mainContent {
    if (self.participant.bio.length > 0) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:self.participant.bio withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label  makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}

-(void) createVisibilityNote:(UIView*) mainContent {
    if (!self.participant.visible) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:@"<i>This entry is not visible.</i>" withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label  makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}

-(void) createNonAttendanceNote:(UIView*) mainContent {
    if (self.participant.cancelled) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:[NSString stringWithFormat:@"<i>%@ is unable to attend.</i>", self.participant.name] withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label  makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}


#pragma mark - UIImagePickerControllerDelegate

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width {
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage* chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    BCHImageService* imageService = delegate.imageService;
    
    if (chosenImage.size.width > 200 ) {
        chosenImage = [self imageWithImage:chosenImage scaledToWidth:200];
    }
    
    NSLog(@"image size is %f %f", chosenImage.size.height, chosenImage.size.width);
    
    [imageService writeImageToFileSystem:chosenImage participant:self.participant];
    
    [imageService uploadAvatar:chosenImage];
    self.avatar.image = chosenImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
