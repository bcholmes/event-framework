//
//  BCHBioTableViewCell.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHBioTableViewCell : UITableViewCell

@property (nonatomic, strong) UIColor* selectedBackgroundColor;

@end
