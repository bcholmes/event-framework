//
//  LoginViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-03-15.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>
#import <Toast/UIView+Toast.h>
#import <AFNetworking/AFNetworking.h>

#import "BCHLoginViewController.h"
#import "BaseAppDelegate.h"
#import "UIDevice+Utils.h"
#import "BCHTheme.h"

#import "OnePasswordExtension.h"

#define IPHONE4_HEIGHT 480


@interface BCHLoginViewController()

@property (nonatomic, weak) IBOutlet UIView* lightboxView;
@property (nonatomic, weak) IBOutlet UITextField *usernameField;
@property (nonatomic, weak) IBOutlet UITextField *passwordField;
@property (nonatomic, weak) IBOutlet UIImageView* imageView;
@property (nonatomic, weak) IBOutlet UIButton* onePasswordButton;


@end

@implementation BCHLoginViewController

// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self.onePasswordButton setHidden:![[OnePasswordExtension sharedExtension] isAppExtensionAvailable]];
    
    self.imageView.animationImages = @[ [UIImage imageNamed:@"Icon_arm-2"], [UIImage imageNamed:@"Icon_arm-3"], [UIImage imageNamed:@"Icon_arm-4"], [UIImage imageNamed:@"Icon_arm-3"], [UIImage imageNamed:@"Icon_arm-2"], [UIImage imageNamed:@"Icon_arm-1"] ];
    self.imageView.animationDuration = 2;
    
    [self.usernameField addTarget:self action:@selector(login:) forControlEvents:UIControlEventEditingDidEndOnExit];
    [self.passwordField addTarget:self action:@selector(login:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    self.lightboxView.layer.masksToBounds = YES;
    self.lightboxView.layer.cornerRadius = 8.f;
    
    if (self.view.frame.size.height <= IPHONE4_HEIGHT) {
        for (NSLayoutConstraint* constraint in self.lightboxView.constraints) {
            if (constraint.firstAttribute == NSLayoutAttributeHeight) {
                constraint.constant = IPHONE4_HEIGHT - 40;
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}

- (IBAction) cancel: (id) sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString*) userid {
    return self.usernameField.text;
}
- (NSString*) password {
    return self.passwordField.text;
}

- (IBAction)findLoginFrom1Password:(id)sender {
    [[OnePasswordExtension sharedExtension] findLoginForURLString:@"https://account.wiscon.net" forViewController:self sender:sender completion:^(NSDictionary *loginDictionary, NSError *error) {
        if (loginDictionary.count == 0) {
            if (error.code != AppExtensionErrorCodeCancelledByUser) {
                NSLog(@"Error invoking 1Password App Extension for find login: %@", error);
            }
            return;
        }
        
        self.usernameField.text = loginDictionary[AppExtensionUsernameKey];
        self.passwordField.text = loginDictionary[AppExtensionPasswordKey];
    }];
}

- (IBAction) login: (id) sender {

    NSString* userid = self.usernameField.text;
    NSString* password = self.passwordField.text;
    
    if (userid != nil && userid.length > 0 && password != nil && password.length > 0) {

        [self.imageView startAnimating];

        BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
        [delegate.loginService loginWithUserid:userid password:password callback:^(NSError * _Nullable error) {
            [self.imageView stopAnimating];
            if (error == nil) {
                [delegate fetchAssignments];

                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                NSInteger statusCode = [[[error userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];
                if (statusCode == 401) {
                    [self.view makeToast:@"Nope nope nope nope nope."];
                    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
                } else {
                    UIAlertController * alert = [UIAlertController
                                                 alertControllerWithTitle:@"Error"
                                                 message:@"We appear to be having technical difficulties with login. Please try again later."
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction
                                      actionWithTitle:@"OK"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                      }]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
        }];
    }
}
@end
