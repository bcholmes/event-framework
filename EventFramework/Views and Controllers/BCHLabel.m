//
//  BCHLabel.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-26.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "BCHLabel.h"

@implementation BCHLabel

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    
	if (self.verticalAlignment == UIControlContentVerticalAlignmentTop ||
	   self.verticalAlignment == UIControlContentVerticalAlignmentBottom)
	{
		//	If one line, we can just use the lineHeight, faster than querying sizeThatFits
        CGFloat baseHeight = self.frame.size.height - self.edgeInsets.bottom - self.edgeInsets.top;
		const CGFloat height = ((self.numberOfLines == 1) ? ceilf(self.font.lineHeight) : [self sizeThatFits:UIEdgeInsetsInsetRect(self.frame, self.edgeInsets).size].height);
		
		if (height < baseHeight) {
			rect.origin.y = ((self.frame.size.height - height) / 2.0f) * ((self.verticalAlignment == UIControlContentVerticalAlignmentTop) ? -1.0f : 1.0f);
        }
	}
    
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}


@end
