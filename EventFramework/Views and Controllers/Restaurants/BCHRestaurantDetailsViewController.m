//
//  BCHRestaurantDetailsViewController.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "BCHRestaurantDetailsViewController.h"

#import "BaseAppDelegate.h"

#import "BCHRestaurant.h"
#import "BCHLocationMark.h"
#import "BCHTheme.h"

#define TWO_KILOMETRES 2000

#define kKMapDefaultZoomWidthInMeters 250.0

@interface BCHRestaurantDetailsViewController ()<MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet MKMapView* mapView;
@property (nonatomic, weak) IBOutlet UITableView* tableView;

@end

@implementation BCHRestaurantDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:@"Directions" style:UIBarButtonItemStylePlain target:self action:@selector(getDirections)];
    self.navigationItem.rightBarButtonItem = backButton;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.restaurant.location.coordinate, kKMapDefaultZoomWidthInMeters, kKMapDefaultZoomWidthInMeters);
    [self.mapView setRegion:region animated:YES];
    
    CLLocation *location = self.restaurant.location;
    BCHLocationMark* pin = [BCHLocationMark annotationWithCoordinate:location.coordinate andLocation:self.restaurant];
    pin.title = self.restaurant.name;
    NSString *subTitle = [self isMetric]
            ? [NSString stringWithFormat:@"%@ : %5.1fkm",self.restaurant.streetAddress,[self.restaurant.location distanceFromLocation:location] / 1000.0]
            : [NSString stringWithFormat:@"%@ : %5.1fmi",self.restaurant.streetAddress,[self.restaurant.location distanceFromLocation:location] / 1609.344];
    [pin setSubtitle:subTitle];

    [self.mapView addAnnotation:pin];
}

- (BOOL)isMetric {
    return [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MapKitDelegate

- (MKAnnotationView *)mapView:(MKMapView *)localMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    MKAnnotationView *pinView = nil;
    if([annotation isKindOfClass:[BCHLocationMark class]]){
        NSString* annotationKey = @"BCHLocationMark";
        pinView = (MKAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:annotationKey];
        if (!pinView) {
            // If an existing pin view was not available, create one
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationKey];
            pinView.image = [UIImage imageNamed:@"Icon_map_pin"];
            pinView.centerOffset = CGPointMake(0, -pinView.image.size.height / 2);
            pinView.canShowCallout = YES;
        } else {
            pinView.annotation = annotation;
        }
    }
    return pinView;
}

-(NSString*) translateToText:(BCHAvailability) availability {
    switch (availability) {
        case BCHAvailabilityYes:
            return @"Yes";
            
        case BCHAvailabilityNo:
            return @"No";
            
        default:
            return @"Unknown";
    }
}

-(MKMapItem*) createMapItem {
    MKPlacemark* placemark = [[MKPlacemark alloc] initWithCoordinate:self.restaurant.location.coordinate
                                                   addressDictionary:nil];
    MKMapItem* item = [[MKMapItem alloc] initWithPlacemark:placemark];
    item.name = self.restaurant.name;
    return item;
}
-(void) getDirections {
    MKMapItem * item = [self createMapItem];
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.restaurant.location.coordinate, TWO_KILOMETRES, TWO_KILOMETRES);
    [MKMapItem openMapsWithItems:[NSArray arrayWithObject:item]
                   launchOptions:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [NSValue valueWithMKCoordinate:region.center],
                                  MKLaunchOptionsMapCenterKey,
                                  [NSValue valueWithMKCoordinateSpan:region.span],
                                  MKLaunchOptionsMapSpanKey,
                                  MKLaunchOptionsDirectionsModeWalking,
                                  MKLaunchOptionsDirectionsModeKey,
                                  nil]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger) tableView:(UITableView*) tableView numberOfRowsInSection:(NSInteger) section {
    if (section == 0) {
        if (self.restaurant.phoneNumber.length > 0 && self.restaurant.url.length > 0) {
            return 3;
        } else if (self.restaurant.phoneNumber.length > 0 || self.restaurant.url.length > 0) {
            return 2;
        } else {
            return 1;
        }
    } else {
        return 11;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* DescriptiveCell = @"descriptiveCell";
    static NSString* SimpleData = @"simpleData";

    if (indexPath.section == 0 && indexPath.row == 0) {
        static NSString* NameCell = @"nameCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NameCell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:NameCell];
        }
        
        cell.textLabel.text = self.restaurant.name;
        cell.textLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;

        cell.detailTextLabel.text = self.restaurant.streetAddress;
        return cell;
    } else if (indexPath.section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:DescriptiveCell];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DescriptiveCell];
        }

        if (indexPath.row == 1 && self.restaurant.phoneNumber.length > 0) {
            cell.textLabel.text = self.restaurant.phoneNumber;
            cell.imageView.image = [UIImage imageNamed:@"Icon_phone"];
        } else {
            cell.textLabel.text = self.restaurant.url;
            cell.imageView.image = [UIImage imageNamed:@"Icon_link"];
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleData];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:SimpleData];
        }
        cell.textLabel.textColor = [BaseAppDelegate instance].theme.baseColor;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        
        cell.detailTextLabel.numberOfLines = 0;
        cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;

        if (indexPath.row == 0) {
            cell.textLabel.text = @"Hours";
            cell.detailTextLabel.text = self.restaurant.hours;
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"Cuisine";
            cell.detailTextLabel.text = self.restaurant.cuisine;
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"Price Range";
            cell.detailTextLabel.text = self.restaurant.priceRangeAsString;
        } else if (indexPath.row == 3) {
            cell.textLabel.text = @"Veggie entries";
            cell.detailTextLabel.text = self.restaurant.veggie;
        } else if (indexPath.row == 4) {
            cell.textLabel.text = @"Alcohol";
            cell.detailTextLabel.text = self.restaurant.alcohol;
        } else if (indexPath.row == 5) {
            cell.textLabel.text = @"Allergen";
            cell.detailTextLabel.text = self.restaurant.allergen;
        } else if (indexPath.row == 6) {
            cell.textLabel.text = @"Take-out";
            cell.detailTextLabel.text = self.restaurant.takeout;
        } else if (indexPath.row == 7) {
            cell.textLabel.text = @"Delivery";
            cell.detailTextLabel.text = [self translateToText:self.restaurant.delivery];
        } else if (indexPath.row == 8) {
            cell.textLabel.text = @"Wifi";
            cell.detailTextLabel.text = [self translateToText:self.restaurant.wifi];
        } else if (indexPath.row == 9) {
            cell.textLabel.text = @"Reservations Recommended";
            cell.detailTextLabel.text = [self translateToText:self.restaurant.reservationRecommended];
        } else if (indexPath.row == 10) {
            cell.textLabel.text = @"Memorial Day";
            cell.detailTextLabel.text = self.restaurant.memorialDayStatus;
        }
        return cell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row > 0) {
        if (indexPath.row == 1 && self.restaurant.phoneNumber.length > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", self.restaurant.fullyQualifiedPhoneNumber]] options:@{} completionHandler:nil];
         } else if (self.restaurant.url.length > 0) {
             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.restaurant.fullyQualifiedUrl] options:@{} completionHandler:nil];
         }
    }
}

@end
