//
//  BCHRestaurantListViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BCHRestaurantList.h"

@interface BCHRestaurantListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate>

@property (nonatomic, strong) BCHRestaurantList* restaurantList;
@property (nonatomic, strong) BCHRestaurantList* filteredRestaurantList;
@property (nonatomic, strong) IBOutlet UITableView* locationTableView;
@property (nonatomic, strong) IBOutlet MKMapView* mapView;

- (void)setMapViewToLocation:(CLLocation *)location;
- (void)createMapAnnotations:(CLLocation*) location;

@end
