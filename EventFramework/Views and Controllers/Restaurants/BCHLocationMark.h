//
//  BCHLocationMark.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "BCHRestaurant.h"

@interface BCHLocationMark : NSObject <MKAnnotation>
+ (id)annotationWithCoordinate:(CLLocationCoordinate2D)coordinate andLocation:(BCHRestaurant*)location;
- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andLocation:(BCHRestaurant*)location;

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, strong) BCHRestaurant* restaurant;

@end
