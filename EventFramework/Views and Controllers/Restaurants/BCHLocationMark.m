//
//  BCHLocationMark.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "BCHLocationMark.h"

@implementation BCHLocationMark

+ (id)annotationWithCoordinate:(CLLocationCoordinate2D)coordinate andLocation:(BCHRestaurant*) restaurant {
	return [[BCHLocationMark alloc] initWithCoordinate:coordinate andLocation:restaurant];
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate andLocation:(BCHRestaurant*)restaurant {
	self = [super init];
	if(nil != self) {
		self.coordinate = coordinate;
        self.restaurant = restaurant;
	}
	return self;
}

@end
