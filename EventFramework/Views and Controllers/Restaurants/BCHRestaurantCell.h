//
//  BCHRestaurantCell.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHRestaurantCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel* title;
@property (strong, nonatomic) IBOutlet UILabel* subtitle;
@property (strong, nonatomic) IBOutlet UILabel* distance;
@property (strong, nonatomic) IBOutlet UILabel* units;
@property (strong, nonatomic) IBOutlet UILabel* cuisine;
@property (strong, nonatomic) IBOutlet UILabel* cost;
@property (strong, nonatomic) IBOutlet UILabel* annotations;

@end
