//
//  BCHRestaurantDetailsViewController.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHSimpleRenderableDetailsViewController.h"

#import "BCHRestaurant.h"

@interface BCHRestaurantDetailsViewController : UIViewController

@property (nonatomic, strong) BCHRestaurant* restaurant;

@end
