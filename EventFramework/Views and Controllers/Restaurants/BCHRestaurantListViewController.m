//
//  BCHRestaurantListViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-20.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "BCHRestaurantListViewController.h"
#import "Reachability.h"
#import "BaseAppDelegate.h"
#import "BCHLocationMark.h"
#import "BCHRestaurantCell.h"
#import "UIViewController+Style.h"
#import "BCHRestaurantService.h"
#import "BCHRestaurantDetailsViewController.h"

#define kKMapDefaultZoomWidthInMeters 1000.0

@interface BCHRestaurantListViewController ()<UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController* searchController;

@property (nonatomic) BOOL isMapCentred;
@property (nonatomic) BOOL isSearching;
@property (nonatomic, strong) NSArray* restaurants;

@end

@implementation BCHRestaurantListViewController


- (void) reloadAllRestaurants:(NSNotification*) notification {
    NSLog(@"Restaurant updates received");
    if (self.restaurantList != nil) {
        self.restaurantList = [BCHRestaurantList nameBasedList:self.restaurants];
        [self.locationTableView reloadData];
    }
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    [self styleNavigationBar:@"Restaurants"];
    [self initializeMenuButton];
	self.locationTableView.delegate = self;
	self.locationTableView.dataSource = self;
    self.mapView.delegate = self;
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.restaurants = delegate.restaurantService.restaurants;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchButtonPressed:)];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadAllRestaurants:)
     name:BCHRestaurantListUpdated
     object:nil];
    
}

-(void) populateMap {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    [self createMapAnnotations:delegate.defaultLocation];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.restaurantList = [BCHRestaurantList nameBasedList:self.restaurants];
	[self.locationTableView reloadData];
    [self populateMap];
    
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    if (!self.isMapCentred) {
        [self setMapViewToLocation:delegate.defaultLocation];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.isSearching) {
        [self searchButtonPressed:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchButtonPressed:(id)sender {
    [self presentViewController:self.searchController animated:YES completion:nil];
    self.isSearching = YES;
}

- (UISearchController*) searchController {
    
    if (!_searchController) {
        
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        _searchController.searchResultsUpdater = self;
        _searchController.delegate = self;
        _searchController.obscuresBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = NO;
        self.definesPresentationContext = NO;
        [_searchController.searchBar sizeToFit];
    }
    return _searchController;
}

- (void)setMapViewToLocation:(CLLocation *)location{
	int customAnnoCount = 0;
    self.isMapCentred = YES;
	for (NSObject <MKAnnotation> *ann in self.mapView.annotations) {
		if([ann isKindOfClass:[BCHLocationMark class]]){
			customAnnoCount++;
		}
	}
	
	// Special case if there's only one annotation
	if (customAnnoCount == 1) {
		NSObject <MKAnnotation> *ann = [self.mapView.annotations objectAtIndex:0];
		MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ann.coordinate, kKMapDefaultZoomWidthInMeters, kKMapDefaultZoomWidthInMeters);
		[self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
		return;
	}
	
	CLLocationCoordinate2D maxCoord = {-90.0f, -180.0f};
	CLLocationCoordinate2D minCoord = {90.0f, 180.0f};
	for (NSObject <MKAnnotation> *ann in self.mapView.annotations) {
		if([ann isKindOfClass:[BCHLocationMark class]]){
			CLLocationCoordinate2D coord = ann.coordinate;
			if(coord.longitude > maxCoord.longitude) {
				maxCoord.longitude = coord.longitude;
			}
			if(coord.latitude > maxCoord.latitude) {
				maxCoord.latitude = coord.latitude;
			}
			if(coord.longitude < minCoord.longitude) {
				minCoord.longitude = coord.longitude;
			}
			if(coord.latitude < minCoord.latitude) {
				minCoord.latitude = coord.latitude;
			}
		}
	}
    
	MKCoordinateRegion region = {{0.0f, 0.0f}, {0.0f, 0.0f}};
    if (self.mapView.annotations.count > 0) {
        region.center.longitude = (minCoord.longitude + maxCoord.longitude) / 2.0;
        region.center.latitude = (minCoord.latitude + maxCoord.latitude) / 2.0;
        if (maxCoord.latitude == minCoord.latitude) { // this means we have a bunch of pins on the same location so zoom to the location of the first pin
            NSObject <MKAnnotation> *ann = [self.mapView.annotations objectAtIndex:0];
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ann.coordinate, kKMapDefaultZoomWidthInMeters, kKMapDefaultZoomWidthInMeters);
            [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
            return;
        } else if (self.mapView.annotations.count > 0) {
            region.span.latitudeDelta = 1.3 * (maxCoord.latitude - minCoord.latitude);		// make the region 30% bigger than necessary
            region.span.longitudeDelta = 1.3 * (maxCoord.longitude - minCoord.longitude);	// make the region 30% bigger than necessary
        }
    } else {
        region = MKCoordinateRegionMakeWithDistance(location.coordinate, kKMapDefaultZoomWidthInMeters, kKMapDefaultZoomWidthInMeters);
    }
	
	[self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

- (void)createMapAnnotations:(CLLocation*) location {
	NSMutableArray *removeList = [[NSMutableArray alloc] init];
	for (id<MKAnnotation> annotation in self.mapView.annotations){
		if ([annotation isKindOfClass:[BCHLocationMark class]]){
			BCHLocationMark* pin = (BCHLocationMark*)annotation;
			[removeList addObject:pin];
		}
	}
	for (BCHLocationMark *pin in removeList){
		[self.mapView removeAnnotation:pin];
	}
    BCHRestaurantList* list = self.isSearching ? self.filteredRestaurantList : self.restaurantList;
    
	NSMutableArray *annotationArray = [[NSMutableArray alloc] init];
	for (BCHRestaurant* restaurant in list.allEntries) {
		CLLocation *location = restaurant.location;
        if (location != nil) {
            BCHLocationMark* pin = [BCHLocationMark annotationWithCoordinate:location.coordinate andLocation:restaurant];
            pin.title = restaurant.name;
            NSString *subTitle = [self isMetric]
                ? [NSString stringWithFormat:@"%@ : %5.1fkm",restaurant.streetAddress,[restaurant.location distanceFromLocation:location] / 1000.0]
                : [NSString stringWithFormat:@"%@ : %5.1fmi",restaurant.streetAddress,[restaurant.location distanceFromLocation:location] / 1609.344];
            [pin setSubtitle:subTitle];
            [annotationArray addObject:pin];
        }
	}
	[self.mapView addAnnotations:annotationArray];
}

- (BOOL)isMetric {
    return [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue];
}

- (NSUInteger) getStoreListIndexForAnnotation:(BCHRestaurant*) restaurant {
	return [self.restaurants indexOfObject:restaurant];
}


- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{
}

- (MKAnnotationView *)mapView:(MKMapView *)localMapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
	MKAnnotationView *pinView = nil;
	if([annotation isKindOfClass:[BCHLocationMark class]]){
        BCHLocationMark *mark = (id) annotation;
        
		NSUInteger tag = [self getStoreListIndexForAnnotation:mark.restaurant];
		NSString *annotationKey = [NSString stringWithFormat:@"BCHLocationMark%ld",(long) tag];
		pinView = (MKAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:annotationKey];
		if (!pinView) {
			// If an existing pin view was not available, create one
			pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationKey];
            pinView.image = [UIImage imageNamed:@"Icon_map_pin"];
            pinView.centerOffset = CGPointMake(0, -pinView.image.size.height / 2);
			pinView.canShowCallout = YES;
            
			UIButton* rightButton = [UIButton buttonWithType: UIButtonTypeDetailDisclosure];
			rightButton.tag = tag;
			[rightButton addTarget:self action:@selector(showDetails:) forControlEvents:UIControlEventTouchUpInside];
			pinView.rightCalloutAccessoryView = rightButton;
		} else {
			pinView.annotation = annotation;
		}
	}
	return pinView;
}

- (IBAction)showDetails:(id)sender{
    NSInteger tag = ((UIButton*) sender).tag;
    if (tag >= 0 && tag < self.restaurants.count) {
        
        BCHRestaurant* restaurant = [self.restaurants objectAtIndex:tag];
        [self showRestaurantDetails:restaurant];
    }
}


#pragma mark -
#pragma mark Table View Data Source Methods

- (BCHRestaurantList*) listForTableView:(UITableView*) tableView {
    if (self.isSearching) {
        return self.filteredRestaurantList;
    } else {
        return self.restaurantList;
    }
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if ([self tableView:tableView numberOfRowsInSection:section] > 0) {
        return [[self listForTableView:tableView].keys objectAtIndex:section];
    } else {
        return nil;
    }
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self listForTableView:tableView].keys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString* key = [[self listForTableView:tableView].keys objectAtIndex:section];
    NSArray* entries = [[self listForTableView:tableView] entriesForKey:key];
    return [entries count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifier = @"BCHRestaurantCell";
    BCHRestaurantCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:CellIdentifier bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }

    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];

    NSString* key = [[self listForTableView:tableView].keys objectAtIndex:indexPath.section];
    NSArray* entries = [[self listForTableView:tableView] entriesForKey:key];

	BCHRestaurant* restaurant = [entries objectAtIndex:indexPath.row];
	
	cell.title.text = restaurant.name;
    if ([self isMetric]) {
        cell.distance.text = [NSString stringWithFormat:@"%5.1f",[restaurant.location distanceFromLocation:delegate.defaultLocation] / 1000.0];
        cell.units.text = @"km";
    } else {
        cell.distance.text = [NSString stringWithFormat:@"%5.1f",[restaurant.location distanceFromLocation:delegate.defaultLocation] / 1609.344];
        cell.units.text = @"mi";
    }
    cell.subtitle.text = restaurant.streetAddress;
    cell.cuisine.text = restaurant.cuisine;
    cell.cost.text = restaurant.priceRangeAsString;
    cell.annotations.text = restaurant.annotations;
	
	return cell;
}

- (void) showRestaurantDetails:(BCHRestaurant *)restaurant {
    [self hideSearchBar];
    BCHRestaurantDetailsViewController* detailController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"restaurantDetails"];
    detailController.restaurant = restaurant;
    [self.navigationController pushViewController:detailController animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* key = [[self listForTableView:tableView].keys objectAtIndex:indexPath.section];
    NSArray* entries = [[self listForTableView:tableView] entriesForKey:key];
    
	BCHRestaurant* restaurant = [entries objectAtIndex:indexPath.row];
    
    [self showRestaurantDetails:restaurant];
}

#pragma mark -

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString* searchText = searchController.searchBar.text;
    if (searchText != nil && searchText.length > 0) {
        NSMutableArray* temp = [[NSMutableArray alloc] init];
        for (BCHRestaurant* restaurant in self.restaurants) {
            if ([restaurant matchesString:searchText]) {
                [temp addObject:restaurant];
            }
        }
        
        self.filteredRestaurantList = [BCHRestaurantList createList:temp ofType:self.restaurantList.type];
    } else {
        self.filteredRestaurantList = self.restaurantList;
    }
    [self.locationTableView reloadData];
    [self populateMap];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void) didDismissSearchController:(UISearchController *)searchController {
    self.filteredRestaurantList = self.restaurantList;
    [self.locationTableView reloadData];
    self.isSearching = NO;
}

-(void) hideSearchBar {
    if (self.isSearching) {
        [_searchController dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
