//
//  BCHLabel.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-26.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic, assign) UIControlContentVerticalAlignment verticalAlignment;
@property (nonatomic, strong) NSString* eventId;

@end
