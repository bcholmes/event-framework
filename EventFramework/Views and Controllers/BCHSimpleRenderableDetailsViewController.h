//
//  SimpleRenderableDetailsViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-16.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "Renderable.h"

@interface BCHSimpleRenderableDetailsViewController : UIViewController<WKUIDelegate,UIGestureRecognizerDelegate,BCHImageProvider,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
}
@property (nonatomic, strong) IBOutlet WKWebView *webView;
@property (nonatomic, strong) NSObject* renderable;
@property (nonatomic) BOOL loaded;
- (void) backPage;
-(void) loadDetails;

@end
