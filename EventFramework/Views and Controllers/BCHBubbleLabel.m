//
//  BCHBubbleView.m
//  EventFramework
//
//  Created by BC Holmes on 2017-06-07.
//  Copyright © 2017 Ayizan Studios. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "BCHBubbleLabel.h"

@implementation BCHBubbleLabel

-(void) layoutSubviews {
    [super layoutSubviews];
    CALayer *layer = [self layer];
    super.backgroundColor = self.bubbleColor;
    layer.masksToBounds = YES;
    layer.cornerRadius = self.bounds.size.height / 2;
    self.textAlignment = NSTextAlignmentCenter;
}

-(CGSize) intrinsicContentSize {
    CGSize result = [super intrinsicContentSize];
    result.width += result.height;
    return result;
}

@end
