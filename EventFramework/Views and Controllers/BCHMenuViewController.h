//
//  BCHMenuViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-10-09.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHMenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UITableView* menuTableView;

- (void) showAnnouncementsPage;

@end
