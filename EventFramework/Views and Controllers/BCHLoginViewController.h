//
//  LoginViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-03-15.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseAppDelegate.h"

@interface BCHLoginViewController : UIViewController {
}

- (IBAction) login: (id) sender;
- (IBAction) cancel: (id) sender;
- (NSString*) userid;
- (NSString*) password;

@end
