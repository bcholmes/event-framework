//
//  BCHMenuViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-10-09.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <SWRevealViewController/SWRevealViewController.h>

#import "BCHMenuViewController.h"
#import "RootViewController.h"
#import "BCHMyScheduleController.h"
#import "BCHRestaurantListViewController.h"
#import "UIColor+Extensions.h"
#import "UIViewController+Style.h"
#import "BCHAnnouncementListViewController.h"
#import "BCHTheme.h"
#import "BCHLeaderboardTableViewController.h"
#import "BCHCalendarCollectionViewController.h"
#import "BCHMenuTableViewCell.h"

@interface BCHMenuViewController ()

@property (nonatomic, strong) NSArray* menuItems;
@property (nonatomic) NSInteger presentedRow;

@end

@implementation BCHMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.view.backgroundColor = delegate.theme.baseColor;
    self.menuItems = delegate.menuItems;
    self.menuTableView.backgroundView = nil;
    self.menuTableView.backgroundColor = delegate.theme.menuItemColor;
    
//    self.menuTableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"BCHMenuViewController viewWillAppear");
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.menuItems = delegate.menuItems;
    [self.menuTableView reloadData];
}

-(void)loginSucceeded:(NSNotification*) notification {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.menuItems = delegate.menuItems;
    [self.menuTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma marl - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuItem";
    BCHMenuTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSInteger row = indexPath.row;
    
    NSString *text = [self.menuItems objectAtIndex:row];
    cell.textLabel.text = NSLocalizedString( text, nil );
    BaseAppDelegate* delegate = [BaseAppDelegate instance];

    cell.backgroundColor = [UIColor clearColor]; // delegate.theme.menuItemColor;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.countLabel.textColor = [UIColor whiteColor];
    cell.countLabel.text = @"";
    cell.countLabel.hidden = YES;
    if ([text isEqualToString:@"Program"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_program"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_program_selected"];
    } else if ([text isEqualToString:@"Grid"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_grid"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_grid_selected"];
    } else if ([text isEqualToString:@"My Schedule"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_mySchedule"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_mySchedule_selected"];
        cell.countLabel.text = [NSString stringWithFormat:@"%ld", delegate.eventService.countOfItemsOnMySchedule];
        cell.countLabel.hidden = NO;
    } else if ([text isEqualToString:@"Bios"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_bio"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_bio_selected"];
    } else if ([text isEqualToString:@"Con Info"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_info"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_info_selected"];
    } else if ([text isEqualToString:@"Announcements"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_announcements"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_announcements_selected"];
    } else if ([text isEqualToString:@"Restaurants"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_restaurants"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_restaurants_selected"];
    } else if ([text isEqualToString:@"Login"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_login"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_login_selected"];
    } else if ([text isEqualToString:@"Leaderboard"]) {
        cell.imageView.image = [UIImage imageNamed:@"Menu_leaderboard"];
        cell.imageView.highlightedImage = [UIImage imageNamed:@"Menu_leaderboard_selected"];
    } else {
        cell.imageView.image = nil;
        cell.imageView.highlightedImage = nil;
    }
    UIView* selectedView = [[UIView alloc] init];
    selectedView.backgroundColor = delegate.theme.selectedMenuItemColor;
    cell.selectedBackgroundView = selectedView;

    return cell;
}


- (void) showAnnouncementsPage {
    SWRevealViewController* revealController = self.revealViewController;
    UIViewController* controller = [[UIStoryboard storyboardWithName:@"Announcements" bundle:nil] instantiateInitialViewController];
    
    NSUInteger row = 0;
    for (NSString* menuItem in self.menuItems) {
        if ([menuItem isEqualToString:@"Announcements"]) {
            self.presentedRow = row;
        }
        row++;
    }
    
    [revealController pushFrontViewController:controller animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SWRevealViewController* revealController = self.revealViewController;
    
    NSInteger row = indexPath.row;
    
    NSString *text = [self.menuItems objectAtIndex:row];
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if ([text isEqualToString:@"Login"]) {
        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        [[self getPrimaryViewController:revealController.frontViewController] login];
    } else if (row == self.presentedRow ) {
        [revealController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    } else if ([text isEqualToString:@"Program"]) {
        RootViewController* controller = [[RootViewController alloc] init];
        UIViewController* newFrontController = [[UINavigationController alloc] initWithRootViewController:controller];
        [revealController pushFrontViewController:newFrontController animated:YES];
    } else if ([text isEqualToString:@"Leaderboard"]) {
        BCHLeaderboardTableViewController* controller = [[BCHLeaderboardTableViewController alloc] init];
        UIViewController* newFrontController = [[UINavigationController alloc] initWithRootViewController:controller];
        [revealController pushFrontViewController:newFrontController animated:YES];
    } else if ([text isEqualToString:@"Announcements"]) {
        [self showAnnouncementsPage];
    } else if ([text isEqualToString:@"My Schedule"]) {
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"mySchedule"];
        UIViewController* newFrontController = [[UINavigationController alloc] initWithRootViewController:controller];
        [revealController pushFrontViewController:newFrontController animated:YES];
    } else if ([text isEqualToString:@"Grid"]) {
        BCHCalendarCollectionViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"grid"];
        UIViewController* newFrontController = [[UINavigationController alloc] initWithRootViewController:controller];
        [revealController pushFrontViewController:newFrontController animated:YES];
    } else if ([text isEqualToString:@"Bios"]) {
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"bios"];
        [revealController pushFrontViewController:controller animated:YES];
    } else if ([text isEqualToString:@"Con Info"]) {
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"conInfo"];
        [revealController pushFrontViewController:controller animated:YES];
    } else if ([text isEqualToString:@"Restaurants"]) {
        UIViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"restaurants"];
        UIViewController* newFrontController = [[UINavigationController alloc] initWithRootViewController:controller];
        [revealController pushFrontViewController:newFrontController animated:YES];
    }
    
    self.presentedRow = row;
}

- (UIViewController*) getPrimaryViewController:(UIViewController*) controller {
    if ([controller isKindOfClass:[UINavigationController class]]) {
        return ((UINavigationController*)controller).topViewController;
    } else if ([controller isKindOfClass:[UISplitViewController class]]) {
        NSArray* controllers = ((UISplitViewController* )controller).viewControllers;
        return [self getPrimaryViewController:controllers.lastObject];
    } else {
        return controller;
    }
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
