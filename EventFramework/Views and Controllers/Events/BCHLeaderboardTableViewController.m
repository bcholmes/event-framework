//
//  BCHLeaderboardTableViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-10.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHLeaderboardTableViewController.h"

#import <CoreData/CoreData.h>

#import "BaseAppDelegate.h"
#import "BCHEventDetailViewController.h"
#import "BCHEventTableViewCell.h"
#import "UIViewController+Style.h"
#import "BCHRank.h"

#define graphBarTag 8675309

@interface BCHLeaderboardTableViewController ()<NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSDictionary* events;
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property (nonatomic, strong) NSString* conventionId;

@end

@implementation BCHLeaderboardTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.conventionId = delegate.currentCon;
    self.events = [self filterAndSortEvents:[delegate allLeafEvents]];
    
    [self queryData:delegate.managedObjectContext];
    
    [self styleNavigationBar:@"Leaderboard"];
    [self initializeMenuButton];
    
}

- (void) queryData:(NSManagedObjectContext*) managedObjectContext {
    
    // Set up the fetched results controller if needed.
    if (self.fetchedResultsController == nil) {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        fetchRequest.entity = [NSEntityDescription entityForName:@"Rank" inManagedObjectContext:managedObjectContext];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"(conventionId == %@) && (score > 0)", self.conventionId];
        
        NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"score" ascending:NO];
        fetchRequest.sortDescriptors = @[ sortDescriptor1 ];//, sortDescriptor2 ];
        
        NSFetchedResultsController* resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:managedObjectContext sectionNameKeyPath:nil cacheName:nil];
        resultsController.delegate = self;
        self.fetchedResultsController = resultsController;
        
        NSError* error;
        if (![self.fetchedResultsController performFetch:&error]) {
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        }
    }
}

-(NSDictionary*) filterAndSortEvents:(NSArray*) events {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] init];
    
    for (UnslackedEvent* event in events) {
        [result setObject:event forKey:event.eventId];
    }
    
    return [NSDictionary dictionaryWithDictionary:result];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fetchedResultsController.fetchedObjects.count;
}

-(NSInteger) highestRank {
    if (self.fetchedResultsController.fetchedObjects.count > 0) {
        BCHRank* rank = self.fetchedResultsController.fetchedObjects[0];
        return [rank.score integerValue];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    static NSString *CellIdentifier = @"LeaderboardCell";
    
    float width = [UIScreen mainScreen].bounds.size.width;
    float height = [UIScreen mainScreen].bounds.size.height;
    
    if (height < width) {
        width = height;
    }

    BCHEventTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"BCHEventTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

        UIView* purple = [[UIView alloc] init];
        purple.backgroundColor = delegate.theme.lightColor;
        cell.selectedBackgroundView = purple;
        
    }
    
    BCHRank* rank = self.fetchedResultsController.fetchedObjects[indexPath.row];
    UnslackedEvent* event = [self.events objectForKey:rank.eventId];

    UIView* view = [cell viewWithTag:graphBarTag];
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width * 0.5, 60)];
        view.backgroundColor = delegate.theme.leaderboardBarColor;
        view.tag = graphBarTag;
        [cell insertSubview:view belowSubview:cell.contentView];
    }
    
    cell.imageView.image = event.icon;
    
    cell.textLabel.attributedText = [event attributedTitle:14.0];
    cell.detailTextLabel.text = event.locationAndTrack;
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [[UIView alloc] init];
    
    float highestRank = [self createUpperBound:[self highestRank]];
    float ratio = (highestRank > 0) ? [rank.score floatValue] / highestRank : 0;
    
    view.frame = CGRectMake(0, 0, width * ratio, 60);
    
    cell.scheduled = event.addedToSchedule;
    
    return cell;
}

-(float) createUpperBound:(NSInteger) rank {
    float rankFloat = rank;
    if (rankFloat < 10) {
        return rankFloat + 1.0;
    } else if (rankFloat < 25) {
        return ceilf((rankFloat+1) / 5.0) * 5.0;
    } else {
        return ceilf((rankFloat+1) / 10.0) * 10.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BCHRank* rank = self.fetchedResultsController.fetchedObjects[indexPath.row];
    UnslackedEvent* event = [self.events objectForKey:rank.eventId];
    if (event != nil) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BCHEventDetailViewController* detailController = [storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        detailController.event = event;

        [self.navigationController pushViewController:detailController animated:YES];
    }
}

#pragma mark <NSFetchedResultsControllerDelegate>

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}

@end
