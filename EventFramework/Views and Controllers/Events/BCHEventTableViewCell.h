//
//  BCHEventTableViewCell.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-04-21.
//

#import <Foundation/Foundation.h>

@interface BCHEventTableViewCell : UITableViewCell

@property (nonatomic, nullable, strong) UIColor* inkColor;
@property (nonatomic, assign) BOOL scheduled;

@end
