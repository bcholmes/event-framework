//
//  MSDayColumnHeader.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import "BCHLocationColumnHeader.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <UIColor-HexString/UIColor+HexString.h>

@interface BCHLocationColumnHeader ()

@property (nonatomic, strong) UILabel* title;
@property (nonatomic, strong) UIView* titleBackground;

@end

@implementation BCHLocationColumnHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.titleBackground = [UIView new];
        self.titleBackground.layer.cornerRadius = nearbyintf(15.0);
        [self addSubview:self.titleBackground];
        
        self.backgroundColor = [UIColor clearColor];
        self.title = [UILabel new];
        self.title.backgroundColor = [UIColor clearColor];
        [self addSubview:self.title];
        
        [self.titleBackground makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.title).with.insets(UIEdgeInsetsMake(-6.0, -12.0, -4.0, -12.0));
        }];
        
        [self.title makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
    }
    return self;
}

-(void) setLocationName:(NSString*) locationName {
    _locationName = locationName;
    self.title.text = locationName;
}

@end
