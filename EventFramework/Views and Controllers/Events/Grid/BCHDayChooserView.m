//
//  BCHDayChooserView.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-20.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHDayChooserView.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <QuartzCore/QuartzCore.h>
#import <BlocksKit/UIControl+BlocksKit.h>

#define DATE_LABEL_SIZE 28
#define DATE_LABEL_FONT_SIZE 13
#define DATE_GAP_SIZE 25


@interface BCHDayButton : UIButton
    
@end

@implementation BCHDayButton

-(void) layoutSubviews {
    [super layoutSubviews];
    [self.layer setCornerRadius:DATE_LABEL_SIZE / 2];
}

- (void) setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    if (selected) {
        self.backgroundColor = [UIColor whiteColor];
        [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    } else {
        self.backgroundColor = [UIColor clearColor];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

@end


@interface BCHDayChooserView()

@property (nonatomic, strong) UIView* containerView;

@end

@implementation BCHDayChooserView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) layoutSubviews {
    if (self.containerView != nil) {
        [self.containerView removeFromSuperview];
        self.containerView = nil;
    }
    
    if (self.containerView == nil && self.days.count > 0) {
        self.containerView = [UIView new];
        [self addSubview:self.containerView];

        [self.containerView makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(self.height);
            make.width.equalTo(@(self.days.count * DATE_LABEL_SIZE + (self.days.count -1) * DATE_GAP_SIZE));
            make.centerX.equalTo(self);
            make.top.equalTo(self.top);
        }];

        UIButton* previous = nil;
        for (NSString* day in self.days) {
            UIButton* button = [BCHDayButton new];
            button.titleLabel.font = [UIFont systemFontOfSize:DATE_LABEL_FONT_SIZE];
            [button setTitle:[day substringToIndex:2] forState: UIControlStateNormal];
            [button bk_addEventHandler:^(id sender) {
                self.selectedDay = day;
            } forControlEvents:UIControlEventTouchUpInside];
            
            if ([day isEqualToString:self.selectedDay]) {
                button.selected = YES;
            }
            
            [self.containerView addSubview:button];
            if (previous == nil) {
                [button makeConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(@(DATE_LABEL_SIZE));
                    make.width.equalTo(@(DATE_LABEL_SIZE));
                    make.left.equalTo(self.containerView.left);
                    make.top.equalTo(self.containerView.top);
                }];
            } else {
                [button makeConstraints:^(MASConstraintMaker *make) {
                    make.height.equalTo(@(DATE_LABEL_SIZE));
                    make.width.equalTo(@(DATE_LABEL_SIZE));
                    make.left.equalTo(previous.right).offset(DATE_GAP_SIZE);
                    make.top.equalTo(self.containerView.top);
                }];
            }
            previous = button;
        }
    }
}

-(void) setDays:(NSArray*) days {
    _days = days;
    [self setNeedsLayout];
}

-(void) setSelectedDay:(NSString*) day {
    NSString* originalDay = _selectedDay;
    _selectedDay = day;
    if (![originalDay isEqualToString:day]) {
        [self assignSelectedButton:day];
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

-(void) assignSelectedButton:(NSString*) day {
    for (UIView* subview in self.containerView.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            UIButton* button = (UIButton*) subview;
            if ([button.currentTitle isEqualToString:[day substringToIndex:2]]) {
                button.selected = YES;
            } else {
                button.selected = NO;
            }
        }
    }
}

@end
