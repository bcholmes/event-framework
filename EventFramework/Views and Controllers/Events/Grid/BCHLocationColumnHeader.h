//
//  MSDayColumnHeader.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHLocationColumnHeader : UICollectionReusableView

@property (nonatomic, strong) NSString* locationName;

@end
