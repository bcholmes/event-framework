//
//  MSGridlineCollectionReusableView.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import "BCHGridline.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <UIColor-HexString/UIColor+HexString.h>

@implementation BCHGridline

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

@end
