//
//  MSTimeRowHeader.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import "BCHTimeRowHeader.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <UIColor-HexString/UIColor+HexString.h>

#import "BCHDateFormattingHelper.h"

@interface BCHTimeRowHeader()

@property (nonatomic, strong) UILabel *title;

@end

@implementation BCHTimeRowHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.title = [UILabel new];
        self.title.backgroundColor = [UIColor clearColor];
        self.title.font = [UIFont systemFontOfSize:12.0];
        [self addSubview:self.title];
        
        [self.title makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.centerY);
            make.right.equalTo(self.right).offset(-5.0);
        }];
    }
    return self;
}

-(void) layoutSubviews {
    [super layoutSubviews];
    self.title.textColor = self.textColor;
}

#pragma mark - MSTimeRowHeader

- (void)setTime:(NSDate*) time {
    _time = time;    
    self.title.text = [[BCHDateFormattingHelper instance].hourAndAmPmFormatter stringFromDate:time];
    [self setNeedsLayout];
}

@end
