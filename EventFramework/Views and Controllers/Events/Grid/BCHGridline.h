//
//  MSGridlineCollectionReusableView.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Revised by BC Holmes
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHGridline : UICollectionReusableView

@end
