//
//  MSEventCell.m
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import "BCHEventCell.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <UIColor-HexString/UIColor+HexString.h>

#import "NSAttributedString+Html.h"

@interface BCHEventCell ()

@property (nonatomic, weak) IBOutlet UIView* backgroundCellView;

@property (nonatomic, weak) IBOutlet UILabel* title;
@property (nonatomic, weak) IBOutlet UILabel* track;


@end

@implementation BCHEventCell

#pragma mark - UIView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

-(void) layoutSubviews {
    [super layoutSubviews];

    [self updateColors];
    
    self.title.attributedText = [self.event attributedTitle:14.0];
    if (self.event.subTitleAsHtml) {
        self.track.attributedText = [[NSAttributedString alloc] initWithString:self.event.subTitleAsHtml attributes:[self subtitleAttributesHighlighted:self.selected]];;
    } else {
        self.track.attributedText = nil;
    }
}

#pragma mark - UICollectionViewCell

- (void)setSelected:(BOOL)selected
{
    if (selected && (self.selected != selected)) {
        [UIView animateWithDuration:0.1 animations:^{
            self.transform = CGAffineTransformMakeScale(1.025, 1.025);
            self.layer.shadowOpacity = 0.2;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                self.transform = CGAffineTransformIdentity;
            }];
        }];
    } else if (selected) {
        self.layer.shadowOpacity = 0.2;
    } else {
        self.layer.shadowOpacity = 0.0;
    }
    [super setSelected:selected]; // Must be here for animation to fire
    [self updateColors];
}

#pragma mark - MSEventCell

-(void) setEvent:(BCHScheduledItem*) event {
    _event = event;
}

- (void)updateColors
{
    self.backgroundCellView.backgroundColor = [self backgroundColorSelected:self.selected];
    self.title.textColor = [self textColorHighlighted:self.selected];
    self.track.textColor = [self subTextColorHighlighted:self.selected];
}

- (NSDictionary *)titleAttributesHighlighted:(BOOL)highlighted
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.hyphenationFactor = 1.0;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    return @{
        NSFontAttributeName : [UIFont boldSystemFontOfSize:12.0],
        NSForegroundColorAttributeName : [self textColorHighlighted:highlighted],
        NSParagraphStyleAttributeName : paragraphStyle
    };
}

- (NSDictionary *)subtitleAttributesHighlighted:(BOOL)highlighted
{
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.alignment = NSTextAlignmentLeft;
    paragraphStyle.hyphenationFactor = 1.0;
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    return @{
        NSFontAttributeName : [UIFont systemFontOfSize:12.0],
        NSForegroundColorAttributeName : [self subTextColorHighlighted:highlighted],
        NSParagraphStyleAttributeName : paragraphStyle
    };
}

- (UIColor *)backgroundColorSelected:(BOOL)selected {
    if (selected) {
        return [self.cellSelectionColor colorWithAlphaComponent:0.85];
    } else if (self.event.highlighted) {
        return [self.highligherColor colorWithAlphaComponent:0.85];
    } else {
        return [self.backgroundColor colorWithAlphaComponent:0.85];
    }
}

- (UIColor *)textColorHighlighted:(BOOL)selected {
    return selected ? [UIColor blackColor] : [UIColor colorWithHexString:@"333333"];
}

- (UIColor*) subTextColorHighlighted:(BOOL)selected {
    return [UIColor colorWithHexString:@"666666"];
}

@end
