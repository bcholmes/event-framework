//
//  MSEventCell.h
//  Example
//
//  Created by Eric Horacek on 2/26/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCHScheduledItem.h"

@interface BCHEventCell : UICollectionViewCell

@property (nonatomic, weak) BCHScheduledItem* event;

@property (nonatomic, strong) UIColor* backgroundColor;
@property (nonatomic, strong) UIColor* cellSelectionColor;
@property (nonatomic, strong) UIColor* highligherColor;

@end
