//
//  BCHDayChooserView.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-20.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHDayChooserView : UIControl

@property (nonatomic, strong) NSArray* days;
@property (nonatomic, strong) NSString* selectedDay;

@end
