//
//  MSDayColumnHeaderBackground.m
//  Example
//
//  Created by Eric Horacek on 2/28/13.
//  Copyright (c) 2015 Eric Horacek. All rights reserved.
//

#import "BCHLocationColumnHeaderBackground.h"
#define MAS_SHORTHAND
#import <Masonry/Masonry.h>
#import <UIColor-HexString/UIColor+HexString.h>

@implementation BCHLocationColumnHeaderBackground

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor colorWithHexString:@"f7f7f7"] colorWithAlphaComponent:0.8];
    }
    return self;
}

@end
