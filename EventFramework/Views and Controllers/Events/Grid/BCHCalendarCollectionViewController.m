//
//  BCHCalendarCollectionViewController.m
//  WisSched
//
//  Created by BC Holmes on 2016-03-13.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHCalendarCollectionViewController.h"

#import "BaseAppDelegate.h"
#import "BCHCollectionViewCalendarLayout.h"
#import "BCHConInfoControllerHelper.h"
#import "BCHContentCollection.h"
#import "BCHCurrentTimeIndicator.h"
#import "BCHDayChooserView.h"
#import "BCHEventCell.h"
#import "BCHEventDetailViewController.h"
#import "BCHGridViewService.h"
#import "BCHGridline.h"
#import "BCHLocationColumnHeader.h"
#import "BCHLocationColumnHeaderBackground.h"
#import "BCHSimpleScheduledItem.h"
#import "BCHTimeRowHeader.h"
#import "BCHTimeRowHeaderBackground.h"
#import "MSCurrentTimeGridline.h"
#import "NSDate+Utils.h"
#import "UIViewController+Style.h"

@interface BCHCalendarCollectionViewController () <BCHCollectionViewDelegateCalendarLayout, UICollectionViewDataSource>

@property (nonatomic, strong) BCHCollectionViewCalendarLayout *collectionViewCalendarLayout;

@property (nonatomic, weak) IBOutlet UICollectionView* collectionView;
@property (nonatomic, weak) IBOutlet BCHDayChooserView* dayChooserView;

@property (nonatomic, strong) BCHGridViewService* gridViewService;

@property (nonatomic, strong) NSString* currentDay;
@property (nonatomic, strong) NSArray* eventList;
@property (nonatomic, strong) NSArray* locationList;
@property (nonatomic, strong) NSDictionary* eventSections;
@property (nonatomic, strong) NSArray* days;

@end

@implementation BCHCalendarCollectionViewController

NSString * const MSEventCellReuseIdentifier = @"MSEventCellReuseIdentifier";
NSString * const MSDayColumnHeaderReuseIdentifier = @"MSDayColumnHeaderReuseIdentifier";
NSString * const MSTimeRowHeaderReuseIdentifier = @"MSTimeRowHeaderReuseIdentifier";


- (void)viewDidLoad {
    [super viewDidLoad];
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    
    self.gridViewService = [[BCHGridViewService alloc] initWithEventService:delegate.eventService];
    
    self.collectionViewCalendarLayout = [BCHCollectionViewCalendarLayout new];
    self.collectionViewCalendarLayout.sectionLayoutType = MSSectionLayoutTypeHorizontalTile;
    self.collectionViewCalendarLayout.delegate = self;
    self.collectionViewCalendarLayout.hourHeight = 60;
    self.collectionViewCalendarLayout.dayColumnHeaderHeight = 35;
    
    self.collectionView.collectionViewLayout = self.collectionViewCalendarLayout;

    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    [self styleNavigationBar:@"Grid"];
    [self initializeMenuButton];

    self.days = self.gridViewService.allDays;
    self.dayChooserView.days = self.days;
    
    [self.dayChooserView addTarget:self action:@selector(dayChosen:) forControlEvents:UIControlEventValueChanged];
    
    [self daySelected:[[NSDate date] dayString:delegate.timeZone]];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"BCHEventCell" bundle:nil] forCellWithReuseIdentifier:MSEventCellReuseIdentifier];
    [self.collectionView registerClass:BCHLocationColumnHeader.class forSupplementaryViewOfKind:MSCollectionElementKindDayColumnHeader withReuseIdentifier:MSDayColumnHeaderReuseIdentifier];
    [self.collectionView registerClass:BCHTimeRowHeader.class forSupplementaryViewOfKind:MSCollectionElementKindTimeRowHeader withReuseIdentifier:MSTimeRowHeaderReuseIdentifier];
    
    self.collectionViewCalendarLayout.sectionWidth = self.layoutSectionWidth;
    
    // These are optional. If you don't want any of the decoration views, just don't register a class for them.
    [self.collectionViewCalendarLayout registerClass:BCHCurrentTimeIndicator.class forDecorationViewOfKind:MSCollectionElementKindCurrentTimeIndicator];
    [self.collectionViewCalendarLayout registerClass:MSCurrentTimeGridline.class forDecorationViewOfKind:MSCollectionElementKindCurrentTimeHorizontalGridline];
    [self.collectionViewCalendarLayout registerClass:BCHGridline.class forDecorationViewOfKind:MSCollectionElementKindVerticalGridline];
    [self.collectionViewCalendarLayout registerClass:BCHGridline.class forDecorationViewOfKind:MSCollectionElementKindHorizontalGridline];
    [self.collectionViewCalendarLayout registerClass:BCHTimeRowHeaderBackground.class forDecorationViewOfKind:MSCollectionElementKindTimeRowHeaderBackground];
    [self.collectionViewCalendarLayout registerClass:BCHLocationColumnHeaderBackground.class forDecorationViewOfKind:MSCollectionElementKindDayColumnHeaderBackground];
    
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void) dayChosen:(BCHDayChooserView *)dayChooserView {
    [self daySelected:dayChooserView.selectedDay];

    [self.collectionView setContentOffset:CGPointZero animated:YES];
    [self.collectionViewCalendarLayout invalidateLayoutCache];
    [self.collectionView reloadData];
}

-(void) daySelected:(NSString*) day {
    if ([self.days containsObject:day]) {
        self.currentDay = day;
    } else {
        self.currentDay = self.days[0];
    }
    self.dayChooserView.selectedDay = self.currentDay;

    NSArray* events = [self.gridViewService itemsForDay:self.currentDay];
    self.eventList = [events sortedArrayUsingSelector:@selector(compareItem:)];
    
    [self collateEvents:self.eventList];
}

-(void) collateEvents:(NSArray*) events {
    NSMutableSet* locations = [NSMutableSet new];
    NSMutableDictionary* eventDictionary = [NSMutableDictionary new];
    for (BCHScheduledItem* event in events) {
        [locations addObjectsFromArray:event.location.allLocations];
        BCHLocation* location = event.location.firstLocation;
        if (location.key != nil) {
            if ([eventDictionary objectForKey:location.key] == nil) {
                [eventDictionary setObject:[NSMutableArray new] forKey:location.key];
            }
            NSMutableArray* array = [eventDictionary objectForKey:location.key];
            [array addObject:event];
        }
    }
    NSArray* tempLocations = [NSArray arrayWithArray:[locations allObjects]];
    self.locationList = [tempLocations sortedArrayUsingSelector:@selector(compare:)];
    
    self.eventSections = [NSDictionary dictionaryWithDictionary:eventDictionary];
}

- (CGFloat)layoutSectionWidth {
    return 180;
}



#pragma mark - MSCollectionViewCalendarLayout

-(NSDate*) collectionView:(UICollectionView *)collectionView layout:(BCHCollectionViewCalendarLayout *)collectionViewCalendarLayout dayForSection:(NSInteger)section {
    
    return ((BCHScheduledItem*) self.eventList[0]).time.startTime;
}

-(NSDate*) collectionView:(UICollectionView *)collectionView layout:(BCHCollectionViewCalendarLayout *)collectionViewCalendarLayout startTimeForItemAtIndexPath:(NSIndexPath*) indexPath {
    BCHScheduledItem* event = [self itemAtIndexPath:indexPath];
    return event.time.startTime;
}

-(NSDate*) collectionView:(UICollectionView *)collectionView layout:(BCHCollectionViewCalendarLayout *)collectionViewCalendarLayout endTimeForItemAtIndexPath:(NSIndexPath *) indexPath {
    BCHScheduledItem* event = [self itemAtIndexPath:indexPath];
    return event.time.endTime;
}

-(NSUInteger) collectionView:(UICollectionView*) collectionView layout:(BCHCollectionViewCalendarLayout *)collectionViewLayout columnSpanForItemAtIndexPath:(NSIndexPath *)indexPath {
    BCHScheduledItem* event = [self itemAtIndexPath:indexPath];
    return event.location.allLocations.count;
}

- (NSDate *)currentTimeComponentsForCollectionView:(UICollectionView *)collectionView layout:(BCHCollectionViewCalendarLayout *)collectionViewCalendarLayout {
    return [NSDate date];
}


-(BCHScheduledItem*) itemAtIndexPath:(NSIndexPath*) indexPath {
    BCHLocation* location = self.locationList[indexPath.section];
    if (location) {
        NSArray* events = [self.eventSections objectForKey:location.key];
        return events[indexPath.row];
    } else {
        return nil;
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.locationList.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    BCHLocation* location = self.locationList[section];
    if (location) {
        NSArray* events = [self.eventSections objectForKey:location.key];
        return events.count;
    } else {
        return 0;
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {

    if (kind == MSCollectionElementKindDayColumnHeader) {
        BCHLocationColumnHeader* header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:MSDayColumnHeaderReuseIdentifier forIndexPath:indexPath];
        BCHLocation* location = [self.locationList objectAtIndex:indexPath.section];
        header.locationName = location.displayName;
        
        return header;
    } else if (kind == MSCollectionElementKindTimeRowHeader) {
        BCHTimeRowHeader *timeRowHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:MSTimeRowHeaderReuseIdentifier forIndexPath:indexPath];
        timeRowHeader.time = [self.collectionViewCalendarLayout dateForTimeRowHeaderAtIndexPath:indexPath];
        return timeRowHeader;
    } else {
        return nil;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    BCHEventCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:MSEventCellReuseIdentifier forIndexPath:indexPath];
    
    BCHScheduledItem* event = [self itemAtIndexPath:indexPath];
    cell.event = event;
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BCHScheduledItem *event = [self itemAtIndexPath:indexPath];
    
    if ([event isMemberOfClass:[UnslackedEvent class]]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BCHEventDetailViewController* detailController = [storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        detailController.event = (UnslackedEvent*) event;
        
        [self.navigationController pushViewController:detailController animated:YES];
    } else if ([event isMemberOfClass:[BCHSimpleScheduledItem class]]) {
        BCHSimpleScheduledItem* item = (BCHSimpleScheduledItem*) event;
        NSLog(@"href: %@", item.href);
        if (item.href.length > 0) {

            id<ContentItem> content = [[BaseAppDelegate instance].contentCollection contentPageForLink:item.href];
            if (content) {
                NSLog(@"about to push %@", self.navigationController == nil ? @"but no navigation controller" : @"and all looks good");
                UIViewController* controller = [[BCHConInfoControllerHelper new] undecoratedControllerFor:content];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    }
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

@end
