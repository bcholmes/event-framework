//
//  MyScheduleController.m
//  unslacked-app
//
//  Created by BC Holmes on 12-04-25.
//  Copyright (c) 2012 Ayizan Studios. All rights reserved.
//

#import "BCHMyScheduleController.h"

#import <MGSwipeTableCell/MGSwipeTableCell.h>

#import "BaseAppDelegate.h"
#import "BCHReminder.h"
#import "UIViewController+Style.h"

@implementation BCHMyScheduleController

- (UITableViewCell *)tableView:(UITableView *)tableView cellForEvent:(NSObject<BCHScheduleEntry>*)event {
    static NSString *CellIdentifier = @"eventCell";
    
    MGSwipeTableCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [self populateCell:cell fromEvent:event];
    
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Remove" backgroundColor:[UIColor redColor] callback:^BOOL(MGSwipeTableCell * _Nonnull cell) {
        
        NSIndexPath* indexPath = [tableView indexPathForCell:cell];
        NSObject<BCHScheduleEntry>* temp = [[self currentEventTable] entryAtIndexPath:indexPath accessType:self.accessType];
        if ([temp isKindOfClass:[UnslackedEvent class]]) {
            [[BaseAppDelegate instance].eventService changeScheduledStatusOfEvent:((UnslackedEvent*) temp) added:NO];
        } else if ([temp isKindOfClass:[BCHReminder class]]) {
            NSManagedObjectContext* context = [BaseAppDelegate instance].managedObjectContext;
            [context performBlock:^{
                [context deleteObject:(BCHReminder*) temp];
                [context save:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:BCHReminderChangedNotification object:nil];
            }];
        }
        
        return YES;
    }]];
    return cell;
}


-(void)eventScheduled:(NSNotification*) notification {
    NSLog(@"event scheduled : notification");
    NSInteger sectionNumber = [self.eventTable sectionNumberFor:notification.object];
    if (sectionNumber != NSNotFound) {
        [self.tableView beginUpdates];
        NSIndexSet* array = [NSIndexSet indexSetWithIndex:sectionNumber];
        NSLog(@"reloadSections");
        [self.tableView reloadSections:array withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

-(void) reloadAllEvents:(NSNotification*) notification {
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableArray* allItems = [NSMutableArray new];
    [allItems addObjectsFromArray:[delegate allLeafEvents]];
    NSManagedObjectContext* context = [BaseAppDelegate instance].managedObjectContext;
    [context performBlock:^{
        NSFetchRequest* fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Reminder"];
        NSError* error = nil;
        NSArray* results = [context executeFetchRequest:fetchRequest error:&error];
        
        if (error == nil && results != nil) {
            [allItems addObjectsFromArray:results];
        }
        self.eventTable = [BCHScheduleEntryTable tableFromEvents:allItems];
        [self.tableView reloadData];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    self.accessType = EventTableAccessTypeSelected;

    [self styleNavigationBar:@"My Schedule"];
    [self initializeMenuButton];
    
    if ([BaseAppDelegate instance].supportsReminders) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createReminder)];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:EventChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAllEvents:) name:MyScheduleChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAllEvents:) name:BCHReminderChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAllEvents:) name:EventUpdates object:nil];
    
    [self reloadAllEvents:nil];
}

-(void) createReminder {
    [self performSegueWithIdentifier:@"createReminder" sender:nil];
}

@end
