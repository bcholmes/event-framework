//
//  NoteViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-11.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnslackedEvent.h"

@interface NoteViewController : UIViewController<UINavigationBarDelegate>

@property (nonatomic, strong) UnslackedEvent* event;
@property (nonatomic, strong) IBOutlet UITextView* textView;
- (IBAction)noteDone;

@end
