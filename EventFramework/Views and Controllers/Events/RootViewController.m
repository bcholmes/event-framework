//
//  RootViewController.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-01.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RootViewController.h"
#import "UnslackedEvent.h"
#import "BaseAppDelegate.h"
#import "BCHLoginViewController.h"
#import "SWRevealViewController.h"
#import "BCHTheme.h"
#import "UIViewController+Style.h"

@interface RootViewController()<UISearchControllerDelegate, UISearchResultsUpdating>

@property (nonatomic, strong) UISearchController* searchController;
@property (nonatomic, assign) BOOL isSearching;

@end


@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.accessType = EventTableAccessTypeAll;
    self.eventTable = [BCHScheduleEntryTable tableFromEvents:[delegate allLeafEvents]];
    self.filteredEventsTable = nil;
    
    [self styleNavigationBar:@"Program"];
    [self initializeMenuButton];
    
    UIBarButtonItem* searchButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                  target:self
                                                                                  action:@selector(searchButtonPressed:)];
    self.navigationItem.rightBarButtonItem = searchButton;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:EventChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:MyScheduleChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAllEvents:) name:EventUpdates object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollIfNecessary) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    delegate.shouldScrollToCurrentTimePeriod = YES;
}

-(void) showLoginAlert {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login?" message:@"Program participants: do you want to get your schedule?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [self login];
    }];
    UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"Not now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
    }];
    [alert addAction:okAction];
    [alert addAction:otherAction];
    [self presentViewController:alert animated:YES completion:nil];
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    [delegate markAsPromptedForLogin];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self scrollIfNecessary];
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    if (delegate.supportsLogin && !delegate.isAlreadyPromptedForLogin) {
        [self showLoginAlert];
    }

    if (self.filteredEventsTable != nil) {
        [self searchButtonPressed:nil];
    }
}

-(void) scrollIfNecessary {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    if (delegate.shouldScrollToCurrentTimePeriod && !self.isSearching) {
        UnslackedTimeRange* range = [self.eventTable closestTimeRangeToNow:delegate.timeZone];
        if (range != nil) {
            NSInteger sectionNumber = [self.eventTable sectionNumberForRange:range];
            NSIndexPath* path = sectionNumber >= 0 ? [NSIndexPath indexPathForRow:0 inSection:sectionNumber] : nil;
            if (path != nil) {
                [self.tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        }
        delegate.shouldScrollToCurrentTimePeriod = NO;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [super numberOfSectionsInTableView:tableView] + ([BaseAppDelegate instance].surveyUrl != nil ? 1 : 0);
}

-(BOOL) isLastSection:(NSInteger) section tableView:(UITableView*) tableView {
    return section == [self numberOfSectionsInTableView:tableView] -1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self isLastSection:section tableView:tableView] && [BaseAppDelegate instance].surveyUrl) {
        return 1;
    } else {
        return [super tableView:tableView numberOfRowsInSection:section];
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section  {
    if ([self isLastSection:section tableView:tableView] && [BaseAppDelegate instance].surveyUrl) {
        return @"After the Con";
    } else {
        return [super tableView:tableView titleForHeaderInSection:section];
    }
}

- (UITableViewCell*) tableView:(UITableView*) tableView cellForRowAtIndexPath:(NSIndexPath*) indexPath {
    if ([self isLastSection:indexPath.section tableView:tableView] && [BaseAppDelegate instance].surveyUrl) {
        UITableViewCell* cell =  [tableView dequeueReusableCellWithIdentifier:@"addendum"];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"addendum"];
        }
        cell.textLabel.text = @"Don't forget the WisCon Survey!";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    } else {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)searchButtonPressed:(id)sender {
    [self presentViewController:self.searchController animated:YES completion:nil];
}

- (UISearchController*) searchController {
    
    if (!_searchController) {
        
        _searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
        _searchController.searchResultsUpdater = self;
        _searchController.delegate = self;
        _searchController.obscuresBackgroundDuringPresentation = NO;
        _searchController.hidesNavigationBarDuringPresentation = NO;
        self.definesPresentationContext = NO;
        [_searchController.searchBar sizeToFit];
        self.isSearching = YES;
    }
    return _searchController;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideSearchBar];
    if ([self isLastSection:indexPath.section tableView:tableView] && [BaseAppDelegate instance].surveyUrl) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[BaseAppDelegate instance].surveyUrl] options:@{} completionHandler:nil];
    } else {
        [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    }
}
#pragma mark - UISearchResultsUpdating protocol

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString* searchText = searchController.searchBar.text;
    if (searchText != nil && searchText.length > 0) {
        self.filteredEventsTable = [self.eventTable eventsContainingSearchString:searchText];
    } else {
        self.filteredEventsTable = nil;
    }
    [self.tableView reloadData];
}

- (BCHScheduleEntryTable*) currentEventTable{
    if (self.filteredEventsTable != nil) {
        return self.filteredEventsTable;
    } else {
        return [super currentEventTable];
    }
}

-(void) didDismissSearchController:(UISearchController *)searchController {
    self.isSearching = NO;
    self.filteredEventsTable = nil;
    [self.tableView reloadData];
}

-(void) hideSearchBar {
    [_searchController dismissViewControllerAnimated:NO completion:nil];
}

@end
