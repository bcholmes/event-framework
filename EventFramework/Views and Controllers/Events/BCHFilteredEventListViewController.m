//
//  BCHFilteredEventListViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-03.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHFilteredEventListViewController.h"
#import "BaseAppDelegate.h"
#import "UIViewController+Style.h"

@implementation BCHFilteredEventListViewController

-(void)eventScheduled:(NSNotification*) notification {
    NSLog(@"event scheduled : notification");
    NSInteger sectionNumber = [self.eventTable sectionNumberFor:notification.object];
    if (sectionNumber != NSNotFound) {
        [self.tableView beginUpdates];
        NSIndexSet* array = [NSIndexSet indexSetWithIndex:sectionNumber];
        NSLog(@"reloadSections");
        [self.tableView reloadSections:array withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"viewDidLoad");
    self.accessType = EventTableAccessTypeAll;
    self.eventTable = [[BCHScheduleEntryTable tableFromEvents:[delegate allLeafEvents]] filteredByEventType:self.eventFilter.eventType];
    self.navigationController.navigationBar.translucent = NO;

    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(eventDidChange:)
     name:EventChangedNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(eventScheduled:)
     name:MyScheduleChangedNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadAllEvents:)
     name:EventUpdates
     object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = self.eventFilter.title;
}

@end
