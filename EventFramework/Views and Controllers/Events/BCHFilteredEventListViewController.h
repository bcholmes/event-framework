//
//  BCHFilteredEventListViewController.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-03.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BaseEventListController.h"
#import "BCHEventFilter.h"

@interface BCHFilteredEventListViewController : BaseEventListController

@property (nonatomic, strong) BCHEventFilter* eventFilter;

@end
