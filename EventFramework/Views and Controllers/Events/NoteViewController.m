//
//  NoteViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-11.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "NoteViewController.h"

@implementation NoteViewController

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.textView.text = self.event.notes;
}

-(IBAction)noteDone {
    self.event.notes = self.textView.text;
    [[NSNotificationCenter defaultCenter] postNotificationName:EventChangedNotification object:self.event];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
