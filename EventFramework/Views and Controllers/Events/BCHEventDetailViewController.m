//
//  BCHEventDetailViewController.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-04.
//

#import "BCHEventDetailViewController.h"

#define MAS_SHORTHAND
#import <BlocksKit/UIControl+BlocksKit.h>
#import <Toast/UIView+Toast.h>
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIKit+AFNetworking.h>
#import <NHCalendarActivity/NHCalendarActivity.h>
#import <PPBadgeView/UIBarButtonItem+PPBadgeView.h>
#import <Twitter/TWTweetComposeViewController.h>
#import <Masonry/Masonry.h>

#import "BaseAppDelegate.h"
#import "BCHBioDetailsViewController.h"
#import "BCHConInfoControllerHelper.h"
#import "BCHContentCollection.h"
#import "BCHDivider.h"
#import "BCHLink.h"
#import "BCHOpenItemButton.h"
#import "BCHRank.h"
#import "BCHToggleBarButtonItem.h"


#define MARGIN 10
#define PADDING 10


@interface BCHEventDetailViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (nonatomic, weak) IBOutlet UIScrollView* scrollView;
@property (nonatomic, weak) IBOutlet UIView* contentView;
@property (nonatomic, weak) IBOutlet BCHToggleBarButtonItem* scheduleButton;
@property (nonatomic, weak) IBOutlet BCHToggleBarButtonItem* highlightButton;
@property (nonatomic, weak) IBOutlet BCHToggleBarButtonItem* yayButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem* hashtagButton;

@property (nonatomic, strong) UIImageView* avatar;

@property (nonatomic, strong) UIView* eventContentView;
@property (nonatomic, strong) UIView* linkListView;
@property (nonatomic, strong) UIView* eventListView;
@property (nonatomic, strong) UIView* participantListView;
@property (nonatomic, strong) BCHRank* rank;

@end


@implementation BCHEventDetailViewController

- (void) populateContents {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    BCHTheme* theme = delegate.theme;
    
    self.eventContentView = [self createMainContentArea:theme];
    self.linkListView = [self createLinkListContentArea:theme];
    self.eventListView = [self createEventListContentArea:theme];
    self.participantListView = [self createParticipantListContentArea:theme];

    UIView* lastSubView = [self.contentView.subviews lastObject];
    
    [self.contentView makeConstraints:^(MASConstraintMaker* make) {
        make.bottom.equalTo(lastSubView.bottom).with.offset(MARGIN);
    }];

    [self toggleToolbarButtons];
    [self configureUI];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;

    self.scheduleButton.selectedImage = [UIImage imageNamed:@"Button_calendar_selected"];
    self.scheduleButton.nonselectedImage = [UIImage imageNamed:@"Button_calendar"];
    
    self.highlightButton.selectedImage = [UIImage imageNamed:@"Button_highlighter_selected"];
    self.highlightButton.nonselectedImage = [UIImage imageNamed:@"Button_highlighter"];
    
    self.yayButton.selectedImage = [UIImage imageNamed:@"Button_yay_selected"];
    self.yayButton.nonselectedImage = [UIImage imageNamed:@"Button_yay"];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showActivityView:)];
    self.navigationItem.rightBarButtonItem = button;

    [self populateContents];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:EventChangedNotification object:self.event];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventDidChange:) name:MyScheduleChangedNotification object:self.event];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadEvent:) name:EventUpdates object:nil];
}
-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self configureUI];
}

- (IBAction)showActivityView:(id)sender {
    NSString *msg = self.event.fullTitleNoHtml;
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"https://account.wiscon.net/program/detail?idItems=%@",self.event.eventId]];
    
    NHCalendarActivity* calendarActivity = [[NHCalendarActivity alloc] init];
    calendarActivity.activityImage = [UIImage imageNamed:@"Icon_calendar_grey"];
    NSArray *activities = @[ calendarActivity ];
    NSArray *items = @[ msg, url, [self createCalendarEvent] ];
    
    UIActivityViewController* activity = [[UIActivityViewController alloc] initWithActivityItems:items
                                                                           applicationActivities:activities];
    [self presentViewController:activity animated:YES completion:NULL];
}

-(NHCalendarEvent *)createCalendarEvent {
    NHCalendarEvent *calendarEvent = [[NHCalendarEvent alloc] init];
    
    calendarEvent.title = self.event.fullTitleNoHtml;
    calendarEvent.location = self.event.location.displayName;
    calendarEvent.startDate = self.event.time.startTime;
    calendarEvent.endDate = self.event.time.endTime;
    calendarEvent.allDay = NO;
    
    // Add alarm
    NSArray *alarms = @[
                        [EKAlarm alarmWithRelativeOffset:- 60.0f * 5.0f]        // 5 minutes before
                        ];
    calendarEvent.alarms = alarms;
    
    return calendarEvent;
}

-(void) configureUI {
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    self.rank = [delegate.eventService findRankForEventId:self.event.eventId];
    
    BCHTheme* theme = delegate.theme;

    if (self.event.highlighted) {
        self.eventContentView.backgroundColor = [UIColor yellowColor];
    } else {
        self.eventContentView.backgroundColor = theme.detailScreenBox;
    }
    
    if ([self.rank.yayCount integerValue] > 0) {
        [self.yayButton pp_addBadgeWithNumber:[self.rank.yayCount integerValue]];
        [self.yayButton pp_showBadge];
    } else {
        [self.yayButton pp_hiddenBadge];
    }
}

-(void) toggleToolbarButtons {
    self.scheduleButton.toggled = self.event.selection.addedToSchedule;
    self.highlightButton.toggled = self.event.selection.highlight;
    self.yayButton.toggled = self.event.selection.yay;
}

-(void) reloadEvent:(NSNotification*) notification {
    for (UIView* view in [self.contentView subviews]) {
        [view removeFromSuperview];
    }
    [self populateContents];
    [self.view setNeedsDisplay];
}

-(void)eventDidChange:(NSNotification*) notification {
    [self toggleToolbarButtons];
    [self configureUI];
    [self.view setNeedsDisplay];
}

-(UIView*) createMainContentArea:(BCHTheme*) theme {
    UIView* mainContent = [UIView new];
    mainContent.backgroundColor = theme.detailScreenBox;
    [self.contentView addSubview:mainContent];
    
    [self createHeading:mainContent];
    [self createMapImage:mainContent theme:theme];
    [self createSubtitle:mainContent theme:theme];
    [self createTextBlurb:mainContent];
    [self createMyEventBlurb:mainContent theme:theme];
    [self createTwitterButton:mainContent theme:theme];
    [self createCancelledMessage:mainContent theme:theme];

    UIView* lastSubView = [mainContent.subviews lastObject];
    
    [mainContent makeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo(self.contentView.left).with.offset(MARGIN);
        make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
        make.top.equalTo(self.contentView.top).with.offset(MARGIN);
        
        make.bottom.equalTo(lastSubView.bottom).with.offset(MARGIN);
    }];
    return mainContent;
}

-(void) createCancelledMessage:(UIView*) mainContent theme:(BCHTheme*) theme {
    if (self.event.cancelled) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:@"<i>This event has been cancelled.</i>" withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label  makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}
-(void) createTwitterButton:(UIView*) mainContent theme:(BCHTheme*) theme {
    UIView* view = [UIView new];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"Button_twitter"] forState:UIControlStateNormal];
    [button bk_addEventHandler:^(id sender) {
        [self tweetEvent];
    } forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];

    [button makeConstraints:^(MASConstraintMaker* make) {
        make.top.greaterThanOrEqualTo(view.top);
        make.bottom.lessThanOrEqualTo(view.bottom);
        make.height.equalTo(@45);
        make.width.equalTo(@45);
        make.right.equalTo(view.right);
    }];
    
    UILabel* label = [UILabel new];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    label.text = self.event.hashTag;
    label.textAlignment = NSTextAlignmentRight;
    label.textColor = theme.greyTextColor;
    
    [view addSubview:label];
    [label  makeConstraints:^(MASConstraintMaker* make) {
        make.top.greaterThanOrEqualTo(view.top);
        make.centerY.equalTo(button.centerY);
        make.right.equalTo(button.left).with.offset(-PADDING);
        make.left.equalTo(view.left);
        make.bottom.lessThanOrEqualTo(view.bottom).with.offset(-PADDING);
    }];
    
    UIView* lastSubView = [mainContent.subviews lastObject];

    [mainContent addSubview:view];
    [view makeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo(mainContent.left).with.offset(PADDING);
        make.right.equalTo(mainContent.right).with.offset(-PADDING);
        make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
    }];
}

-(void) tweetEvent {
    NSString* hashtag = self.event.hashTag.length == 0 ? @"" : [self.event.hashTag stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSURL* twitterUrl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://post?message=%@", hashtag]];
    if ([[UIApplication sharedApplication] canOpenURL:twitterUrl]) {
        [[UIApplication sharedApplication] openURL:twitterUrl options:@{} completionHandler:nil];
    } else {
        NSURL* twitterUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://twitter.com/intent/tweet?text=%@",hashtag]];
        [[UIApplication sharedApplication] openURL:twitterUrl options:@{} completionHandler:nil];
    }
    
}

-(void) createHeading:(UIView*) mainContent {
    UIView* view = [UIView new];
    self.avatar = [UIImageView new];
    
    self.avatar.image = self.event.icon;
    [view addSubview:self.avatar];

    [self.avatar  makeConstraints:^(MASConstraintMaker* make) {
            make.top.greaterThanOrEqualTo(view.top);
            make.bottom.lessThanOrEqualTo(view.bottom).with.offset(-PADDING);
            make.left.equalTo(view.left);
            make.height.equalTo(@40);
            make.width.equalTo(@40);
        }];
    UILabel* label = [UILabel new];
    label.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    label.attributedText = [self.event attributedTitle:label.font.pointSize];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    
    [view addSubview:label];
    [label  makeConstraints:^(MASConstraintMaker* make) {
        make.top.greaterThanOrEqualTo(view.top);
        make.centerY.equalTo(self.avatar.centerY);
        make.left.equalTo(self.avatar.right).with.offset(PADDING);
        make.right.equalTo(view.right);
        make.bottom.lessThanOrEqualTo(view.bottom).with.offset(-PADDING);
    }];
    
    [mainContent addSubview:view];
    [view makeConstraints:^(MASConstraintMaker* make) {
        make.left.equalTo(mainContent.left).with.offset(PADDING);
        make.right.equalTo(mainContent.right).with.offset(-PADDING);
        make.top.equalTo(mainContent.top).with.offset(PADDING);
    }];
}

-(void) createMapImage:(UIView*) mainContent theme:(BCHTheme*) theme {
    if (self.event.location.thumbnail != nil) {
        UIImageView* map = [UIImageView new];
        map.contentMode = UIViewContentModeScaleAspectFit;
        map.image = [UIImage imageNamed:[self.event.location.thumbnail stringByDeletingPathExtension]];

        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:map];
        [map makeConstraints:^(MASConstraintMaker* make) {
            make.left.greaterThanOrEqualTo(mainContent.centerX);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
            make.width.equalTo(map.height).multipliedBy( map.image.size.width / map.image.size.height );
        }];
    }
}


-(void) createSubtitle:(UIView*) mainContent theme:(BCHTheme*) theme {
    if (self.event.subTitle.length > 0) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        NSString* html = self.event.subTitle;
        if (self.event.trackCategory.length > 0) {
            html = [NSString stringWithFormat:@"%@<br>%@", self.event.trackCategory, self.event.subTitle];
        }
        label.attributedText = [NSAttributedString fromHtml:html withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.textAlignment = NSTextAlignmentRight;
        label.textColor = theme.greyTextColor;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}

-(void) createMyEventBlurb:(UIView*) mainContent theme:(BCHTheme*) theme {
    if (self.event.isMyEvent) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:@"<i>You are a participant on this event.</i>" withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.textColor = theme.greyTextColor;

        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}


-(void) createTextBlurb:(UIView*) mainContent {
    if (self.event.eventDescription.length > 0) {
        UILabel* label = [UILabel new];
        UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
        label.attributedText = [NSAttributedString fromHtml:self.event.eventDescription withFontSize:font.pointSize];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent addSubview:label];
        [label  makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom).with.offset(PADDING);
        }];
    }
}

-(UIView*) createEventListContentArea:(BCHTheme*) theme {
    
    if (self.event.subEvents.count > 0) {
        UIView* topContent = [self.contentView.subviews lastObject];
        UIView* mainContent = [UIView new];
        mainContent.backgroundColor = theme.detailScreenBox;
        [self.contentView addSubview:mainContent];
        
        for (UnslackedEvent* event in self.event.subEvents) {
            [self createEventButton:event parent:mainContent];
        }
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.contentView.left).with.offset(MARGIN);
            make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
            make.top.equalTo(topContent.bottom).with.offset(MARGIN);
            
            make.bottom.equalTo(lastSubView.bottom);
        }];
        return mainContent;
    } else {
        return nil;
    }
}

-(UIView*) createParticipantListContentArea:(BCHTheme*) theme {
    
    if (self.event.participation.count > 0) {
        UIView* topContent = [self.contentView.subviews lastObject];
        UIView* mainContent = [UIView new];
        mainContent.backgroundColor = theme.detailScreenBox;
        [self.contentView addSubview:mainContent];
        
        for (BCHParticipation* participation in self.event.participation) {
            [self createParticipantButton:participation parent:mainContent theme:theme];
        }
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.contentView.left).with.offset(MARGIN);
            make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
            make.top.equalTo(topContent.bottom).with.offset(MARGIN);
            
            make.bottom.equalTo(lastSubView.bottom);
        }];
        return mainContent;
    } else {
        return nil;
    }
}


-(UIView*) createLinkListContentArea:(BCHTheme*) theme {
    
    if (self.event.links.count > 0) {
        UIView* topContent = [self.contentView.subviews lastObject];
        UIView* mainContent = [UIView new];
        mainContent.backgroundColor = theme.detailScreenBox;
        [self.contentView addSubview:mainContent];
        
        for (BCHLink* link in self.event.links) {
            [self createLinkButton:link parent:mainContent];
        }
        
        UIView* lastSubView = [mainContent.subviews lastObject];
        
        [mainContent makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.contentView.left).with.offset(MARGIN);
            make.right.equalTo(self.contentView.right).with.offset(-MARGIN);
            make.top.equalTo(topContent.bottom).with.offset(MARGIN);
            
            make.bottom.equalTo(lastSubView.bottom);
        }];
        return mainContent;
    } else {
        return nil;
    }
}

-(void) createEventButton:(UnslackedEvent*) event parent:(UIView*) mainContent {
//    BaseAppDelegate* delegate = [BaseAppDelegate instance];
//    BCHTheme* theme = delegate.theme;
    
    BCHOpenItemButton* button = [BCHOpenItemButton new];
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    button.textLabel.attributedText = [NSAttributedString fromHtml:event.title withFontSize:font.pointSize];
    button.imageView.image = event.icon;
    
    [button bk_addEventHandler:^(id sender) {
        BCHEventDetailViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        controller.event = event;
        [self.navigationController pushViewController:controller animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    if (mainContent.subviews.count == 0) {
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(mainContent.top);
        }];
    } else {
        UIView* lastSubView = [mainContent.subviews lastObject];
        BCHDivider* divider = [BCHDivider new];
        [mainContent addSubview:divider];
        [divider makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom);
            make.height.equalTo(@1);
        }];
        
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(divider.bottom);
        }];
    }
}

-(void) createParticipantButton:(BCHParticipation*) participation parent:(UIView*) mainContent theme:(BCHTheme*) theme {
    
    BCHOpenItemButton* button = [BCHOpenItemButton new];
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    NSString* html = participation.participant.fullNameAsHtml;
    if (participation.isModerator) {
        html = [NSString stringWithFormat:@"<b>M:</b> %@", html];
    }
    if (participation.cancelled || participation.participant.cancelled) {
        button.textLabel.attributedText = [NSAttributedString fromHtml:[NSString stringWithFormat:@"<s>%@</s>", html] fontSize:font.pointSize styleProvider:theme];
    } else {
        button.textLabel.attributedText = [NSAttributedString fromHtml:html withFontSize:font.pointSize];
    }
    if (participation.participant.pronouns) {
        button.secondaryText = participation.participant.pronouns;
    }
    button.imageView.layer.cornerRadius = 20;
    button.imageView.layer.masksToBounds = YES;

    if (participation.participant.hasAvatar) {
        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        [button.imageView setImageWithURL:[delegate urlForAvatarId:participation.participant.avatarId] placeholderImage:participation.participant.defaultAvatar];
    } else {
        button.imageView.image = participation.participant.defaultAvatar;
    }
    
    [button bk_addEventHandler:^(id sender) {
        BCHBioDetailsViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"bioDetails"];
        controller.participant = participation.participant;
        [self.navigationController pushViewController:controller animated:YES];
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    if (mainContent.subviews.count == 0) {
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(mainContent.top);
        }];
    } else {
        UIView* lastSubView = [mainContent.subviews lastObject];
        BCHDivider* divider = [BCHDivider new];
        [mainContent addSubview:divider];
        [divider makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom);
            make.height.equalTo(@1);
        }];
        
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(divider.bottom);
        }];
    }
}

-(void) createLinkButton:(BCHLink*) link parent:(UIView*) mainContent {
    //    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    //    BCHTheme* theme = delegate.theme;
    
    BCHOpenItemButton* button = [BCHOpenItemButton new];
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    button.textLabel.attributedText = [NSAttributedString fromHtml:link.name withFontSize:font.pointSize];
    button.imageView.image = link.icon;
    
    [button bk_addEventHandler:^(id sender) {
        if ([link.href rangeOfString:@"wissched:conInfo/"].location == 0) {
            BaseAppDelegate* delegate = [BaseAppDelegate instance];
            id<ContentItem> item = [delegate.contentCollection contentForKey:[link.href lastPathComponent]];
            UIViewController* controller = [[BCHConInfoControllerHelper new] controllerFor:item];
            [self.navigationController pushViewController:controller animated:YES];
        } else {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:link.href] options:@{} completionHandler:nil];
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    if (mainContent.subviews.count == 0) {
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(mainContent.top);
        }];
    } else {
        UIView* lastSubView = [mainContent.subviews lastObject];
        BCHDivider* divider = [BCHDivider new];
        [mainContent addSubview:divider];
        [divider makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(lastSubView.bottom);
            make.height.equalTo(@1);
        }];
        
        [mainContent addSubview:button];
        [button makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(mainContent.left).with.offset(PADDING);
            make.right.equalTo(mainContent.right).with.offset(-PADDING);
            make.top.equalTo(divider.bottom);
        }];
    }
}

#pragma mark Event Actions

-(BCHEventService*) eventService {
    return [BaseAppDelegate instance].eventService;
}

- (IBAction) addEventToMySchedule {
    BOOL state = self.event.addedToSchedule;
    [[self eventService] changeScheduledStatusOfEvent:self.event added:!self.event.selection.addedToSchedule];
    [self.view makeToast:(state ? @"Event removed from schedule" :  @"Event added to schedule")];
}
- (IBAction) highlightEvent {
    [[self eventService] changeHighlightStatusOfEvent:self.event status:!self.event.selection.highlight];
}
- (IBAction) markAsYay {
    [[self eventService] changeYayStatusOfEvent:self.event status:!self.event.selection.yay];
}
@end
