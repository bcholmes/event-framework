//
//  BCHEventDetailViewController.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-04.
//

#import <Foundation/Foundation.h>

#import "UnslackedEvent.h"

@interface BCHEventDetailViewController : UIViewController

@property (nonatomic, strong) UnslackedEvent* event;

@end
