//
//  BCHGridCell.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-04-26.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHGridCell : NSObject

@property (nonatomic) NSUInteger column;
@property (nonatomic) NSUInteger row;
@property (nonatomic) NSUInteger width;
@property (nonatomic) NSUInteger height;
@property (nonatomic) NSUInteger pageNumber;
@property (nonatomic, strong) NSString* label;
@property (nonatomic, strong) NSMutableArray* events;

@end
