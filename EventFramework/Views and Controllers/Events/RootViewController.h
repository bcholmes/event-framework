//
//  RootViewController.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-01.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BaseAppDelegate.h"
#import "BCHScheduleEntryTable.h"
#import "BaseEventListController.h"


@interface RootViewController : BaseEventListController

@property (nonatomic, strong) BCHScheduleEntryTable* filteredEventsTable;

@end
