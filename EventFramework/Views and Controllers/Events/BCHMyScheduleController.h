//
//  MyScheduleController.h
//  unslacked-app
//
//  Created by BC Holmes on 12-04-25.
//  Copyright (c) 2012 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseEventListController.h"

@interface BCHMyScheduleController : BaseEventListController

@end
