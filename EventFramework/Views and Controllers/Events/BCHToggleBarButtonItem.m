//
//  BCHToggleBarButtonItem.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-07.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import "BCHToggleBarButtonItem.h"

@implementation BCHToggleBarButtonItem

-(void) setToggled:(BOOL)toggled {
    _toggled = toggled;
    if (toggled) {
        self.image = self.selectedImage;
    } else {
        self.image = self.nonselectedImage;
    }
}
@end
