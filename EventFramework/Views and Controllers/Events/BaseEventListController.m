//
//  BaseEventListController.m
//  unslacked-app
//
//  Created by BC Holmes on 12-01-04.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseEventListController.h"
#import "BaseAppDelegate.h"
#import "BCHEventTableViewCell.h"
#import "BCHEventDetailViewController.h"
#import "BCHVersionHelper.h"
#import "BCHTheme.h"
#import "BCHReminder.h"
#import "BCHReminderViewController.h"
#import "UIColor+Extensions.h"

@implementation BaseEventListController

-(void) viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
}
-(void) reloadAllEvents:(NSNotification*) notification {
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.eventTable = [BCHScheduleEntryTable tableFromEvents:[delegate allLeafEvents]];
    [self.tableView reloadData];
}

-(void)eventDidChange:(NSNotification*) notification {
    NSIndexPath* path = [[self currentEventTable] indexPathFor:notification.object accessType:self.accessType];
    if (path != nil) {
        [self.tableView beginUpdates];
        NSArray *array = [NSArray arrayWithObject:path];
        [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}


- (BCHScheduleEntryTable*) currentEventTable {
    return self.eventTable;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section  {
    if ([self tableView:tableView numberOfRowsInSection:section] > 0) {
        UnslackedTimeRange* key = [[[self currentEventTable] sectionKeys] objectAtIndex:section];
        return [key asString];
    } else {
        return nil;
    }
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
*/
// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self currentEventTable].sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray* sectionContents = [[self currentEventTable] sectionForIndex:section accessType:self.accessType];
    return [sectionContents count];
}

-(UIImage*) iconForEvent:(id<BCHScheduleEntry>) event {
    return event.icon;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForEvent:(NSObject<BCHScheduleEntry>*) event
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"BCHEventTableViewCell" bundle:nil] forCellReuseIdentifier:CellIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }

    [self populateCell:cell fromEvent:event];
    return cell;
}

-(void) populateCell:(UITableViewCell*) cell fromEvent:(NSObject<BCHScheduleEntry>*) event {
    [cell.imageView setClipsToBounds:NO];
    cell.imageView.image = [self iconForEvent:event];
    
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    cell.textLabel.attributedText = [NSAttributedString fromHtml:event.fullTitleAsHtml fontSize:cell.textLabel.font.pointSize styleProvider:delegate.theme];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.attributedText = [NSAttributedString fromHtml:event.listSubtitle withFontSize:cell.detailTextLabel.font.pointSize];
    cell.textLabel.highlightedTextColor = [UIColor blackColor];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [[UIView alloc] init];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    UIView *purple = [[UIView alloc] init];
    purple.backgroundColor = delegate.theme.lightColor;
    cell.selectedBackgroundView = purple;
    if (event.highlighted) {
        cell.backgroundColor = [UIColor yellowColor];
    } else if (event.isMyEvent) {
        cell.backgroundColor = delegate.theme.myItemColor;
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if ([cell isKindOfClass:[BCHEventTableViewCell class]]) {
        ((BCHEventTableViewCell*) cell).scheduled = event.addedToSchedule;
    }
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSObject<BCHScheduleEntry>* event = [[self currentEventTable] entryAtIndexPath:indexPath accessType:self.accessType];
    if (event == nil) {
        NSLog(@"event nil");
    }
    
    return [self tableView:tableView cellForEvent:event];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSObject<BCHScheduleEntry>* event = [[self currentEventTable] entryAtIndexPath:indexPath accessType:self.accessType];
    if ([event isKindOfClass:[UnslackedEvent class]]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BCHEventDetailViewController* detailController = [storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        
        detailController.event = (UnslackedEvent*) event;
        [self.navigationController pushViewController:detailController animated:YES];
    } else if ([event isKindOfClass:[BCHReminder class]]) {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BCHReminderViewController* detailController = [storyboard instantiateViewControllerWithIdentifier:@"reminder"];
        
        detailController.reminder = (BCHReminder*) event;
        [self.navigationController pushViewController:detailController animated:YES];
    }
}



@end
