//
//  BaseEventListController.h
//  unslacked-app
//
//  Created by BC Holmes on 12-01-04.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BCHScheduleEntryTable.h"
#import "BCHScheduleEntry.h"

@interface BaseEventListController : UITableViewController {
    
}

@property (nonatomic, strong) BCHScheduleEntryTable* eventTable;
@property (nonatomic, assign) EventTableAccessType accessType;

-(void)eventDidChange:(NSNotification*) notification;
-(void)reloadAllEvents:(NSNotification*) notification;
- (BCHScheduleEntryTable*) currentEventTable;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForEvent:(NSObject<BCHScheduleEntry>*) event;
-(void) populateCell:(UITableViewCell*) cell fromEvent:(NSObject<BCHScheduleEntry>*) event;
@end
