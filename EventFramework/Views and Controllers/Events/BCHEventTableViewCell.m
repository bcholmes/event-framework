//
//  BCHEventTableViewCell.m
//  WisSched / EventFramework
//
//  Created by BC Holmes on 2018-04-21.
//

#import "BCHEventTableViewCell.h"

#import <MaterialComponents/MaterialInk.h>

@interface BCHEventTableViewCell()

@property (nonatomic, strong) IBOutlet UILabel* eventTitle;
@property (nonatomic, strong) IBOutlet UILabel* eventSubTitle;
@property (nonatomic, strong) IBOutlet UIImageView* eventIconImageView;
@property (nonatomic, strong) IBOutlet UIImageView* scheduledIconImageView;

@property(strong, nonatomic, nonnull) MDCInkView *inkView;
@property(nonatomic, assign) CGPoint lastTouch;

@end

@implementation BCHEventTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void) commonInit {
    if (!self.inkView) {
        self.inkView = [[MDCInkView alloc] initWithFrame:self.bounds];
    }
    [self addSubview:_inkView];
}

#pragma mark Ink

- (void)startInk {
    [self.inkView startTouchBeganAtPoint:_lastTouch animated:YES withCompletion:nil];
}

- (void)endInk {
    [self.inkView startTouchEndAtPoint:_lastTouch animated:YES withCompletion:nil];
}


-(void) layoutSubviews {
    [super layoutSubviews];
    self.inkView.inkColor = self.inkColor;
    self.inkView.frame = self.bounds;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL) animated {
    [super setHighlighted:highlighted animated:animated];
    if (highlighted) {
        [self startInk];
    } else {
        [self endInk];
    }
}

- (void)prepareForReuse {
    [super prepareForReuse];
    [self.inkView cancelAllAnimationsAnimated:NO];
}


#pragma mark - UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    self.lastTouch = location;
    // Call super only after -lastTouch has been recorded, since super can call -setHighlighted:.
    [super touchesBegan:touches withEvent:event];
}

-(UILabel*) textLabel {
    return self.eventTitle;
}

-(UILabel*) detailTextLabel {
    return self.eventSubTitle;
}

-(UIImageView*) imageView {
    return self.eventIconImageView;
}

-(void) setScheduled:(BOOL)scheduled {
    _scheduled = scheduled;
    self.scheduledIconImageView.hidden = !scheduled;
}

@end
