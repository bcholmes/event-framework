//
//  BCHToggleBarButtonItem.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-07.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHToggleBarButtonItem : UIBarButtonItem

@property (nonatomic, strong) UIImage* selectedImage;
@property (nonatomic, strong) UIImage* nonselectedImage;
@property (nonatomic) BOOL toggled;

@end
