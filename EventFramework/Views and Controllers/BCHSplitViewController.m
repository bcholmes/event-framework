//
//  BCHSplitViewController.m
//  WisSched
//
//  Created by BC Holmes on 2016-03-13.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHSplitViewController.h"

@interface BCHSplitViewController ()<UISplitViewControllerDelegate>

@end

@implementation BCHSplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)splitViewController:(UISplitViewController *)splitViewController
collapseSecondaryViewController:(UIViewController *)secondaryViewController
  ontoPrimaryViewController:(UIViewController *)primaryViewController {
    
    return YES;
        
}

@end
