//
//  SimpleRenderableDetailsViewController.m
//  EventsSchedule
//
//  Created by BC Holmes on 2013-03-16.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "BCHSimpleRenderableDetailsViewController.h"

#import <MaterialComponents/MaterialAppBar.h>
#import <CoreServices/CoreServices.h>

#import "BaseAppDelegate.h"
#import "BCHEventDetailViewController.h"
#import "BCHRestaurant.h"
#import "BCHAnnouncement.h"
#import "BCHPage.h"
#import "BCHImageService.h"
#import "BCHTheme.h"
#import "BCHContentCollection.h"
#import "BCHVersionHelper.h"
#import "UnslackedEvent.h"

@interface BCHSimpleRenderableDetailsViewController()<MDCFlexibleHeaderViewLayoutDelegate,UIScrollViewDelegate,WKNavigationDelegate>

@property (nonatomic, strong) MDCAppBar* appBar;
@property (nonatomic, strong) UIImageView* headerImage;
@property (nonatomic, assign) CGFloat maxHeaderHeight;

@end

@implementation BCHSimpleRenderableDetailsViewController

-(void) loadDetails {
    if ([self.renderable isKindOfClass:[BCHPage class]]) {
        BCHPage* page = (BCHPage*) self.renderable;
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[page.content stringByDeletingPathExtension] ofType:[page.content pathExtension]] isDirectory:NO]]];
    } else if ([self.renderable isKindOfClass:[BCHAnnouncement class]]) {
        BCHAnnouncement* announcement = (BCHAnnouncement*) self.renderable;
        [self.webView loadHTMLString:[announcement renderAsHtml]  baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (!self.loaded) {
        self.webView.UIDelegate = self;
        self.webView.navigationDelegate = self;
        [self loadDetails];
        self.loaded = true;
    }
}

-(void)eventDidChange:(NSNotification*) notification {
    [self loadDetails];
    [self.view setNeedsDisplay];
}

- (void) backPage {
    if (self.splitViewController != nil) {
        [self.navigationController.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) viewDidLoad {
    [super viewDidLoad];
    [self configureAppBar];
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"765-arrow-left-white"] style:UIBarButtonItemStylePlain target:self action:@selector(backPage)];
    backButton.tintColor = [UIColor clearColor];
    self.navigationItem.leftBarButtonItem = backButton;

    self.navigationItem.hidesBackButton = NO;
//    self.navigationController.navigationBar.translucent = NO;
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    [self.webView setOpaque:NO];
    self.webView.scrollView.delegate = self;
    [BCHVersionHelper setNavigationTextColor:self.navigationController toColor:[UIColor whiteColor]];
}

-(void) dealloc {
    self.webView.scrollView.delegate = nil;
}

-(void) configureAppBar {
    self.appBar = [MDCAppBar new];
    
    self.appBar.navigationBar.backgroundColor = [UIColor clearColor];
    self.appBar.navigationBar.leadingBarItemsTintColor = [UIColor clearColor];
    self.appBar.headerViewController.layoutDelegate = self;
    [self.appBar.navigationBar setTitle:nil];

    [self addChildViewController:self.appBar.headerViewController];
    if ([self.renderable isKindOfClass:[BCHPage class]] && ((BCHPage*) self.renderable).banner != nil) {
        self.headerImage = [UIImageView new];
        self.headerImage.image = [UIImage imageNamed:[self chooseBanner:((BCHPage*) self.renderable).banner]];
        self.headerImage.contentMode = UIViewContentModeScaleAspectFill;
        self.headerImage.clipsToBounds = YES;
        self.headerImage.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    
    CGFloat imageAspectRatio = 72.0f/120.0f;
    CGFloat screenWidth = self.view.bounds.size.width;
    self.appBar.headerViewController.headerView.minimumHeight = 44 + [UIApplication sharedApplication].statusBarFrame.size.height;
    self.appBar.headerViewController.headerView.maximumHeight = self.maxHeaderHeight = MIN(240, screenWidth * imageAspectRatio);
    if (self.headerImage != nil) {
        self.headerImage.frame = self.appBar.headerViewController.headerView.bounds;
        
        [self.appBar.headerViewController.headerView insertSubview:self.headerImage atIndex:0];
    }
    self.appBar.headerViewController.headerView.backgroundColor = [BaseAppDelegate instance].theme.baseColor;
    self.appBar.headerViewController.headerView.trackingScrollView = self.webView.scrollView;
    
    [self.appBar addSubviewsToParent];
}

-(NSString*) chooseBanner:(NSString*) banner {
    return banner.length > 0 ? [NSString stringWithFormat:@"banner-%@", banner] : nil;
}

-(void) scrollViewDidScroll:(UIScrollView*) scrollView {
    [self.appBar.headerViewController.headerView trackingScrollViewDidScroll];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView*) scrollView {
    [self.appBar.headerViewController.headerView trackingScrollViewDidEndDecelerating];
}

-(void) scrollViewDidEndDragging:(UIScrollView*) scrollView willDecelerate:(BOOL) decelerate {
    [self.appBar.headerViewController.headerView trackingScrollViewDidEndDraggingWillDecelerate:decelerate];
}

-(void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    [self.appBar.headerViewController.headerView trackingScrollViewWillEndDraggingWithVelocity:velocity targetContentOffset:targetContentOffset];
}


- (void) webView:(WKWebView*) webView decidePolicyForNavigationAction:(WKNavigationAction*) navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString* url = navigationAction.request.URL.absoluteString;
    
    if ([url rangeOfString:@"/device/event/"].location != NSNotFound) {
        
        UnslackedEvent* event = [delegate eventById:[url lastPathComponent]];
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BCHEventDetailViewController* detailController = [storyboard instantiateViewControllerWithIdentifier:@"eventDetails"];
        detailController.event = event;
        [self.navigationController pushViewController:detailController animated:YES];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        
    } else if ([url rangeOfString:@"/device/info/"].location != NSNotFound) {

        BCHContentCollection* collection = delegate.contentCollection;
        BCHPage* page = [collection contentPageForLink:[url lastPathComponent]];
        if (page != nil) {
        
            BCHSimpleRenderableDetailsViewController* controller = [[BCHSimpleRenderableDetailsViewController alloc] initWithNibName:@"BCHSimpleRenderableDetailsView" bundle:nil];
            controller.renderable = page;
            [self.navigationController pushViewController:controller animated:YES];
            decisionHandler(WKNavigationActionPolicyCancel);
        } else {
            decisionHandler(WKNavigationActionPolicyAllow);
        }
    } else if ([[[navigationAction.request URL] scheme] isEqual:@"mailto"]) {
        [[UIApplication sharedApplication] openURL:[navigationAction.request URL] options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([[[navigationAction.request URL] scheme] isEqual:@"tel"]) {
        [[UIApplication sharedApplication] openURL:[navigationAction.request URL] options:@{} completionHandler:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
    } else if ([[url lowercaseString] rangeOfString:@"http"].location == 0) {
        [[UIApplication sharedApplication] openURL:navigationAction.request.URL options:@{} completionHandler:nil];
        
        decisionHandler(WKNavigationActionPolicyCancel);
    } else {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

-(BOOL) isAddImageAllowed {
    if ([self.renderable isKindOfClass:[BCHParticipant class]]) {
        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        BCHParticipant* participant = (BCHParticipant*) self.renderable;
        return !participant.isAnonymous && [participant.key isEqualToString:delegate.personId];
    } else {
        return false;
    }
}

- (BOOL) isImageAvailable {
    if ([self.renderable isKindOfClass:[BCHParticipant class]]) {
        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        BCHImageService* imageService = delegate.imageService;
        return [imageService isAvatarAvailable:(BCHParticipant*) self.renderable];
    } else {
        return false;
    }
}
- (NSString*) imageSrc {
    if (![self isImageAvailable]) {
        return nil;
    } else if ([self.renderable isKindOfClass:[BCHParticipant class]]) {
        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        BCHImageService* imageService = delegate.imageService;
        return [imageService getAvatarAsEncodedHtmlImage:(BCHParticipant*) self.renderable];
    } else {
        return nil;
    }
}

#pragma mark - UIImagePickerControllerDelegate

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width {
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage* chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    BCHImageService* imageService = delegate.imageService;
    
    if (chosenImage.size.width > 200 ) {
        chosenImage = [self imageWithImage:chosenImage scaledToWidth:200];
    }
    
    NSLog(@"image size is %f %f", chosenImage.size.height, chosenImage.size.width);
    
    BCHParticipant* participant = (BCHParticipant*) self.renderable;
    [imageService writeImageToFileSystem:chosenImage participant:participant];
    
    [imageService uploadAvatar:chosenImage];
    [self loadDetails];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark = UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController
                    animated:(BOOL)animated {
    
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    navigationController.navigationBar.barTintColor = delegate.theme.baseColor;
    navigationController.navigationBar.translucent = NO;
}

#pragma mark MDCFlexibleHeaderViewLayoutDelegate

- (void)flexibleHeaderViewController:(nonnull MDCFlexibleHeaderViewController *)flexibleHeaderViewController flexibleHeaderViewFrameDidChange:(nonnull MDCFlexibleHeaderView *)flexibleHeaderView {
    
}
@end
