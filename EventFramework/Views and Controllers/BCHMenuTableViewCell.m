//
//  BCHMenuTableViewCell.m
//  EventFramework
//
//  Created by BC Holmes on 2017-06-08.
//  Copyright © 2017 Ayizan Studios. All rights reserved.
//

#import "BCHMenuTableViewCell.h"

@interface BCHMenuTableViewCell()

@property (nonatomic, weak, nullable) IBOutlet UIImageView* iconView;
@property (nonatomic, weak, nullable) IBOutlet UILabel* menuItemLabel;

@end

@implementation BCHMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(UIImageView*) imageView {
    return self.iconView;
}

-(UILabel*) textLabel {
    return self.menuItemLabel;
}

@end
