//
//  BCHConInfoControllerHelper.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import "BCHConInfoControllerHelper.h"
#import "BCHSimpleList.h"
#import "BCHPage.h"
#import "BCHEventFilter.h"
#import "BCHSimpleListTableViewController.h"
#import "BCHFilteredEventListViewController.h"
#import "BCHSimpleRenderableDetailsViewController.h"

@implementation BCHConInfoControllerHelper

-(UIViewController*) undecoratedControllerFor:(id<ContentItem>) item {
    if ([(NSObject*) item isKindOfClass:[BCHSimpleList class]]) {
        BCHSimpleListTableViewController* controller = [[BCHSimpleListTableViewController alloc] init];
        controller.simpleList = (BCHSimpleList*) item;
        return controller;
    } else if ([(NSObject*) item isKindOfClass:[BCHEventFilter class]]) {
        BCHEventFilter* filter = (BCHEventFilter*) item;
        BCHFilteredEventListViewController* controller = [[BCHFilteredEventListViewController alloc] init];
        controller.eventFilter = filter;
        return controller;
    } else {
        BCHPage* page = (BCHPage*) item;
        BCHSimpleRenderableDetailsViewController* detailController = [[BCHSimpleRenderableDetailsViewController alloc] init];
        detailController.renderable = page;
        return detailController;
    }
}

-(UIViewController*) controllerFor:(id<ContentItem>) item {
    if ([(NSObject*) item isKindOfClass:[BCHSimpleList class]]) {
        BCHSimpleListTableViewController* controller = [[BCHSimpleListTableViewController alloc] init];
        controller.simpleList = (BCHSimpleList*) item;
        return controller;
    } else if ([(NSObject*) item isKindOfClass:[BCHEventFilter class]]) {
        BCHEventFilter* filter = (BCHEventFilter*) item;
        BCHFilteredEventListViewController* controller = [[BCHFilteredEventListViewController alloc] init];
        controller.eventFilter = filter;
        UIViewController* navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        return navigationController;
    } else {
        BCHPage* page = (BCHPage*) item;
        BCHSimpleRenderableDetailsViewController* detailController = [[BCHSimpleRenderableDetailsViewController alloc] init];
        detailController.renderable = page;
        UIViewController* navigationController = [[UINavigationController alloc] initWithRootViewController:detailController];

        return navigationController;
    }
}

@end
