//
//  BCHSimpleListTableViewController.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import "BCHSimpleListTableViewController.h"

#import "BaseAppDelegate.h"
#import "UIViewController+Style.h"

@interface BCHSimpleListTableViewController ()

@property (nonatomic, strong) NSArray* items;

@end

@implementation BCHSimpleListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.allowsSelection = NO;
    self.navigationItem.title = self.simpleList.title;
    self.items = self.simpleList.items;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* ListItem = @"listItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ListItem];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ListItem];
    }

    BCHSimpleListItem* item = self.items[indexPath.row];
    cell.textLabel.text = item.title;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    if (item.subTitle != nil) {
        cell.detailTextLabel.text = item.subTitle;
    } else {
        cell.detailTextLabel.text = nil;
    }
    
    if (item.href != nil) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BCHSimpleListItem* item = self.items[indexPath.row];
    if (item.href != nil) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.href] options:@{} completionHandler:nil];
    }
}
@end
