//
//  BCHSimpleListTableViewController.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import <UIKit/UIKit.h>

#import "BCHSimpleList.h"

@interface BCHSimpleListTableViewController : UITableViewController

@property (nonatomic, strong) BCHSimpleList* simpleList;

@end
