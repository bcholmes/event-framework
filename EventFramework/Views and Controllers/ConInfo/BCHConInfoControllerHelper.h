//
//  BCHConInfoControllerHelper.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2018-05-06.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "ContentItem.h"

@interface BCHConInfoControllerHelper : NSObject

-(UIViewController*) controllerFor:(id<ContentItem>) contentItem;
-(UIViewController*) undecoratedControllerFor:(id<ContentItem>) item;

@end
