//
//  InfoListView.h
//  unslacked-app
//
//  Created by BC Holmes on 11-09-03.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoListView : UITableViewController {
    NSArray *_pages;
}

@property (nonatomic, strong) NSArray* pages;
@property (nonatomic, strong) NSString* subMenuTitle;

@end
