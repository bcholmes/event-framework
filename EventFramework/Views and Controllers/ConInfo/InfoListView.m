//
//  InfoListView.m
//  unslacked-app
//
//  Created by BC Holmes on 11-09-03.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import "InfoListView.h"

#import <pop/POP.h>

#import "BCHPage.h"
#import "BCHSimpleRenderableDetailsViewController.h"
#import "BaseAppDelegate.h"
#import "SubMenu.h"
#import "UIViewController+Style.h"
#import "BCHTheme.h"
#import "BCHConInfoControllerHelper.h"

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)


@interface InfoListView()

@property (nonatomic, retain) NSMutableArray* openFlags;

@end


@implementation InfoListView

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;

    BaseAppDelegate* delegate = (BaseAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (self.subMenuTitle != nil) {
        self.navigationItem.title = self.subMenuTitle;
    } else {
        [self styleNavigationBar:@"Con Info"];
        [self initializeMenuButton];
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];

    if (self.pages == nil) {
        self.pages = delegate.contentTopLevel;
    }
    self.openFlags = [self createOpenFlags:self.pages.count];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


-(NSMutableArray*) createOpenFlags:(NSInteger) count {
    NSMutableArray* result = [[NSMutableArray alloc] init];
    for (int i = 0; i < count; i++) {
        [result addObject:[NSNumber numberWithBool:NO]];
    }
    return result;
}
- (BOOL) isSectionOpen:(NSInteger) section {
    return self.openFlags.count > section && [[self.openFlags objectAtIndex:section] boolValue];
}

-(void) transitionToDetailsController:(BCHPage*) page {
    BCHSimpleRenderableDetailsViewController* detailController = [[BCHSimpleRenderableDetailsViewController alloc] init];
    detailController.renderable = page;
    UIViewController* navigationController = [[UINavigationController alloc] initWithRootViewController:detailController];
    
    [self.splitViewController showDetailViewController:navigationController sender:self];
}

-(void) toggleOpenIndicator:(NSIndexPath*) indexPath {
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if (cell != nil) {
        BOOL expanded = [[self.openFlags objectAtIndex:indexPath.section] boolValue];
        
        if (expanded) {
            POPSpringAnimation* spin = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
            
            spin.fromValue = @(0);
            spin.toValue = @(M_PI / 2);
            spin.springBounciness = 15;
            spin.velocity = @(10);
            [cell.imageView.layer pop_addAnimation:spin forKey:@"openAnimation"];
        } else {
            POPSpringAnimation* spin = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerRotation];
            
            spin.fromValue = @(M_PI / 2);
            spin.toValue = @(0);
            spin.springBounciness = 15;
            spin.velocity = @(10);
            [cell.imageView.layer pop_addAnimation:spin forKey:@"closeAnimation"];
            
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.pages count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self isSectionOpen:section]) {
        SubMenu* menu = (SubMenu*) [self.pages objectAtIndex:section];
        return menu.items.count + 1;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    BaseAppDelegate* delegate = [BaseAppDelegate instance];
    UIView *purple = [[UIView alloc] init];
    purple.backgroundColor = delegate.theme.lightColor;
    cell.selectedBackgroundView = purple;
    
    id<ContentItem> page = [self.pages objectAtIndex:indexPath.section];
    if (page.isMenu && indexPath.row == 0) {
        cell.textLabel.text = page.title;
        cell.imageView.layer.transform = CATransform3DIdentity;
        if ([self isSectionOpen:indexPath.section]) {
            cell.imageView.layer.transform = CATransform3DMakeRotation(DEGREES_TO_RADIANS( 90 ), 0, 0, 1.0);
        }
        cell.imageView.image = [UIImage imageNamed:@"caretright.png"];
//        cell.imageView.image = [self isSectionOpen:indexPath.section] ? [UIImage imageNamed:@"caretdown.png"] : [UIImage imageNamed:@"caretright.png"];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    } else if (page.isMenu) {
        SubMenu* menu = (SubMenu*) page;
        id<ContentItem> subItem = [menu.items objectAtIndex:(indexPath.row-1)];
        cell.textLabel.text = subItem.title;
        cell.imageView.image = nil;
        cell.backgroundColor = delegate.theme.subListColor;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    } else {
        cell.textLabel.text = page.title;
        cell.imageView.image = nil;
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSUInteger row = indexPath.section;
    id<ContentItem> item = [self.pages objectAtIndex:row];
    
    if (item.isMenu && indexPath.row == 0) {
        BOOL expanded = [[self.openFlags objectAtIndex:indexPath.section] boolValue];
        [self.openFlags replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:!expanded]];
        [self.tableView beginUpdates];
        SubMenu* subMenu = (SubMenu*) item;
        NSMutableArray* paths = [[NSMutableArray alloc] init];
        for (int i = 1; i <= subMenu.items.count; i++) {
            [paths addObject:[NSIndexPath indexPathForRow:i inSection:indexPath.section]];
        }
        if (expanded) {
            [tableView deleteRowsAtIndexPaths:paths
                             withRowAnimation:UITableViewRowAnimationTop];
        } else {
            [tableView insertRowsAtIndexPaths:paths
                             withRowAnimation:UITableViewRowAnimationTop];
        }
        [self.tableView endUpdates];
        
        [self toggleOpenIndicator:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
    } else if (item.isMenu) {
        SubMenu* subMenu = (SubMenu*) item;
        [self transitionToDetailsController:[subMenu.items objectAtIndex:(indexPath.row-1)]];
    } else {
        UIViewController* controller = [[BCHConInfoControllerHelper new] controllerFor:item];
        [self.splitViewController showDetailViewController:controller sender:self];
    }
}



@end
