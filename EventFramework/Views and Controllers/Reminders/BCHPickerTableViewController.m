//
//  BCHPickerTableViewController.m
//  EventFramework
//
//  Created by BC Holmes on 2019-05-18.
//

#import "BCHPickerTableViewController.h"

@interface BCHPickerTableViewController ()

@end

@implementation BCHPickerTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.records.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pickerCell" forIndexPath:indexPath];
    cell.textLabel.text = self.records[indexPath.row];    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* option = self.records[indexPath.row];
    if (self.onSelection != nil) {
        self.onSelection(option);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
