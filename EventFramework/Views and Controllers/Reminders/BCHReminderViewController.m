//
//  BCHReminderViewController.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-05-20.
//

#import "BCHReminderViewController.h"

@interface BCHReminderViewController ()

@property (nonatomic, weak) IBOutlet UILabel* titleLabel;
@property (nonatomic, weak) IBOutlet UILabel* subtitleLabel;

@end

@implementation BCHReminderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = self.reminder.title;
    self.subtitleLabel.text = self.reminder.location;
}

@end
