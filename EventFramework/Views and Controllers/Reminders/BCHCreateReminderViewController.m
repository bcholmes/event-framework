//
//  BCHCreateReminderViewController.m
//  EventFramework
//
//  Created by BC Holmes on 2019-05-18.
//

#import "BCHCreateReminderViewController.h"

#import <MaterialComponents/MaterialTextFields.h>
#import <MaterialComponents/MaterialBottomSheet.h>
#import <NSDate-Additions/NSDate+Additions.h>
#import <UITintedButton/UIButton+tintImage.h>

#import "BaseAppDelegate.h"

#import "BCHReminder.h"
#import "BCHLocation.h"
#import "BCHGridViewService.h"
#import "BCHPickerTableViewController.h"
#import "BCHTimePickerViewController.h"

#import "NSDate+Utils.h"

#define FIFTEEN_MINUTES (15 * 60)
#define ONE_HOUR (60 * 60)

@interface BCHReadOnlyTextDelegate : NSObject<UITextFieldDelegate>

@end

@interface BCHControllerTextDelegate : NSObject<UITextFieldDelegate>

@property (nonatomic, strong) MDCTextInputControllerBase* controller;

@end

@implementation BCHReadOnlyTextDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;
}
- (BOOL)textField:(UITextField*) textField shouldChangeCharactersInRange:(NSRange) range replacementString:(NSString *) string {
    return NO;
}
@end

@implementation BCHControllerTextDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.controller setErrorText:nil errorAccessibilityValue:nil];
    return YES;
}

- (BOOL)textField:(UITextField*) textField shouldChangeCharactersInRange:(NSRange) range replacementString:(NSString *) string {
    [self.controller setErrorText:nil errorAccessibilityValue:nil];
    return YES;
}

@end

@interface BCHCreateReminderViewController ()<MDCBottomSheetControllerDelegate>

@property (nonatomic, weak) IBOutlet MDCTextField* descriptionTextField;
@property (nonatomic, weak) IBOutlet MDCTextField* locationTextField;
@property (nonatomic, weak) IBOutlet MDCTextField* dayTextField;
@property (nonatomic, weak) IBOutlet MDCTextField* startTimeTextField;
@property (nonatomic, weak) IBOutlet MDCTextField* endTimeTextField;
@property (nonatomic, weak) IBOutlet UIScrollView* scrollView;

@property (nonatomic, strong) NSArray* days;
@property (nonatomic, strong) MDCTextInputControllerUnderline* descriptionController;
@property (nonatomic, strong) MDCTextInputControllerUnderline* locationController;
@property (nonatomic, strong) MDCTextInputControllerUnderline* dayController;
@property (nonatomic, strong) MDCTextInputControllerUnderline* startTimeController;
@property (nonatomic, strong) MDCTextInputControllerUnderline* endTimeController;

@property (nonatomic, strong) BCHReadOnlyTextDelegate* readOnlyTextDelegate;
@property (nonatomic, strong) BCHControllerTextDelegate* locationDelegate;
@property (nonatomic, strong) BCHControllerTextDelegate* descriptionDelegate;

@property (nonatomic, strong) BCHGridViewService* gridViewService;

@property (nonatomic, strong) NSDateFormatter* timeFormatter;

@property (nonatomic, strong) NSDate* startDate;
@property (nonatomic, strong) NSDate* endDate;

@property (nonatomic, strong) NSString* field;

@end

@implementation BCHCreateReminderViewController

- (UIButton *)createDropDownButton {
    UIButton* dropDown = [[UIButton alloc] initWithFrame:CGRectZero];
    [dropDown setImage:[UIImage imageNamed:@"764-arrow-down"] forState:UIControlStateNormal];
    [dropDown setImageTintColor:[BaseAppDelegate instance].theme.baseColor forState:UIControlStateNormal];
    dropDown.translatesAutoresizingMaskIntoConstraints = NO;
    [dropDown setContentCompressionResistancePriority:UILayoutPriorityDefaultLow - 1
                                              forAxis:UILayoutConstraintAxisHorizontal];
    [dropDown setContentCompressionResistancePriority:UILayoutPriorityDefaultLow - 1
                                              forAxis:UILayoutConstraintAxisVertical];
    [dropDown setContentHuggingPriority:UILayoutPriorityDefaultLow + 1
                                forAxis:UILayoutConstraintAxisHorizontal];
    [dropDown setContentHuggingPriority:UILayoutPriorityDefaultLow + 1
                                forAxis:UILayoutConstraintAxisVertical];
    dropDown.opaque = NO;
    return dropDown;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    BaseAppDelegate* delegate = [BaseAppDelegate instance];

    UIToolbar* doneToolbar = [[UIToolbar alloc] init];
    [doneToolbar sizeToFit];
    UIBarButtonItem* done = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonFromKeyboardClicked:)];
    UIBarButtonItem* spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [doneToolbar setItems:[NSArray arrayWithObjects:spacer, done, nil]];
    
    self.descriptionTextField.inputAccessoryView = doneToolbar;
    self.locationTextField.inputAccessoryView = doneToolbar;
    self.dayTextField.inputAccessoryView = doneToolbar;
    self.startTimeTextField.inputAccessoryView = doneToolbar;
    self.endTimeTextField.inputAccessoryView = doneToolbar;
    
    self.readOnlyTextDelegate = [BCHReadOnlyTextDelegate new];
    self.dayTextField.delegate = self.readOnlyTextDelegate;
    self.startTimeTextField.delegate = self.readOnlyTextDelegate;
    self.endTimeTextField.delegate = self.readOnlyTextDelegate;
   
    self.locationTextField.trailingViewMode = UITextFieldViewModeAlways;
    self.locationTextField.trailingView = [self createDropDownButton];
    [((UIButton*) self.locationTextField.trailingView) addTarget:self action:@selector(chooseLocation:) forControlEvents:UIControlEventTouchUpInside];

    self.dayTextField.trailingViewMode = UITextFieldViewModeAlways;
    self.dayTextField.trailingView = [self createDropDownButton];
    [((UIButton*) self.dayTextField.trailingView) addTarget:self action:@selector(chooseDay:) forControlEvents:UIControlEventTouchUpInside];

    self.startTimeTextField.trailingViewMode = UITextFieldViewModeAlways;
    self.startTimeTextField.trailingView = [self createDropDownButton];
    [((UIButton*) self.startTimeTextField.trailingView) addTarget:self action:@selector(chooseStartTime:) forControlEvents:UIControlEventTouchUpInside];
    [self.startTimeTextField addTarget:delegate action:@selector(chooseStartTime:) forControlEvents:UIControlEventTouchUpInside];
    
    self.endTimeTextField.trailingViewMode = UITextFieldViewModeAlways;
    self.endTimeTextField.trailingView = [self createDropDownButton];
    [((UIButton*) self.endTimeTextField.trailingView) addTarget:self action:@selector(chooseEndTime:) forControlEvents:UIControlEventTouchUpInside];
    
    self.gridViewService = [[BCHGridViewService alloc] initWithEventService:delegate.eventService];
    self.days = self.gridViewService.allDays;
    
    self.descriptionController = [[MDCTextInputControllerUnderline alloc] initWithTextInput:self.descriptionTextField];
    [delegate.theme applyTheme:self.descriptionController];

    self.locationController = [[MDCTextInputControllerUnderline alloc] initWithTextInput:self.locationTextField];
    [delegate.theme applyTheme:self.locationController];

    self.dayController = [[MDCTextInputControllerUnderline alloc] initWithTextInput:self.dayTextField];
    [delegate.theme applyTheme:self.dayController];

    self.startTimeController = [[MDCTextInputControllerUnderline alloc] initWithTextInput:self.startTimeTextField];
    [delegate.theme applyTheme:self.startTimeController];

    self.endTimeController = [[MDCTextInputControllerUnderline alloc] initWithTextInput:self.endTimeTextField];
    [delegate.theme applyTheme:self.endTimeController];
    
    self.descriptionDelegate = [BCHControllerTextDelegate new];
    self.descriptionDelegate.controller = self.descriptionController;
    self.descriptionTextField.delegate = self.descriptionDelegate;

    self.locationDelegate = [BCHControllerTextDelegate new];
    self.locationDelegate.controller = self.locationController;
    self.locationTextField.delegate = self.locationDelegate;
    
    NSString* day = [[NSDate date] dayString:delegate.timeZone];
    if (![self.days containsObject:day]) {
        day = self.days[0];
    }
    
    self.dayTextField.text = day;
    
    NSDate* nowish = [NSDate new];
    NSTimeInterval time = nowish.timeIntervalSince1970;
    time = ((NSInteger) ((time + (FIFTEEN_MINUTES - 60)) / FIFTEEN_MINUTES)) * FIFTEEN_MINUTES;
    nowish = [NSDate dateWithTimeIntervalSince1970:time];
    self.startDate = nowish;
    self.endDate = [nowish dateByAddingTimeInterval:ONE_HOUR];
    
    self.timeFormatter = [NSDateFormatter new];
    [self.timeFormatter setDateStyle:NSDateFormatterMediumStyle];
    [self.timeFormatter setDateFormat:@"h:mm a"];
    [self.timeFormatter setTimeZone:delegate.timeZone];
    
    self.startTimeTextField.text = [self.timeFormatter stringFromDate:self.startDate];
    self.endTimeTextField.text = [self.timeFormatter stringFromDate:self.endDate];
}

-(void) chooseLocation:(id) sender {
    BCHPickerTableViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pickerList"];
    viewController.records = [BCHLocation locationNames];
    viewController.onSelection= ^(NSString* option) {
        [self.locationController setErrorText:nil errorAccessibilityValue:nil];
        self.locationTextField.text = option;
    };
    
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController];
    [self presentViewController:bottomSheet animated:true completion:nil];
}

-(void) chooseDay:(id) sender {
    BCHPickerTableViewController* viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pickerList"];
    viewController.records = self.days;
    viewController.onSelection = ^(NSString* option) {
        self.dayTextField.text = option;
    };
    
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController];
    [self presentViewController:bottomSheet animated:true completion:nil];
}

-(void) chooseStartTime:(id) sender {
    self.field = @"startTime";
    [self chooseTime:self.startTimeTextField date:self.startDate];
}

-(void) chooseEndTime:(id) sender {
    self.field = @"endTime";
    [self chooseTime:self.endTimeTextField date:self.endDate];
}

-(void) chooseTime:(UITextField*) textField date:(NSDate*) date {
    BCHTimePickerViewController* viewController =  [self.storyboard instantiateViewControllerWithIdentifier:@"pickerTime"];
    viewController.initialDate = date;
    MDCBottomSheetController *bottomSheet = [[MDCBottomSheetController alloc] initWithContentViewController:viewController];
    bottomSheet.delegate = self;
    [self presentViewController:bottomSheet animated:true completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)keyboardWillShow:(NSNotification*) notification {
    
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

-(void)keyboardWillHide {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
}

- (IBAction) doneButtonFromKeyboardClicked:(id)sender {
    [self.view endEditing:YES];
}

-(IBAction) create:(id)sender {
    BOOL ok = YES;
    if ([self.descriptionTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        [self.descriptionController setErrorText:@"C'mon. Gimme something" errorAccessibilityValue:nil];
        ok = NO;
    }
    if ([self.locationTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) {
        [self.locationController setErrorText:@"You gotta be somewhere, no?" errorAccessibilityValue:nil];
        ok = NO;
    }
    NSArray* events = [self.gridViewService itemsForDay:self.dayTextField.text];
    if (events.count <= 0) {
        ok = NO;
    }

    if (ok) {
        NSDate* dateOfDay = ((UnslackedEvent*) events[0]).time.startTime;
        
        NSDateFormatter* dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        dateFormatter.timeZone = [BaseAppDelegate instance].timeZone;
        
        NSDateFormatter* timeFormatter = [NSDateFormatter new];
        timeFormatter.dateFormat = @"HH:mmZ";
        timeFormatter.timeZone = [BaseAppDelegate instance].timeZone;
        
        NSString* datePortion = [dateFormatter stringFromDate:dateOfDay];
        
        NSString* startTimePortion = [timeFormatter stringFromDate:self.startDate];
        NSString* endTimePortion = [timeFormatter stringFromDate:self.endDate];
        
        NSDateFormatter* isoFormatter = [NSDateFormatter new];
        isoFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mmZ";
        isoFormatter.timeZone = [BaseAppDelegate instance].timeZone;
        
        
        NSDate* startTime = [isoFormatter dateFromString:[NSString stringWithFormat:@"%@T%@", datePortion, startTimePortion]];
        NSDate* endTime = [isoFormatter dateFromString:[NSString stringWithFormat:@"%@T%@", datePortion, endTimePortion]];
        
        while ([startTime isLaterThanDate:endTime]) {
            endTime = [endTime dateByAddingDays:1];
        }

        BaseAppDelegate* delegate = [BaseAppDelegate instance];
        [delegate.managedObjectContext performBlock:^{
            BCHReminder* reminder = [NSEntityDescription insertNewObjectForEntityForName:@"Reminder" inManagedObjectContext:delegate.managedObjectContext];
            reminder.reminderId = [NSUUID UUID].UUIDString;
            
            reminder.title = self.descriptionTextField.text;
            reminder.location = self.locationTextField.text;
            reminder.startTime = startTime;
            reminder.endTime = endTime;
            
            [delegate.managedObjectContext save:nil];

            [[NSNotificationCenter defaultCenter] postNotificationName:BCHReminderChangedNotification object:reminder];
        }];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void) assignEndTime:(NSDate*) date {
    self.endDate = date;
    self.endTimeTextField.text = [self.timeFormatter stringFromDate:self.endDate];
}

- (void) assignStartTime:(NSDate*) date {
    self.startDate = date;
    self.startTimeTextField.text = [self.timeFormatter stringFromDate:self.startDate];
}

- (void) bottomSheetControllerDidDismissBottomSheet:(nonnull MDCBottomSheetController*) controller {
    BCHTimePickerViewController* timePicker = (BCHTimePickerViewController*) controller.contentViewController;
    if ([@"startTime" isEqualToString:self.field]) {
        NSTimeInterval interval = self.endDate.timeIntervalSince1970 - self.startDate.timeIntervalSince1970;
        [self assignStartTime:timePicker.datePicker.date];
        [self assignEndTime:[timePicker.datePicker.date dateByAddingTimeInterval:interval]];
    } else if ([@"endTime" isEqualToString:self.field]) {
        [self assignEndTime:timePicker.datePicker.date];
    }
}

@end
