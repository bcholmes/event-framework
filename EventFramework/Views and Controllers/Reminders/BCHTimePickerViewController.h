//
//  BCHTimePickerViewController.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-05-19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCHTimePickerViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIDatePicker* datePicker;

@property (nonatomic, strong) NSDate* initialDate;

@end

NS_ASSUME_NONNULL_END
