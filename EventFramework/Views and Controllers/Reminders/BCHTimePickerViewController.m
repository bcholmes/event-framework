//
//  BCHTimePickerViewController.m
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-05-19.
//

#import "BCHTimePickerViewController.h"

#import "BaseAppDelegate.h"

@interface BCHTimePickerViewController ()

@end

@implementation BCHTimePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePicker.timeZone = [BaseAppDelegate instance].timeZone;
    self.datePicker.date = self.initialDate;
}

-(CGSize) preferredContentSize {
    CGSize size = [super preferredContentSize];
    return CGSizeMake(size.width, self.datePicker.frame.size.height + self.datePicker.frame.origin.x + 16.0);
}
@end
