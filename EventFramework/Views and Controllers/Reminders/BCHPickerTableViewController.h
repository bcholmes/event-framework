//
//  BCHPickerTableViewController.h
//  EventFramework
//
//  Created by BC Holmes on 2019-05-18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^selectionHandler)(NSString*);

@interface BCHPickerTableViewController : UITableViewController

@property (nonatomic, nullable, strong) NSArray* records;

@property (nonatomic, copy) selectionHandler onSelection;

@end

NS_ASSUME_NONNULL_END
