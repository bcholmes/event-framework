//
//  BCHReminderViewController.h
//  1PasswordExtension
//
//  Created by BC Holmes on 2019-05-20.
//

#import <UIKit/UIKit.h>

#import "BCHReminder.h"

NS_ASSUME_NONNULL_BEGIN

@interface BCHReminderViewController : UIViewController

@property (nonatomic, strong) BCHReminder* reminder;

@end

NS_ASSUME_NONNULL_END
