//
//  BCHLightboxView.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-14.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "BCHLightboxView.h"

@implementation BCHLightboxView

-(void) awakeFromNib {
    [super awakeFromNib];
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 25.f;
}

@end
