//
//  BCHMenuTableViewCell.h
//  EventFramework
//
//  Created by BC Holmes on 2017-06-08.
//  Copyright © 2017 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHMenuTableViewCell : UITableViewCell

@property (nonatomic, weak, nullable) IBOutlet UILabel* countLabel;

@end
