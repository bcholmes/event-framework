//
//  BCHBubbleView.h
//  EventFramework
//
//  Created by BC Holmes on 2017-06-07.
//  Copyright © 2017 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHBubbleLabel : UILabel

@property (nullable, nonatomic, strong) UIColor* bubbleColor;

@end
