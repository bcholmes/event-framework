//
//  BCHTokenGenerator.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-05-09.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHTokenGenerator : NSObject

-(id) initWithClientId:(NSString*) clientId andPersonId:(NSString*) personId;

@property (nonatomic, strong) NSString* clientId;
@property (nonatomic, strong) NSString* personId;
@property (nonatomic, readonly) NSString* token;

@end
