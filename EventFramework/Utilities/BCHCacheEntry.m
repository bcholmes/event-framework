//
//  BCHCacheEntry.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-08.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHCacheEntry.h"

@implementation BCHCacheEntry

- (id) initWithCoder:(NSCoder*)decoder {
    if ((self = [super init])) {
        self.data = [decoder decodeObjectForKey:@"data"];
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.data forKey:@"data"];
}

- (NSString *) description {
    return [NSString stringWithFormat:@"{ data length=%lu }",
            (unsigned long)[self.data length]];
}

@end
