//
//  UIDevice.m
//  unslacked-app
//
//  Created by BC Holmes on 2012-09-23.
//
//

#import "UIDevice+Utils.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@implementation UIDevice (Utils)

+(bool) isIPad {
    return [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad;
}

@end
