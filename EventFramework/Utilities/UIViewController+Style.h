//
//  UIViewController+Style.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-10-09.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Style)

-(void) styleNavigationBar:(NSString*) title;
-(void) initializeMenuButton;
-(void) login;

@end
