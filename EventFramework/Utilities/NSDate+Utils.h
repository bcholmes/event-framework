//
//  UIDate+Utils.h
//  WisSched
//
//  Created by BC Holmes on 2016-03-13.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

-(NSString*) dayString:(NSTimeZone*) timeZone;

@end
