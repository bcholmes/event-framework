//
//  BCHVersionHelper.h
//  EventsSchedule
//
//  Created by BC Holmes on 12/7/2013.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BCHVersionHelper : NSObject

+(void) setNavigationBar:(UINavigationController*) navigationController toColor: (UIColor*) color;
+(void) setCellBackground:(UITableViewCell*) cell toColor: (UIColor*) color;
+(void) setNavigationTextColor:(UINavigationController*) navigationController toColor:(UIColor*) color;

@end
