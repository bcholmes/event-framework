//
//  BCHLoginHelper.h
//  EventsSchedule
//
//  Created by BC Holmes on 2014-05-04.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHLoginHelper : NSObject

@property (nonatomic, readonly) BOOL isLoggedIn;
@property (nonatomic, readonly) BOOL isAlreadyPrompted;
@property (nonatomic, readonly) NSString* personId;
@property (nonatomic, readonly) NSString* appKey;
@property (nonatomic, readonly) NSString* jwtToken;

-(void) recordLogin:(NSString*) personId appKey:(NSString*) appKey jwtToken:(NSString*) jwtToken;
-(void) clearLogin;
-(void) markAsPrompted;

@end
