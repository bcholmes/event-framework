//
//  HttpHelper.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BCHHttpHelper : NSObject {
}

-(NSURL*) constructUrl:(NSString*) path;

// Old API - to be deprecated
+ (void) sendFormData:(NSString*) data toUrl:(NSURL*) url onSuccess:(void (^)(NSData*,NSString*))successHandler onError:(void (^)(NSError*,NSData*,NSDictionary*)) errorHandler;
+ (void) sendJson:(NSString*) json toUrl:(NSURL*) url headers:(NSDictionary*) headers onSuccess:(void (^)(NSData*,NSString*))successHandler onError:(void (^)(NSError*,NSData*,NSDictionary*)) errorHandler;


// New API - much nicer
- (void) getJsonFromPath: (NSString*) path callback: (void(^)(NSError*, NSURLResponse* response, NSData *)) callback;
- (void) postJsonData: (NSData*) jsonData withPath: (NSString *) path callback: (void(^)(NSError*, NSURLResponse* response, NSData *)) callback;
- (void) sendFormData:(NSDictionary*) jsonData withPath:(NSString*) path callback:(void(^)(NSError*, NSURLResponse* response, NSData *)) callback;
@end
