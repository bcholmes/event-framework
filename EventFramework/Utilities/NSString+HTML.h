//
//  NSString+HTML.h
//  EventFramework
//
//  Created by BC Holmes on 2019-06-15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (HTML)

-(NSString*) stringByRemovingHtmlMarkup;

@end

NS_ASSUME_NONNULL_END
