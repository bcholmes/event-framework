//
//  BCHOpenItemButton.h
//  WisSched
//
//  Created by BC Holmes on 2016-08-21.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCHOpenItemButton : UIControl

@property (nonatomic, strong) UIColor* highlightedColor;

@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) UILabel* textLabel;
@property (nonatomic, strong) UIColor* inkColor;
@property (nonatomic, strong) NSString* secondaryText;

@end
