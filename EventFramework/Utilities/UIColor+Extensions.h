//
//  UIColor+Extensions.h
//
//  Created by BC Holmes on 2014-04-11.
//  Copyright (c) 2014 2014 Ayizan Studios.. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (Extensions)

- (UIColor*)blendWithColor:(UIColor*)color2 alpha:(CGFloat)alpha2;

@end
