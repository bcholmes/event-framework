//
//  NSString+HTML.m
//  EventFramework
//
//  Created by BC Holmes on 2019-06-15.
//

#import "NSString+HTML.h"

@implementation NSString (HTML)

- (NSString*) replaceAllViaRegex:(NSString *)string pattern:(NSString*) pattern replacement:(NSString*) replacement {
    NSError *error = nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    if(error != nil){
        NSLog(@"Error: %@",error);
        return string;
    } else{
        NSString *replaced = [regex stringByReplacingMatchesInString:string options:0 range:NSMakeRange(0, [string length]) withTemplate:replacement];
        return replaced;
    }
}

-(NSString*) stringByRemovingHtmlMarkup {
    NSMutableString* fullTitle = [self mutableCopy];
    
    NSRange range = [fullTitle rangeOfString:@"<"];
    while (range.location != NSNotFound) {
        NSRange range2 = [fullTitle rangeOfString:@">"];
        
        range.length = range2.location - range.location + 1;
        [fullTitle deleteCharactersInRange:range];
        range = [fullTitle rangeOfString:@"<"];
    }

    NSString* string = [self replaceAllViaRegex:[NSString stringWithString:fullTitle] pattern:@"&#8217;" replacement:@"'"];
    return [self replaceAllViaRegex:string pattern:@"&(?:[a-z\\d]+|#\\d+|#x[a-f\\d]+);" replacement:@""];
}
@end
