//
//  BCHHttpHelper.m
//  unslacked-app
//
//  Created by BC Holmes on 11-07-03.
//  Copyright 2011 Ayizan Studios. All rights reserved.
//

#import "BCHHttpHelper.h"
#import <UIDevice-Hardware/UIDevice-Hardware.h>

#define AYIZAN_ORG @"https://ayizan.org"

@implementation BCHHttpHelper


+ (void) sendRequest:(NSMutableURLRequest*) request toUrl:(NSURL*) url headers:(NSDictionary*) headers onSuccess:(void (^)(NSData*,NSString*))successHandler onError:(void (^)(NSError*,NSData*,NSDictionary*)) errorHandler {
    [request addValue:[[UIDevice currentDevice] modelName] forHTTPHeaderField: @"X-UA-Device"];
    if (headers != nil) {
        for (NSString* key in headers.allKeys) {
            [request setValue:headers[key] forHTTPHeaderField:key];
        }
    }

    NSURLResponse* response;
    NSError* error;
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (data != nil) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"http json status code: %ld  and content type: %@", (long) httpResponse.statusCode, [[httpResponse allHeaderFields] valueForKey:@"content-type"]);
        if (httpResponse.statusCode == 200) {
            successHandler(data, [[httpResponse allHeaderFields] valueForKey:@"content-type"]);
        } else if (data == nil || error != nil || httpResponse.statusCode != 200) {
            if (errorHandler != nil) {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    errorHandler(data == nil ? nil : error, data, httpResponse.allHeaderFields);
                });
            }
        }
    }
}
+ (void) sendFormData:(NSString*) data toUrl:(NSURL*) url onSuccess:(void (^)(NSData*,NSString*))successHandler onError:(void (^)(NSError*,NSData*,NSDictionary*)) errorHandler {

    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
            [self sendFormData:data toUrl:url onSuccess:^(NSData* data, NSString* contentType) {
                if (successHandler != nil) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        successHandler(data, contentType);
                    });
                }
            } onError:errorHandler];
        });
    } else {
        NSMutableURLRequest* post = [NSMutableURLRequest requestWithURL: url];
        if (data != nil) {
            [post addValue: @"application/x-www-form-urlencoded;charset=utf-8" forHTTPHeaderField: @"Content-Type"];
            [post setHTTPMethod: @"POST"];
            [post setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [BCHHttpHelper sendRequest:post toUrl:url headers:nil onSuccess:successHandler onError:errorHandler];
    }
}

+ (void) sendJson:(NSString*) json toUrl:(NSURL*) url headers:(NSDictionary*) headers onSuccess:(void (^)(NSData*,NSString*))successHandler onError:(void (^)(NSError*,NSData*,NSDictionary*)) errorHandler {
    
    if ([NSThread isMainThread]) {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
            [self sendJson:json toUrl:url headers:headers onSuccess:^(NSData* data, NSString* contentType) {
                if (successHandler != nil) {
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        successHandler(data, contentType);
                    });
                }
            } onError:errorHandler];
        });
    } else {
        
        NSMutableURLRequest* post = [NSMutableURLRequest requestWithURL: url];
        if (json != nil) {
            [post addValue: @"application/json;charset=utf-8" forHTTPHeaderField: @"Content-Type"];
            [post setHTTPMethod: @"POST"];
            [post setHTTPBody:[json dataUsingEncoding:NSUTF8StringEncoding]];
        }
        [BCHHttpHelper sendRequest:post toUrl:url headers:headers onSuccess:successHandler onError:errorHandler];
    }
}

- (void) configureStandardHeaders:(NSMutableURLRequest*) request forPath:(NSString*) path {
    [request addValue:[[UIDevice currentDevice] modelName] forHTTPHeaderField: @"X-UA-Device"];
    request.HTTPShouldHandleCookies = NO;
}

- (void) getJsonFromPath:(NSString *) path callback: (void(^)(NSError*, NSURLResponse* response, NSData*)) callback {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [request setURL:[self constructUrl:path]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self configureStandardHeaders:request forPath:path];
    [self processRequest:request callback:callback];
}

- (void) postJsonData: (NSData *) jsonData withPath: (NSString *) path callback: (void(^)(NSError*, NSURLResponse* response, NSData *)) callback {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString: [AYIZAN_ORG stringByAppendingString:path ]]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [self configureStandardHeaders:request forPath:path];
    [request setHTTPBody:jsonData];
    [self processRequest:request callback:callback];
}

-(NSURL*) constructUrl:(NSString*) path {
    if ([path rangeOfString:@"http:"].location == 0 || [path rangeOfString:@"https:"].location == 0) {
        return [NSURL URLWithString:path];
    } else {
        return [NSURL URLWithString: [AYIZAN_ORG stringByAppendingString:path]];
    }
}

- (void) sendFormData: (NSDictionary*) jsonData withPath:(NSString*) path callback:(void(^)(NSError*, NSURLResponse* response, NSData *)) callback {
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] init];
    [request setURL:[self constructUrl:path]];
    [request addValue: @"application/x-www-form-urlencoded;charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setHTTPMethod: @"POST"];
    
    NSMutableString* data = [NSMutableString new];
    for (NSString* key in jsonData.allKeys) {
        if (data.length > 0) {
            [data appendString:@"&"];
        }
        NSString* value = [jsonData objectForKey:key];
        [data appendFormat:@"%@=%@", key, [value stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }    
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    [self configureStandardHeaders:request forPath:path];
    [self processRequest:request callback:callback];
}


- (NSURLSession*) oneAtATimeSession {
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        [configuration setHTTPMaximumConnectionsPerHost:1];
        session = [NSURLSession sessionWithConfiguration:configuration];
        
    });
    return session;
}

- (void) processRequest:(NSMutableURLRequest*) request callback:(void(^)(NSError* error, NSURLResponse* response, NSData* data)) callback {
    NSURLSessionDataTask* task = [[self oneAtATimeSession] dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* connectionError) {
        
        NSLog(@"http request (%@) status code: %ld", request.URL, (long) ((NSHTTPURLResponse*) response).statusCode);

        if (callback == nil) {
            // nothing to do...
        } else if (connectionError && [connectionError.domain isEqualToString:NSURLErrorDomain] && connectionError.code == NSURLErrorUserCancelledAuthentication) {
            callback([[NSError alloc] initWithDomain:@"org.ayizan.http" code:401 userInfo:nil], nil, nil);
        } else if (connectionError) {
            callback(connectionError, nil, nil);
        } else {
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*) response;
            if (httpResponse.statusCode >= 200 || httpResponse.statusCode < 300) {
                callback(nil, response, data);
            } else {
                callback([[NSError alloc] initWithDomain:@"org.ayizan.http" code:httpResponse.statusCode userInfo:nil], nil, nil);
            }
        }
    }];
    [task resume];
}

@end
