//
//  BCHLoginHelper.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-05-04.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "BCHLoginHelper.h"

@interface BCHLoginData : NSObject

@property (nonatomic, nonnull, strong) NSString* appKey;
@property (nonatomic, nonnull, strong) NSString* personId;
@property (nonatomic, nonnull, strong) NSString* jwtToken;
@property (nonatomic, assign) BOOL promptedForLogin;

@end

@implementation BCHLoginData

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.appKey forKey:@"appKey"];
    [encoder encodeObject:self.personId forKey:@"personId"];
    [encoder encodeObject:self.jwtToken forKey:@"jwtToken"];
    [encoder encodeBool:self.promptedForLogin forKey:@"promptedForLogin"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.appKey = [decoder decodeObjectForKey:@"appKey"];
        self.personId = [decoder decodeObjectForKey:@"personId"];
        self.jwtToken = [decoder decodeObjectForKey:@"jwtToken"];
        self.promptedForLogin = [decoder decodeBoolForKey:@"promptedForLogin"];
    }
    return self;
}

@end

@interface BCHLoginHelper()

@property (nonnull, nonatomic, strong) BCHLoginData* loginData;

@end

@implementation BCHLoginHelper

-(instancetype) init {
    if (self = [super init]) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        NSData* storedEncodedObject = [userDefaults objectForKey:@"loginData"];
        if (storedEncodedObject) {
            self.loginData = [NSKeyedUnarchiver unarchiveObjectWithData:storedEncodedObject];
        } else {
            self.loginData = [BCHLoginData new];
        }
    }
    return self;
}

-(NSString*) appKey {
    return self.loginData.appKey;
}

-(NSString*) personId {
    return self.loginData.personId;
}

-(NSString*) jwtToken {
    return self.loginData.jwtToken;
}

-(BOOL) isLoggedIn {
    return self.loginData.appKey != nil;
}

-(BOOL) isAlreadyPrompted {
    return self.loginData.promptedForLogin;
}

-(void) recordLogin:(NSString*) personId appKey:(NSString*) appKey jwtToken:(NSString*) jwtToken {
    self.loginData.personId = personId;
    self.loginData.appKey = appKey;
    self.loginData.jwtToken = jwtToken;
    [self persist];
}

-(void) markAsPrompted {
    self.loginData.promptedForLogin = YES;
    [self persist];
}

-(void) clearLogin {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"loginData"];
    self.loginData = [BCHLoginData new];
}

-(void) persist {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.loginData] forKey:@"loginData"];
    [userDefaults synchronize];
}

@end
