//
//  UIDate+Utils.m
//  WisSched
//
//  Created by BC Holmes on 2016-03-13.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

-(NSString*) dayString:(NSTimeZone*) timeZone {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setTimeZone:timeZone];
    [formatter setLocale:usLocale];
    
    formatter.dateFormat = @"EEEE";
    return [formatter stringFromDate:self];
}

@end
