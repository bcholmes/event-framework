//
//  UIViewController+Style.m
//  EventsSchedule
//
//  Created by BC Holmes on 2014-10-09.
//  Copyright (c) 2014 Ayizan Studios. All rights reserved.
//

#import "UIViewController+Style.h"
#import "BaseAppDelegate.h"
#import "SWRevealViewController.h"
#import "BCHLoginViewController.h"
#import "BCHTheme.h"

@implementation UIViewController (Style)

-(void) styleNavigationBar:(NSString*) title {
    self.navigationController.navigationBar.topItem.title = title;
    self.navigationController.navigationBar.translucent = NO;
}

-(void) initializeMenuButton {
    SWRevealViewController *revealController = [self revealViewController];
    
    [self.navigationController.navigationBar addGestureRecognizer:revealController.panGestureRecognizer];
    
    UIBarButtonItem *revealButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"hamburger"]
                                                                         style:UIBarButtonItemStylePlain target:revealController action:@selector(revealToggle:)];
    
    self.navigationItem.leftBarButtonItem = revealButtonItem;
}

- (void) login {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    BCHLoginViewController* controller = [storyboard instantiateViewControllerWithIdentifier:@"login"];
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

@end
