//
//  BCHDateFormattingHelper.h
//  WisSched
//
//  Created by BC Holmes on 2016-05-20.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHDateFormattingHelper : NSObject

@property (nonatomic, retain) NSTimeZone* timeZone;
@property (nonatomic, readonly) NSDateFormatter* hourAndAmPmFormatter;
@property (nonatomic, readonly) NSDateFormatter* hourMinuteAmPmFormatter;


+(instancetype) instance;

@end
