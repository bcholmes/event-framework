//
//  NSAttributedString+Html.h
//  EventsSchedule
//
//  Created by BC Holmes on 2015-04-03.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

typedef enum {
    BCHHtmlBoldStyle          = 1 << 0,
    BCHHtmlItalicStyle        = 1 << 1,
    BCHHtmlStrikethroughStyle = 1 << 2
} BCHHtmlStyle;


@protocol BCHStyleProvider <NSObject>

-(NSDictionary*) findStyleWithAttributes:(BCHHtmlStyle) htmlStyles size:(CGFloat) size;

@end

@interface NSAttributedString (HTML)

+(NSAttributedString*) fromHtml:(NSString*) html;
+(NSAttributedString*) fromHtml:(NSString*) html withFontSize:(CGFloat) size;
+(NSAttributedString*) fromHtml:(NSString*) html fontSize:(CGFloat) fontSize styleProvider:(NSObject<BCHStyleProvider>*) styleProvider;

@end
