//
//  BCHVersionHelper.m
//  EventsSchedule
//
//  Created by BC Holmes on 12/7/2013.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import "BCHVersionHelper.h"

@implementation BCHVersionHelper

+(void) setNavigationBar:(UINavigationController*) navigationController toColor: (UIColor*) color {
    navigationController.navigationBar.barTintColor = color;
    navigationController.navigationBar.translucent = NO;
}

+(void) setCellBackground:(UITableViewCell*) cell toColor: (UIColor*) color {
    cell.contentView.backgroundColor=color;
}

+(void) setNavigationTextColor:(UINavigationController*) navigationController toColor:(UIColor*) color {
    navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : color};
}

@end
