//
//  NSAttributedString+Html.m
//  EventsSchedule
//
//  Created by BC Holmes on 2015-04-03.
//  Copyright (c) 2015 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSAttributedString+Html.h"

@interface BCHTrivialStyleProvider : NSObject<BCHStyleProvider>

@end

@implementation BCHTrivialStyleProvider

-(NSDictionary*) findStyleWithAttributes:(BCHHtmlStyle) htmlStyles size:(CGFloat) fontSize {
    UIFont* font = [UIFont systemFontOfSize:fontSize];
    
    UIFontDescriptorSymbolicTraits traits = 0;
    if (htmlStyles & BCHHtmlBoldStyle) {
        traits |= UIFontDescriptorTraitBold;
    }
    if (htmlStyles & BCHHtmlItalicStyle)
    {
        traits |= UIFontDescriptorTraitItalic;
    }
    
    UIFont* newFont = [UIFont fontWithDescriptor:[[font fontDescriptor] fontDescriptorWithSymbolicTraits:traits] size:font.pointSize];
    
    if (htmlStyles & BCHHtmlStrikethroughStyle) {
        return @{ NSStrikethroughStyleAttributeName: [NSNumber numberWithInteger:NSUnderlinePatternSolid | NSUnderlineStyleSingle],
                  NSFontAttributeName : newFont,
                  NSForegroundColorAttributeName : [UIColor blackColor] };
    } else {
        return @{ NSFontAttributeName : newFont,
                  NSForegroundColorAttributeName : [UIColor blackColor]};
    }
}

@end



@implementation NSAttributedString (HTML)

static NSDictionary* htmlEntities;

+(NSDictionary*) initializeEntities {
    NSDictionary* result = [NSDictionary dictionaryWithObjectsAndKeys:
                            
                            // Taken from http://www.w3.org/TR/xhtml1/dtds.html#a_dtd_Special_characters
                            // Ordered by uchar lowest to highest for bsearching
                            // A.2.2. Special characters
                            [NSAttributedString stringFromUnichar:34], @"&quot;",
                            [NSAttributedString stringFromUnichar:38], @"&amp;",
                            [NSAttributedString stringFromUnichar:39], @"&apos;",
                            [NSAttributedString stringFromUnichar:60], @"&lt;",
                            [NSAttributedString stringFromUnichar:62], @"&gt;",
                            
                            // A.2.1. Latin-1 characters
                            [NSAttributedString stringFromUnichar:160], @"&nbsp;",
                            [NSAttributedString stringFromUnichar:161], @"&iexcl;",
                            [NSAttributedString stringFromUnichar:162], @"&cent;",
                            [NSAttributedString stringFromUnichar:163], @"&pound;",
                            [NSAttributedString stringFromUnichar:164], @"&curren;",
                            [NSAttributedString stringFromUnichar:165],  @"&yen;",
                            [NSAttributedString stringFromUnichar:166], @"&brvbar;",
                            [NSAttributedString stringFromUnichar:167], @"&sect;",
                            [NSAttributedString stringFromUnichar:168], @"&uml;",
                            [NSAttributedString stringFromUnichar:169], @"&copy;",
                            [NSAttributedString stringFromUnichar:170], @"&ordf;",
                            [NSAttributedString stringFromUnichar:171], @"&laquo;",
                            [NSAttributedString stringFromUnichar:172],  @"&not;",
                            [NSAttributedString stringFromUnichar:173],  @"&shy;",
                            [NSAttributedString stringFromUnichar:174], @"&reg;",
                            [NSAttributedString stringFromUnichar:175], @"&macr;",
                            [NSAttributedString stringFromUnichar:176], @"&deg;",
                            [NSAttributedString stringFromUnichar:177], @"&plusmn;",
                            [NSAttributedString stringFromUnichar:178], @"&sup2;",
                            [NSAttributedString stringFromUnichar:179], @"&sup3;",
                            [NSAttributedString stringFromUnichar:180], @"&acute;",
                            [NSAttributedString stringFromUnichar:181], @"&micro;",
                            [NSAttributedString stringFromUnichar:182], @"&para;",
                            [NSAttributedString stringFromUnichar:183], @"&middot;",
                            [NSAttributedString stringFromUnichar:184], @"&cedil;",
                            [NSAttributedString stringFromUnichar:185], @"&sup1;",
                            [NSAttributedString stringFromUnichar:186], @"&ordm;",
                            [NSAttributedString stringFromUnichar:187], @"&raquo;",
                            [NSAttributedString stringFromUnichar:188], @"&frac14;",
                            [NSAttributedString stringFromUnichar:189], @"&frac12;",
                            [NSAttributedString stringFromUnichar:190], @"&frac34;",
                            [NSAttributedString stringFromUnichar:191], @"&iquest;",
                            [NSAttributedString stringFromUnichar:192], @"&Agrave;",
                            [NSAttributedString stringFromUnichar:193], @"&Aacute;",
                            [NSAttributedString stringFromUnichar:194], @"&Acirc;",
                            [NSAttributedString stringFromUnichar:195],  @"&Atilde;",
                            [NSAttributedString stringFromUnichar:196],  @"&Auml;",
                            [NSAttributedString stringFromUnichar:197],  @"&Aring;",
                            [NSAttributedString stringFromUnichar:198], @"&AElig;",
                            [NSAttributedString stringFromUnichar:199], @"&Ccedil;",
                            [NSAttributedString stringFromUnichar:200], @"&Egrave;",
                            [NSAttributedString stringFromUnichar:201], @"&Eacute;",
                            [NSAttributedString stringFromUnichar:202], @"&Ecirc;",
                            [NSAttributedString stringFromUnichar:203], @"&Euml;",
                            [NSAttributedString stringFromUnichar:204], @"&Igrave;",
                            [NSAttributedString stringFromUnichar:205],  @"&Iacute;",
                            [NSAttributedString stringFromUnichar:206], @"&Icirc;",
                            [NSAttributedString stringFromUnichar:207], @"&Iuml;",
                            [NSAttributedString stringFromUnichar:208], @"&ETH;",
                            [NSAttributedString stringFromUnichar:209], @"&Ntilde;",
                            [NSAttributedString stringFromUnichar:210], @"&Ograve;",
                            [NSAttributedString stringFromUnichar:211], @"&Oacute;",
                            [NSAttributedString stringFromUnichar:212],  @"&Ocirc;",
                            [NSAttributedString stringFromUnichar:213], @"&Otilde;",
                            [NSAttributedString stringFromUnichar:214], @"&Ouml;",
                            [NSAttributedString stringFromUnichar:215], @"&times;",
                            [NSAttributedString stringFromUnichar:216], @"&Oslash;",
                            [NSAttributedString stringFromUnichar:217],  @"&Ugrave;",
                            [NSAttributedString stringFromUnichar:218], @"&Uacute;",
                            [NSAttributedString stringFromUnichar:219], @"&Ucirc;",
                            [NSAttributedString stringFromUnichar:220], @"&Uuml;",
                            [NSAttributedString stringFromUnichar:221],  @"&Yacute;",
                            [NSAttributedString stringFromUnichar:222],  @"&THORN;",
                            [NSAttributedString stringFromUnichar:223], @"&szlig;",
                            [NSAttributedString stringFromUnichar:224],  @"&agrave;",
                            [NSAttributedString stringFromUnichar:225], @"&aacute;",
                            [NSAttributedString stringFromUnichar:226], @"&acirc;",
                            [NSAttributedString stringFromUnichar:227], @"&atilde;",
                            [NSAttributedString stringFromUnichar:228], @"&auml;",
                            [NSAttributedString stringFromUnichar:229], @"&aring;",
                            [NSAttributedString stringFromUnichar:230], @"&aelig;",
                            [NSAttributedString stringFromUnichar:231], @"&ccedil;",
                            [NSAttributedString stringFromUnichar:232], @"&egrave;",
                            [NSAttributedString stringFromUnichar:233], @"&eacute;",
                            [NSAttributedString stringFromUnichar:234],  @"&ecirc;",
                            [NSAttributedString stringFromUnichar:235],  @"&euml;",
                            [NSAttributedString stringFromUnichar:236], @"&igrave;",
                            [NSAttributedString stringFromUnichar:237], @"&iacute;",
                            [NSAttributedString stringFromUnichar:238], @"&icirc;",
                            [NSAttributedString stringFromUnichar:239], @"&iuml;",
                            [NSAttributedString stringFromUnichar:240], @"&eth;",
                            [NSAttributedString stringFromUnichar:241], @"&ntilde;",
                            [NSAttributedString stringFromUnichar:242],  @"&ograve;",
                            [NSAttributedString stringFromUnichar:243], @"&oacute;",
                            [NSAttributedString stringFromUnichar:244], @"&ocirc;",
                            [NSAttributedString stringFromUnichar:245], @"&otilde;",
                            [NSAttributedString stringFromUnichar:246], @"&ouml;",
                            [NSAttributedString stringFromUnichar:247],  @"&divide;",
                            [NSAttributedString stringFromUnichar:248], @"&oslash;",
                            [NSAttributedString stringFromUnichar:249], @"&ugrave;",
                            [NSAttributedString stringFromUnichar:250], @"&uacute;",
                            [NSAttributedString stringFromUnichar:251],  @"&ucirc;",
                            [NSAttributedString stringFromUnichar:252], @"&uuml;",
                            [NSAttributedString stringFromUnichar:253], @"&yacute;",
                            [NSAttributedString stringFromUnichar:254],@"&thorn;",
                            [NSAttributedString stringFromUnichar:255],@"&yuml;",

                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:338],@"&OElig;",
                            [NSAttributedString stringFromUnichar:339],@"&oelig;",
                            [NSAttributedString stringFromUnichar:352],@"&Scaron;",
                            [NSAttributedString stringFromUnichar:353],@"&scaron;",
                            [NSAttributedString stringFromUnichar:376],@"&Yuml;",
                            
                            // A.2.3. Symbols
                            [NSAttributedString stringFromUnichar:402], @"&fnof;",
                            
                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:710],@"&circ;",
                            [NSAttributedString stringFromUnichar:732],@"&tilde;",
                            
                            // A.2.3. Symbols cont'd
                            [NSAttributedString stringFromUnichar:913], @"&Alpha;",
                            [NSAttributedString stringFromUnichar:914], @"&Beta;",
                            [NSAttributedString stringFromUnichar:915], @"&Gamma;",
                            [NSAttributedString stringFromUnichar:916], @"&Delta;",
                            [NSAttributedString stringFromUnichar:917], @"&Epsilon;",
                            [NSAttributedString stringFromUnichar:918], @"&Zeta;",
                            [NSAttributedString stringFromUnichar:919], @"&Eta;",
                            [NSAttributedString stringFromUnichar:920], @"&Theta;",
                            [NSAttributedString stringFromUnichar:921], @"&Iota;",
                            [NSAttributedString stringFromUnichar:922], @"&Kappa;",
                            [NSAttributedString stringFromUnichar:923], @"&Lambda;",
                            [NSAttributedString stringFromUnichar:924], @"&Mu;",
                            [NSAttributedString stringFromUnichar:925], @"&Nu;",
                            [NSAttributedString stringFromUnichar:926], @"&Xi;",
                            [NSAttributedString stringFromUnichar:927], @"&Omicron;",
                            [NSAttributedString stringFromUnichar:928], @"&Pi;",
                            [NSAttributedString stringFromUnichar:929], @"&Rho;",
                            [NSAttributedString stringFromUnichar:931], @"&Sigma;",
                            [NSAttributedString stringFromUnichar:932], @"&Tau;",
                            [NSAttributedString stringFromUnichar:933], @"&Upsilon;",
                            [NSAttributedString stringFromUnichar:934], @"&Phi;",
                            [NSAttributedString stringFromUnichar:935],  @"&Chi;",
                            [NSAttributedString stringFromUnichar:936], @"&Psi;",
                            [NSAttributedString stringFromUnichar:937], @"&Omega;",
                            [NSAttributedString stringFromUnichar:945], @"&alpha;",
                            [NSAttributedString stringFromUnichar:946],  @"&beta;",
                            [NSAttributedString stringFromUnichar:947], @"&gamma;",
                            [NSAttributedString stringFromUnichar:948], @"&delta;",
                            [NSAttributedString stringFromUnichar:949], @"&epsilon;",
                            [NSAttributedString stringFromUnichar:950], @"&zeta;",
                            [NSAttributedString stringFromUnichar:951],  @"&eta;",
                            [NSAttributedString stringFromUnichar:952], @"&theta;",
                            [NSAttributedString stringFromUnichar:953], @"&iota;",
                            [NSAttributedString stringFromUnichar:954], @"&kappa;",
                            [NSAttributedString stringFromUnichar:955], @"&lambda;",
                            [NSAttributedString stringFromUnichar:956], @"&mu;",
                            [NSAttributedString stringFromUnichar:957], @"&nu;",
                            [NSAttributedString stringFromUnichar:958], @"&xi;",
                            [NSAttributedString stringFromUnichar:959], @"&omicron;",
                            [NSAttributedString stringFromUnichar:960], @"&pi;",
                            [NSAttributedString stringFromUnichar:961], @"&rho;",
                            [NSAttributedString stringFromUnichar:962], @"&sigmaf;",
                            [NSAttributedString stringFromUnichar:963], @"&sigma;",
                            [NSAttributedString stringFromUnichar:964], @"&tau;",
                            [NSAttributedString stringFromUnichar:965], @"&upsilon;",
                            [NSAttributedString stringFromUnichar:966], @"&phi;",
                            [NSAttributedString stringFromUnichar:967], @"&chi;",
                            [NSAttributedString stringFromUnichar:968], @"&psi;",
                            [NSAttributedString stringFromUnichar:969], @"&omega;",
                            [NSAttributedString stringFromUnichar:977], @"&thetasym;",
                            [NSAttributedString stringFromUnichar:978], @"&upsih;",
                            [NSAttributedString stringFromUnichar:982], @"&piv;",
                            
                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:8194],@"&ensp;",
                            [NSAttributedString stringFromUnichar:8195],@"&emsp;",
                            [NSAttributedString stringFromUnichar:8201],@"&thinsp;",
                            [NSAttributedString stringFromUnichar:8204],@"&zwnj;",
                            [NSAttributedString stringFromUnichar:8205],@"&zwj;",
                            [NSAttributedString stringFromUnichar:8206],@"&lrm;",
                            [NSAttributedString stringFromUnichar:8207], @"&rlm;",
                            
                            [NSAttributedString stringFromUnichar:8211],@"&ndash;",
                            [NSAttributedString stringFromUnichar:8212],@"&mdash;",
                            [NSAttributedString stringFromUnichar:8216],@"&lsquo;",
                            [NSAttributedString stringFromUnichar:8217],@"&rsquo;",
                            [NSAttributedString stringFromUnichar:8218],@"&sbquo;",
                            [NSAttributedString stringFromUnichar:8220],@"&ldquo;",
                            [NSAttributedString stringFromUnichar:8221],@"&rdquo;",
                            [NSAttributedString stringFromUnichar:8222],@"&bdquo;",
                            
                            [NSAttributedString stringFromUnichar:8224],@"&dagger;",
                            [NSAttributedString stringFromUnichar:8225],@"&Dagger;",
                            // A.2.3. Symbols cont'd
                            [NSAttributedString stringFromUnichar:8226], @"&bull;",
                            [NSAttributedString stringFromUnichar:8230], @"&hellip;",
                            
                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:8240],@"&permil;",
                            
                            // A.2.3. Symbols cont'd
                            [NSAttributedString stringFromUnichar:8242], @"&prime;",
                            [NSAttributedString stringFromUnichar:8243], @"&Prime;",
                            
                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:8249], @"&lsaquo;",
                            [NSAttributedString stringFromUnichar:8250], @"&rsaquo;",
                            
                            // A.2.3. Symbols cont'd
                            [NSAttributedString stringFromUnichar:8254], @"&oline;",
                            [NSAttributedString stringFromUnichar:8260], @"&frasl;", 
                            
                            // A.2.2. Special characters cont'd
                            [NSAttributedString stringFromUnichar:8364],@"&euro;", 
                            
                            // A.2.3. Symbols cont'd  
                            [NSAttributedString stringFromUnichar:8465], @"&image;", 
                            [NSAttributedString stringFromUnichar:8472], @"&weierp;", 
                            [NSAttributedString stringFromUnichar:8476], @"&real;", 
                            [NSAttributedString stringFromUnichar:8482], @"&trade;", 
                            [NSAttributedString stringFromUnichar:8501], @"&alefsym;", 
                            [NSAttributedString stringFromUnichar:8592], @"&larr;", 
                            [NSAttributedString stringFromUnichar:8593], @"&uarr;", 
                            [NSAttributedString stringFromUnichar:8594], @"&rarr;", 
                            [NSAttributedString stringFromUnichar:8595], @"&darr;", 
                            [NSAttributedString stringFromUnichar:8596], @"&harr;", 
                            [NSAttributedString stringFromUnichar:8629], @"&crarr;",
                            [NSAttributedString stringFromUnichar:8656], @"&lArr;", 
                            [NSAttributedString stringFromUnichar:8657], @"&uArr;", 
                            [NSAttributedString stringFromUnichar:8658], @"&rArr;", 
                            [NSAttributedString stringFromUnichar:8659], @"&dArr;", 
                            [NSAttributedString stringFromUnichar:8660], @"&hArr;", 
                            [NSAttributedString stringFromUnichar:8704], @"&forall;", 
                            [NSAttributedString stringFromUnichar:8706], @"&part;", 
                            [NSAttributedString stringFromUnichar:8707],  @"&exist;", 
                            [NSAttributedString stringFromUnichar:8709], @"&empty;", 
                            [NSAttributedString stringFromUnichar:8711], @"&nabla;", 
                            [NSAttributedString stringFromUnichar:8712], @"&isin;", 
                            [NSAttributedString stringFromUnichar:8713], @"&notin;", 
                            [NSAttributedString stringFromUnichar:8715], @"&ni;", 
                            [NSAttributedString stringFromUnichar:8719], @"&prod;",
                            [NSAttributedString stringFromUnichar:8721], @"&sum;", 
                            [NSAttributedString stringFromUnichar:8722], @"&minus;", 
                            [NSAttributedString stringFromUnichar:8727],  @"&lowast;",
                            [NSAttributedString stringFromUnichar:8730], @"&radic;", 
                            [NSAttributedString stringFromUnichar:8733], @"&prop;", 
                            [NSAttributedString stringFromUnichar:8734], @"&infin;", 
                            [NSAttributedString stringFromUnichar:8736], @"&ang;", 
                            [NSAttributedString stringFromUnichar:8743], @"&and;", 
                            [NSAttributedString stringFromUnichar:8744], @"&or;", 
                            [NSAttributedString stringFromUnichar:8745], @"&cap;", 
                            [NSAttributedString stringFromUnichar:8746], @"&cup;", 
                            [NSAttributedString stringFromUnichar:8747], @"&int;", 
                            [NSAttributedString stringFromUnichar:8756], @"&there4;", 
                            [NSAttributedString stringFromUnichar:8764], @"&sim;", 
                            [NSAttributedString stringFromUnichar:8773], @"&cong;", 
                            [NSAttributedString stringFromUnichar:8776], @"&asymp;", 
                            [NSAttributedString stringFromUnichar:8800], @"&ne;", 
                            [NSAttributedString stringFromUnichar:8801], @"&equiv;", 
                            [NSAttributedString stringFromUnichar:8804], @"&le;", 
                            [NSAttributedString stringFromUnichar:8805], @"&ge;", 
                            [NSAttributedString stringFromUnichar:8834], @"&sub;", 
                            [NSAttributedString stringFromUnichar:8835], @"&sup;", 
                            [NSAttributedString stringFromUnichar:8836], @"&nsub;", 
                            [NSAttributedString stringFromUnichar:8838], @"&sube;", 
                            [NSAttributedString stringFromUnichar:8839], @"&supe;", 
                            [NSAttributedString stringFromUnichar:8853], @"&oplus;", 
                            [NSAttributedString stringFromUnichar:8855], @"&otimes;", 
                            [NSAttributedString stringFromUnichar:8869], @"&perp;", 
                            [NSAttributedString stringFromUnichar:8901], @"&sdot;", 
                            [NSAttributedString stringFromUnichar:8968], @"&lceil;", 
                            [NSAttributedString stringFromUnichar:8969], @"&rceil;", 
                            [NSAttributedString stringFromUnichar:8970], @"&lfloor;", 
                            [NSAttributedString stringFromUnichar:8971], @"&rfloor;", 
                            [NSAttributedString stringFromUnichar:9001], @"&lang;", 
                            [NSAttributedString stringFromUnichar:9002], @"&rang;", 
                            [NSAttributedString stringFromUnichar:9674], @"&loz;", 
                            [NSAttributedString stringFromUnichar:9824],@"&spades;",  
                            [NSAttributedString stringFromUnichar:9827],  @"&clubs;", 
                            [NSAttributedString stringFromUnichar:9829], @"&hearts;", 
                            [NSAttributedString stringFromUnichar:9830],@"&diams;", 
    
                            nil];
    
    return result;
    
}

+(NSString*) stringFromUnichar:(unichar) entityValue {
    return [NSString stringWithFormat: @"%C", entityValue];
}

+(NSRange) rangeOfHtmlSpecialCharacter:(NSString*) html {
    NSRange rangeAngleBracket = [html rangeOfString:@"<"];
    NSRange rangeAmpersand = [html rangeOfString:@"&"];

    if (rangeAngleBracket.location == NSNotFound &&
        rangeAmpersand.location == NSNotFound) {
        return rangeAngleBracket;
    } else if (rangeAmpersand.location == NSNotFound) {
        return rangeAngleBracket;
    } else if (rangeAngleBracket.location == NSNotFound) {
        return rangeAmpersand;
    } else if (rangeAmpersand.location < rangeAngleBracket.location) {
        return rangeAmpersand;
    } else {
        return rangeAngleBracket;
    }
}

+(NSAttributedString*) fromHtml:(NSString*) html {
    UIFont* font = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    return [NSAttributedString fromHtml:html withFontSize:font.pointSize];
}

+(NSAttributedString*) fromHtml:(NSString*) html fontSize:(CGFloat) fontSize styleProvider:(NSObject<BCHStyleProvider>*) styleProvider {
    
    BCHHtmlStyle currentStyles = 0;
    NSMutableAttributedString* result = [[NSMutableAttributedString alloc] init];
    
    if (html.length > 0) {
        NSDictionary* currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
        NSMutableString* fullTitle = [html mutableCopy];
        
        NSRange range = [self rangeOfHtmlSpecialCharacter:fullTitle];
        while (range.location != NSNotFound) {
            if (range.location > 0) {
                NSString* part = [fullTitle substringToIndex:range.location];
                [fullTitle deleteCharactersInRange:NSMakeRange(0, part.length)];
                
                NSAttributedString * temp = [[NSAttributedString alloc] initWithString:part attributes:currentFont];
                [result appendAttributedString:temp];
            }
            
            BOOL isAngleBracket = [[fullTitle substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"<"];
            
            NSRange range2 = isAngleBracket ? [fullTitle rangeOfString:@">"] : [fullTitle rangeOfString:@";"];
            if (range2.location != NSNotFound) {
                range = NSMakeRange(0, range2.location + 1);
                
                NSString* tag = [fullTitle substringWithRange:range];
                if ([tag caseInsensitiveCompare:@"<i>"] == NSOrderedSame || [tag caseInsensitiveCompare:@"<cite>"] == NSOrderedSame) {
                    currentStyles |= BCHHtmlItalicStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"<b>"] == NSOrderedSame) {
                    currentStyles |= BCHHtmlBoldStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"<s>"] == NSOrderedSame) {
                    currentStyles |= BCHHtmlStrikethroughStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"</i>"] == NSOrderedSame || [tag caseInsensitiveCompare:@"</cite>"] == NSOrderedSame) {
                    currentStyles &= ~BCHHtmlItalicStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"</b>"] == NSOrderedSame) {
                    currentStyles &= ~BCHHtmlBoldStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"</s>"] == NSOrderedSame) {
                    currentStyles &= ~BCHHtmlStrikethroughStyle;
                    currentFont = [styleProvider findStyleWithAttributes:currentStyles size:fontSize];
                } else if ([tag caseInsensitiveCompare:@"<p>"] == NSOrderedSame) {
                    if (result.length > 0) {
                        [result appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:currentFont]];
                    }
                } else if ([tag caseInsensitiveCompare:@"<br>"] == NSOrderedSame) {
                    if (result.length > 0) {
                        [result appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n" attributes:currentFont]];
                    }
                } else if ([tag rangeOfString:@"&#"].location == 0) {
                    NSRange temp = NSMakeRange(2, tag.length - 3);
                    NSString* entity = [tag substringWithRange:temp];
                    unichar entityValue = (unichar) [entity intValue];
                    [result appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat: @"%C", entityValue]  attributes:currentFont]];
                } else {
                    if (htmlEntities == nil) {
                        htmlEntities = [NSAttributedString initializeEntities];
                    }
                    if ([htmlEntities objectForKey:tag] != nil) {
                        [result appendAttributedString:[[NSAttributedString alloc] initWithString:[htmlEntities objectForKey:tag]  attributes:currentFont]];
                    } else {
                        NSLog(@"Unknown tag: %@", tag);
                    }
                }
                
                [fullTitle deleteCharactersInRange:range];
                
            } else {
                NSString* text = [fullTitle substringWithRange:NSMakeRange(0, 1)];
                [fullTitle deleteCharactersInRange:NSMakeRange(0, 1)];
                
                NSMutableAttributedString * temp = [[NSMutableAttributedString alloc] initWithString:text attributes:currentFont];
                [result appendAttributedString:temp];
            }
            range = [self rangeOfHtmlSpecialCharacter:fullTitle];
        }
        if (fullTitle.length > 0) {
            NSAttributedString * temp = [[NSAttributedString alloc] initWithString:fullTitle attributes:currentFont];
            [result appendAttributedString:temp];
        }
    }
    return result;
}

+(NSAttributedString*) fromHtml:(NSString*) html withFontSize:(CGFloat) fontSize {
    return [NSAttributedString fromHtml:html fontSize:fontSize styleProvider:[BCHTrivialStyleProvider new]];
}

@end
