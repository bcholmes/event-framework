//
//  NSData+Conversion.h
//  EventsSchedule
//
//  Created by BC Holmes on 2016-02-24.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Conversion)

#pragma mark - String Conversion
- (NSString *) hexadecimalString;

@end