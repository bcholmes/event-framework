//
//  BCHDateFormattingHelper.m
//  WisSched
//
//  Created by BC Holmes on 2016-05-20.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHDateFormattingHelper.h"

@implementation BCHDateFormattingHelper

+ (id) instance {
    static BCHDateFormattingHelper* sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

-(NSDateFormatter*) hourAndAmPmFormatter {
    static NSDateFormatter* hourAndAmPmFormatter;
    if (!hourAndAmPmFormatter) {
        hourAndAmPmFormatter = [NSDateFormatter new];
        if (self.timeZone != nil) {
            hourAndAmPmFormatter.timeZone = self.timeZone;
        }
        hourAndAmPmFormatter.dateFormat = @"h a";
    }
    return hourAndAmPmFormatter;
}

-(NSDateFormatter*) hourMinuteAmPmFormatter {
    static NSDateFormatter* hourAndAmPmFormatter;
    if (!hourAndAmPmFormatter) {
        hourAndAmPmFormatter = [NSDateFormatter new];
        if (self.timeZone != nil) {
            hourAndAmPmFormatter.timeZone = self.timeZone;
        }
        hourAndAmPmFormatter.dateFormat = @"h:mm aa";
    }
    return hourAndAmPmFormatter;
}


@end
