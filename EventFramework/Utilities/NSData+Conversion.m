//
//  NSData+Conversion.m
//  EventsSchedule
//
//  Created by BC Holmes on 2016-02-24.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "NSData+Conversion.h"

@implementation NSData (Conversion)

#pragma mark - String Conversion

- (NSString *)hexadecimalString {
    NSUInteger length = self.length;
    unichar* hexChars = (unichar*)malloc(sizeof(unichar) * (length*2));
    unsigned char* bytes = (unsigned char*)self.bytes;
    for (NSUInteger i = 0; i < length; i++) {
        unichar c = bytes[i] / 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2] = c;
        c = bytes[i] % 16;
        if (c < 10) c += '0';
        else c += 'A' - 10;
        hexChars[i*2+1] = c;
    }
    
    return [[NSString alloc] initWithCharactersNoCopy:hexChars
                                               length:length*2
                                         freeWhenDone:YES];
}

@end