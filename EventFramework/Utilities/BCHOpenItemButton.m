//
//  BCHOpenItemButton.m
//  WisSched
//
//  Created by BC Holmes on 2016-08-21.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import "BCHOpenItemButton.h"

#define MAS_SHORTHAND
#import <Masonry/Masonry.h>

#import <MaterialComponents/MaterialInk.h>

#define PADDING 10

@interface BCHOpenItemButton()

@property (nonatomic, strong, nullable) MDCInkTouchController* inkController;
@property (nonatomic, strong, nullable) UILabel* secondaryTextLabel;
@property (nonatomic, strong, nullable) UIStackView* stackView;

@end

@implementation BCHOpenItemButton

-(instancetype) init {
    if (self = [super init]) {
        self.imageView = [UIImageView new];
        self.textLabel = [UILabel new];
    }
    return self;
}

-(void) setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
    if (highlighted) {
        self.backgroundColor = self.highlightedColor;
    }
    else {
        self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0];
    }
}
-(void) layoutSubviews {
    if (self.subviews.count == 0) {
        if (self.inkController == nil) {
            self.inkController = [[MDCInkTouchController alloc] initWithView:self];
            self.inkController.defaultInkView.inkColor = self.inkColor;
            [self.inkController addInkView];
        }
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [self addSubview:self.imageView];
        
        [self makeConstraints:^(MASConstraintMaker* make) {
            make.height.equalTo(@56);
        }];

        
        [self.imageView makeConstraints:^(MASConstraintMaker* make) {
            make.centerY.equalTo(self.centerY);
            make.left.equalTo(self.left);
            make.height.equalTo(@40);
            make.width.equalTo(@40);
        }];

        if (self.secondaryText.length > 0) {
            self.secondaryTextLabel = [UILabel new];
            [self addSubview:self.secondaryTextLabel];
            self.secondaryTextLabel.text = self.secondaryText;
            self.secondaryTextLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];

            self.stackView = [[UIStackView alloc] initWithArrangedSubviews:@[ self.textLabel, self.secondaryTextLabel ]];
        } else {
            self.stackView = [[UIStackView alloc] initWithArrangedSubviews:@[ self.textLabel ]];
        }

        [self addSubview:self.stackView];
        self.stackView.axis = UILayoutConstraintAxisVertical;
        self.stackView.userInteractionEnabled = NO;
        [self.stackView makeConstraints:^(MASConstraintMaker* make) {
            make.left.equalTo(self.imageView.right).with.offset(PADDING);
            make.right.equalTo(self.right);
            make.centerY.equalTo(self.centerY);
        }];
    }
}


@end
