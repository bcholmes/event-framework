//
//  UIDevice.h
//  unslacked-app
//
//  Created by BC Holmes on 2012-09-23.
//
//

#import <UIKit/UIKit.h>
#import <UIDevice-Hardware/UIDevice-Hardware.h>


#define iPhone6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 667)
#define iPhone6Plus ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 736)


@interface UIDevice (Utils)

+(bool) isIPad;

@end
