//
//  BCHCardView.h
//  EventFramework
//
//  Created by BC Holmes on 2019-04-08.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BCHCardView : UIView

@property (nonatomic, strong) UIColor* inkColor;
@property (nonatomic, assign) BOOL highlighted;

@end

NS_ASSUME_NONNULL_END
