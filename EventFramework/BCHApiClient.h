//
//  UnslackedApiClient.h
//  unslacked-app
//
//  Created by BC Holmes on 11-07-03.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCHPrimaryEvent.h"

@class BCHContentCollection;
@class BCHCacheService;

@interface BCHApiClient : NSObject 

@property (nonatomic, strong) NSTimeZone* timeZone;

- (id) initWithCache:(BCHCacheService*) cacheService;
-(BCHPrimaryEvent*) getTopLevelEventById:(NSString*) eventId usingLocations:(NSDictionary*) locations;
-(BOOL) fetchLatestUpdates:(NSString*) eventId;
-(void) processUpdates:(BCHPrimaryEvent*) event usingLocations:(NSDictionary*) locations;
-(NSDictionary*) getLocations;
-(BCHContentCollection*) getContent:(NSString*) name;
@end
