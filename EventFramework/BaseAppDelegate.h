//
//  BaseAppDelegate.h
//  EventsSchedule
//
//  Created by BC Holmes on 2013-01-19.
//  Copyright (c) 2013 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

#import "BCHPrimaryEvent.h"
#import "BCHLoginService.h"
#import "BCHTheme.h"
#import "EventLocator.h"
#import "BCHEventService.h"
#import "BCHAnnouncementService.h"

@class SWRevealViewController;
@class BCHApiClient;
@class BCHContentCollection;
@class BCHPage;
@class BCHImageService;
@class BCHRestaurantService;

@interface BaseAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, EventLocator>

@property (nonatomic, strong) BCHRestaurantService* restaurantService;

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) BCHPrimaryEvent *event;
@property (weak, nonatomic, readonly) NSString* clientId;
@property (nonatomic, strong) BCHApiClient* apiClient;
@property (nonatomic, strong) NSDictionary* locations;
@property (nonatomic, strong) NSString* currentCon;
@property (nonatomic, strong) NSString* currentConName;
@property (nonatomic, strong) NSTimeZone* timeZone;
@property (nonatomic, strong) BCHContentCollection* contentCollection;
@property (nonatomic, strong) NSString* contentJsonName;
@property (weak, nonatomic, readonly) NSArray* contentTopLevel;
@property (nonatomic, readonly) BOOL isLoggedIn;
@property (nonatomic, readonly) BOOL isAlreadyPromptedForLogin;
@property (weak, nonatomic, readonly) NSString* personId;
@property (strong, nullable, nonatomic) CLLocation* defaultLocation;
@property (strong, nullable, nonatomic) CLLocation* conventionLocation;
@property (nonatomic, strong) BCHLoginService* loginService;

@property (nonatomic, strong) BCHTheme* theme;
@property (nonatomic) BOOL supportsRestaurants;
@property (nonatomic) BOOL supportsLogin;
@property (nonatomic) BOOL supportsConInfo;
@property (nonatomic) BOOL supportsAnnouncements;
@property (nonatomic) BOOL shouldScrollToCurrentTimePeriod;
@property (nonatomic, strong) NSString* blogUrl;

@property (nonatomic, strong) BCHEventService* eventService;
@property (nonatomic, strong) BCHAnnouncementService* announcementService;

@property (nonatomic, readonly) BCHImageService* imageService;

@property (nonatomic, readonly) NSArray* menuItems;

@property (strong, nonatomic) SWRevealViewController *viewController;

@property (nonatomic, strong) NSManagedObjectContext* managedObjectContext;
@property (nonatomic, nullable, strong) void (^announcementCompletionHandler)(void);
@property (nonatomic, nullable, strong) NSString* surveyUrl;
@property (nonatomic, assign) BOOL supportsReminders;

- (void)loadEventsList;
- (void)markForUpload;
- (void)dataSynch;
- (NSArray* _Nonnull) topLevelEventsForDay:(NSString* _Nonnull) day;
- (NSArray* _Nonnull) allLeafEvents;
- (BCHPage* _Nullable) contentByKey:(NSString* _Nonnull) key;
- (UnslackedEvent*)eventById:(NSString*) eventId;
- (void) markAsPromptedForLogin;

- (NSURL*) urlForAvatarId:(NSString*) avatarId;
- (void) fetchAssignments;

+ (NSString*) generateGuid;
+ (BaseAppDelegate*) instance;

@end
