Pod::Spec.new do |s|
  s.name             = "EventFramework"
  s.version          = "0.0.2"
  s.summary          = "Common components shared between the WisSched and FogGuide apps"
  s.homepage         = "https://bitbucket.org/bcholmes/event-framework"
  s.license          = { :type => 'Internally owned code.' }
  s.author           = { "BC Holmes" => "webmaster@bcholmes.org" }
  s.source           = { :git => "https://bitbucket.org/bcholmes/event-framework", :tag => s.version }

  s.platforms     = { :ios => "11.0" }
  s.requires_arc = true

  s.source_files = 'EventFramework/*/*.{h,m}','EventFramework/**/*.{h,m}'

  s.dependency 'UIDevice-Hardware', '~> 0.1'
  s.dependency 'BlocksKit'
  s.dependency 'MGSwipeTableCell'
  s.dependency 'Colours'

  s.frameworks = 'UIKit'
  s.module_name = 'EventFramework'
end
