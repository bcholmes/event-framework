//
//  NSAttributedStringHtmlTests.m
//  WisSched
//
//  Created by BC Holmes on 2016-08-27.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSAttributedString+Html.h"

@interface NSAttributedStringHtmlTests : XCTestCase

@end

@implementation NSAttributedStringHtmlTests

- (void)testConvertSimpleHtml {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is a <b>test</b>"];
    XCTAssertEqual(string.length, 14, "string length");
}

- (void)testContainsNakedAmpersand {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is something & something else"];
    XCTAssertEqual(string.length, 34, "string length");
}

- (void)testContainsStrikethrough {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is <s>something</s> else"];
    XCTAssertEqual(string.length, 22, "string length");
}

- (void)testContainsInconsistentCapitalization {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is <i>something</I> else"];
    XCTAssertEqual(string.length, 22, "string length");
}

- (void)testContainsAdjacentTags {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is <i><b>something</b></i> else"];
    NSLog(@"string: %@", [string string]);
    XCTAssertEqual(string.length, 22, "string length");
}

- (void)testContainsHtmlEntityTags {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is something &amp; something else"];
    NSLog(@"string: %@", [string string]);
    XCTAssertEqual(string.length, 34, "string length");
}

- (void)testContainsMixOfTagsAndEntities {
    NSAttributedString* string = [NSAttributedString fromHtml:@"This is something <b>&amp;</b> something else"];
    NSLog(@"string: %@", [string string]);
    XCTAssertEqual(string.length, 34, "string length");
}


- (void)testContainsParagraphTags {
    NSAttributedString* string = [NSAttributedString fromHtml:@"<p>This is something<p>And something else"];
    NSLog(@"string: %@", [string string]);
    XCTAssertEqual(string.length, 37, "string length");
}



@end
